(** * Course9: introduction to the Module system *)




(** * A tiny introduction to Coq's Module system. *)

(** Modules are used to structure data and 
theories in order to 
  - allow program (or proof) reuse in 
   various contexts and 
  - offer the ability to hide implementation 
   details and thus prevent users to rely on them 
   and therefore allow to improve the design of 
   the module without keeping the previous 
   implementation details.
Coq Module system is inspired from the one of OCaml 
and, while it is very powerful, we will only touch 
upon basic features of the module system. 
*)

(** A module is given in general by first 
specifying a _signature_ or _interface_ that 
specifies what data and logical information 
a module shall contain and possibly also 
specify / restrict how visible it may be 
from the outside of the module. 

Declaration of a signature is done via *)
Module Type Moduletest. 
(* ... *)
End Moduletest. 


(** The most basic use of Module is to prevent 
name cashes: 
*)

Module Mynat.
Inductive nat:= 
|O: nat
|S: nat -> nat.
End Mynat.

Module Mynat2.
Inductive nat:= 
|O: nat
|S: nat -> nat.
End Mynat2.

Print nat. 

Print Mynat.nat.

Print Mynat2.nat.

(** But modules can be used in much richer ways 
in order to structure data and logical theories, 
let us see some examples of use. *)


Require Import List.
Import ListNotations.


(** Example of "Module Type", also known as a 
_signature_, or an interface. *)

Module Type MYLIST.
 Parameter t : Type -> Type.
 Parameter empty : forall A, t A.
 Parameter cons : forall A, A -> t A -> t A.
 Parameter decons : forall A, t A -> option (A * t A).
 Parameter length : forall A, t A -> nat.
End MYLIST.


(** Example of an implementation of this signature.

    With the "<:" syntax below, Coq checks that the definitions
    in module MyList0 are compatible with MYLIST.
    But MyList0 is left unrestricted afterwards.
*)

Module MyList0 <: MYLIST.
 Definition t := list.
 Definition empty {A} : list A := [].
 Definition cons := @List.cons.
Definition decons {A} (l:list A) :=
  match l with
  | [] => None
  | x::l => Some (x,l)
  end.
 Definition length := @List.length.

 (* Note: the implementation can contain extra stuff not mentionned in 
the signature. *)
 Definition truc := 1 + 2.
End MyList0.

Print MyList0.t.

Check MyList0.truc.

Compute (MyList0.cons _ 1 MyList0.empty).



(** The problem is that the interface specification 
allows other implementations which do not correspond to 
our idea: *)

Module MyList1 <: MYLIST.
 Definition t := list.
 Definition empty {A} : list A := [].
 Definition cons {A} (a: A) (l: list A) := l.
 Definition decons {A} (l:list A) : option (A * t A):= None.
 Definition length {A} (l: list A) := 42.
End MyList1.

(** In fact, a signature can contain contain declaration 
of computational data as well as logical data as well as 
the declaration as a proper name space. *)

Module Type Moduletest'. 
 Parameter A: Set.
 Parameter inf sup: nat. 
 Axiom Bounds : inf <= sup.
End Moduletest'. 


(** We can use this to make the specification more precise, 
in the module signature: *)

Module Type MYLOGICALLIST.
 Parameter t : Type -> Type.
 Parameter empty : forall A, t A.
 Parameter cons : forall A, A -> t A -> t A.
 Parameter decons : forall A, t A -> option (A * t A).
 Parameter length : forall A, t A -> nat.

 Axiom empty_def: forall A, decons A (empty A) = None. 
 Axiom cons_decons: forall A, forall (a: A), forall (l: t A), 
                 decons A (cons A a l) = Some (a, l).
 Axiom length_cons : forall A, forall (a: A), forall (l: t A), 
    length A (cons A a l) = S(length A l).
End MYLOGICALLIST.

 
Module MyList <: MYLOGICALLIST.
(* data / computational component *)
 Definition t := list.
 Definition empty {A} : list A := [].
 Definition cons := @List.cons.
 Definition decons {A} (l:list A) :=
  match l with
  | [] => None
  | x::l => Some (x,l)
  end.
 Definition length := @List.length.

(* logical component *)
 Theorem empty_def: forall A: Type, @decons A (@empty A) = None.
  Proof. trivial. Qed.

 Theorem cons_decons: forall A, forall (a: A), forall (l: t A), 
                 decons (cons A a l) = Some (a, l).
 Proof. trivial. Qed.

 Theorem length_cons : forall A, forall (a: A), forall (l: t A), 
    @length A (@cons A a l) = S(@length A l).
  Proof. trivial. Qed.

End MyList.

Print MyList.t.

Compute (MyList.cons _ 1 MyList.empty).

Print MyList.length_cons.


(** Now, if we use the syntax ":" below instead of "<:" in the definition
    of a module, all internal details will be masked afterwards, hidden
    by MYLOGICALLIST, and only the information given by MYLOGICALLIST 
    will be available.
    In particular, this will prevent here any computation, since
    the body of the definitions will be inaccessible.
    So in Coq this ":" is usually far too restrictive, unlike in
    languages like OCaml.
*)

Module RestrictedMyList : MYLOGICALLIST := MyList.

Print RestrictedMyList.t.

Compute (RestrictedMyList.cons _ 1 (RestrictedMyList.empty _)).

Fail Check RestrictedMyList.truc.



(** A "functor" ("foncteur" in French) is a module parametrized
    by another module.
*)


(** Example: starting from [MYLOGICALLIST], one may propose some [head] and
    [tail] functions. *)

Module HeadTail (M:MYLOGICALLIST).
  Definition head {A} : M.t A -> option A :=
    fun l => match M.decons _ l with None => None | Some (x,l') => Some x end.
  Definition tail {A} : M.t A -> option (M.t A) :=
    fun l => match M.decons _ l with None => None | Some (x,l') => Some l' end.
End HeadTail.

(** For now, this does not create any new concrete functions. *)

Fail Print HeadTail.head.

(** But we can use this in a generic way on any implementation of MYLOGICALLIST. *)

Module MyListHdTl := HeadTail(MyList).

Print MyListHdTl.head.

Compute MyListHdTl.head [1;2].

(* But we do not have anymore access to the components from MyList... *)
Fail Print MyListHdTl.cons. 


(** We can even extend a module, via a notion of inclusion. *)

Module MyList2.
 Include MyList.
 Include HeadTail(MyList).
End MyList2.

Print MyList2.head.
Print MyList2.cons.
 
(** Lighter syntax for the same thing: *)

Module MyList3 := MyList <+ HeadTail.

(** Another example of functor: starting from a first module
    satisfying interface Foo, we could build another one for which
    the [length] function is working in constant time.
    For that we store somewhere this size, and update it after
    all operations. That's a typical example of time vs. space tradeoff.
*)

Module FastLength (M:MYLIST) <: MYLIST.
 Definition t A := (M.t A * nat)%type.
 Definition empty A := (M.empty A, 0).
 Definition cons A x (l:t A) :=
  let (l,n) := l in
  (M.cons A x l, S n).
 Definition decons A (l:t A) :=
   let (l,n) := l in
   match M.decons A l with
   | None => None
   | Some (x,l) => Some (x,(l,pred n))
   end.
 Definition length A (l:t A) := snd l.

End FastLength.

(** Finally, a module can be imported, resulting 
in giving access to the module components with 
their short names:
*)

Module MyBool. 
 Definition t := bool. 
 Definition tt := true.
 Definition ff := false. 
End MyBool. 

Check MyBool.ff. 

Fail Check ff.

Import MyBool. 

Check ff. 

