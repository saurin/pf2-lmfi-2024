(** * Course8: Inversion and automation. *)



(** * The [Inversion] tactic for inductive types:


*)


(** * A few words about the inversion tactics 

Already seen : injection and discriminate :

  - [injection H]  where [H : S x = S y] gives [x = y].
  - [injection H]  where [H : x::l = x'::l'] gives both [x=x'] and [l=l'].
  - [discriminate H] where [H : O = S x] conludes the goal
    (since this case is impossible).
*)


(** The tactic [inversion] is a generalization of both,
   trying to recover from an inductive predicate
   what situations may have led to this concrete predicate.
*)



Inductive even : nat -> Prop :=
 | even_O : even O
 | even_SS n : even n -> even (S (S n)).

Lemma even_2 : even 2.
Proof. 
 apply even_SS.
 apply even_O.
Qed.

Lemma even_plus4 : forall n, even n -> even (4+n).
Proof.
 intros.
 apply even_SS.
 apply even_SS.
 assumption.
Qed.

(** Up to now, [even_2] and [even_plus4] are direct proofs,
   no need for inversion. *)

Lemma not_even_one : ~even 1.
Proof.
 intro.
 (* Here, destruct H (or induction H) would forget that our 
    number is 1... *)
 destruct H. Show 1. Show 2. Undo.
(** Before destruct H, we need to save all details ourselves
    (for instance via "remember"). *)
 remember 1 as m.
 destruct H.
 - discriminate.
 - discriminate.
Qed.

(** inversion is here nicer than this remember + destruct,
   and way more general *)

Lemma not_even_one_bis : ~even 1.
Proof.
 intro.
 inversion H.
Qed.

Lemma even_plus3 n : even (3+n) -> even (1+n).
Proof.
 intro H.
 inversion H. 
 (* subst. (* if you want to get rid of remaining equations *)*)
 assumption.
Qed.

(** Since equality is also an inductive predicate, inversion 
   also works on equality hypothesis (and subsumes both 
   [injection] and [discriminate]). *)

Lemma test_inj x : S x = 0 -> False.
Proof.
intro H.
inversion H.
Qed.

(** ** Why is it called inversion? *)
 
Theorem even_SS_inv : forall n: nat, even (S(S n)) -> even n.
Proof.
intros n H. inversion H. trivial.
Qed.

(** * How does [inversion] work?

Let us try to prove manually the previous theorem. 
*)

Theorem even_SS_inv_bis : forall n: nat, even (S(S n)) -> even n.
Proof.
intros n H. 
generalize (refl_equal (S(S n))).
pattern (S(S n)) at -2.
elim H.
- intro H0. discriminate.
- intros n0 H0 H1 H2. 
injection H2. intro H3. 
rewrite H3 in H0. assumption.
Qed.

(** The same can be done to show that [1] is not [even]: *)


Lemma not_even_one_ter : ~even 1.
Proof.
intro H.
generalize (refl_equal 1).
pattern 1 at -2.
elim H. 
- intro H0. discriminate.
- intros n0 H0 H1 H2. discriminate H2.
(* or shorter: *) 
Restart. 
 intro H; generalize (refl_equal 1); pattern 1 at -2.
elim H; discriminate.
Qed.




(** * Impredicative encodings *)

(** Note that we can quantify on all propositions and get a 
   new proposition. That's impredicativity. 
   In Coq that's specific to Prop : the Type universe is 
   predicative (i.e. not impredicative).

** Alternative (impredicative) False.
*)

Definition FalseBis : Prop := forall (P:Prop), P.

Definition All_Type_at_i := forall (P:Type), P.

Check All_Type_at_i.

Lemma False_equiv : False <-> FalseBis.
Proof.
 split.
 - destruct 1.
 - intro H. unfold FalseBis in *.
   apply H.
Qed.


(** ** Alternative (impredicative) disjunction *)

Definition OrBis (P Q : Prop) : Prop :=
 forall R:Prop, (P -> R) -> (Q -> R) -> R.

Lemma or_equiv P Q : P \/ Q <-> OrBis P Q.
Proof.
 split.
 - destruct 1.
   + unfold OrBis. intros R pr qr. apply pr, H.
   + intros R pr qr. apply qr, H.
 - intro H. unfold OrBis in H. apply H.
   + now left.
   + now right.
Qed.

(** ** Alternative (impredicative) definition for exists *)

Definition ExistsBis {X}(P:X->Prop)
 := forall Q:Prop, (forall x, P x -> Q) -> Q.

Lemma Exists_equiv {X}(P:X->Prop) :
 (exists x, P x) <-> ExistsBis P.
Proof.
 split.
 - intros (w,H). unfold ExistsBis.
   intros Q H'. apply (H' w), H.
 - intro H. unfold ExistsBis in H. apply H.
   intros w Hw.
   exists w. auto.
Qed.


(** ** Alternative (impredicative) conjunction *)

Definition AndBis (P Q : Prop) : Prop :=
 forall R:Prop, (P -> Q -> R) -> R.

Lemma and_equiv P Q : P /\ Q <-> AndBis P Q.
Proof.
 split.
 - destruct 1. intros R pqr. apply pqr; auto.
 - intro H. apply H. split; auto.
Qed.


(** * More on [apply] 
*)

Require Import Arith.
Check Nat.le_trans. 

SearchPattern (?X <= ?Y -> ?Z*?X <= ?Z*?Y).
SearchPattern (?X <= ?Y -> ?X*?Z <= ?Y*?Z).

Theorem le_mult_mult :
forall a b c d:nat, a <= c -> b <= d -> a*b <= c*d.
Proof.
 intros a b c d H H0.
 Fail apply Nat.le_trans.
(* Indeed no variable is infered here for m. 
a solution is to use "apply with": *)
 apply Nat.le_trans with (m := c*b).
 apply Nat.mul_le_mono_r; assumption.
 apply Nat.mul_le_mono_l; assumption.
Qed.

(** [apply t with (v1 := t1)...(vn:=tn)]
may be useful as soon as some variables may 
not be infered just by looking at the goal and 
comparing it with the head type of [t].


** [eapply]

*)

Theorem le_mult_mult_bis :
forall a b c d:nat, a <= c -> b <= d -> a*b <= c*d.
Proof.
intros a b c d H H0.
eapply Nat.le_trans.
(* eapply does not try to guess m, just leave it
as an existential variable ... *)
eapply Nat.mul_le_mono_r.
(* ... to be instantiated later: *) 
eexact H. 
apply Nat.mul_le_mono_l; assumption.
Qed.


(** * More on the [auto] tactic 

** [auto with]

The behaviour of [auto] can be extended by 
specifying additional tactics to be tried by 
auto, as part of tactics databases, with the syntax 

[auto with b1 b2 ...bn.]

The basic tactic database is named [core].
Some additional such databases are for instance 
[arith]: 

[auto with arith.]


** Tactics databases 

auto can be extended by defining tactics databases 
that we may instruct auto to use while automating 
a search.

auto with b1 b2 ...bn

 We can also identify a list of lemmas that 
should be considered by auto when trying to solve a 
goal. The general syntax for this is:

[Hint Resolve thm_1 ... thm_k : database.]

The core database can be extended, for instance to try 
discriminate each time the shape of the goal may be suitable:

[Hint Extern 4 (_ <> _) => discriminate : core.]
*)


#[export] Hint Resolve even_O even_SS : even_base.


Lemma even_6 : even 6.
Proof.
auto.
auto with even_base.
Qed.

Lemma even_10 : even 10.
Proof.
auto with even_base.
auto 6 with even_base.
Qed.


Lemma even_20 : even 20.
Proof.
auto 11 with even_base.
Qed.

Lemma even_plus10 : forall n, even n -> even (10 + n).
Proof.
intros n H. simpl.
auto 6 with even_base.
Qed.


(** ** Beware of what you introduce in your database. 

Some theorems that could be used by auto are in fact bad 
candidates because they create loops. For instance:

[sym_equal: ∀(A:Type)(xy:A),x=y→y=x]

Indeed, [apply sym_equal] turn the current goal into 
another goal on which the tactic can be applied again,
leading to the original goal, and looping: auto cannot 
detect such loops.

Sometimes, the subgoal does not have the same shape 
but the search may run into a never-ending non-conclusive 
search, for instance:

[le_S_n : forall n m:nat, Sn<=Sm->n<=m]

This may still be useful when dealt with care. Imagine 
for instance that you are trying to prove goal [n <= n]
in a context containing [H : S(S (S (S n))) <= S( S (S (S m)))]

then applying four times theorem [le_S_n] and then hypothesis 
[H] allow to conclude and solve the goal. 

Defining a tactics database containing solely this theorem:

[Hint Resolve le_S_n : le_base.]

One can thus solve the goal with:
[auto with le_base.]

Given that only theorem [le_S_n] is used here the complexity
will remain "acceptable".
*)

Check le_S_n.

#[export] Hint Resolve le_S_n : le_base.

Lemma le_S_auto: forall n m: nat, 
S(S (S (S n))) <= S( S (S (S m))) -> n <= m.
Proof. 
intros n m H. 
auto with le_base.
Qed.

Print le_S_auto.

Lemma le_10_auto: forall n m: nat, 
10 + n <= 10 + m -> n <= m.
Proof. 
intros n m H. simpl in H.  
auto 11 with le_base.
Qed.




Lemma le_10_depth16: forall n m: nat, 
10 + n <= 10 + m -> n <= m.
Proof. 
intros n m H. simpl in H.  
Time auto 16 with le_base.
Qed.

Lemma le_10_depth17: forall n m: nat, 
10 + n <= 10 + m -> n <= m.
Proof. 
intros n m H. simpl in H.  
Time auto 17 with le_base.
Qed.

Lemma le_10_depth19: forall n m: nat, 
10 + n <= 10 + m -> n <= m.
Proof. 
intros n m H. simpl in H.  
Time auto 19 with le_base.
Qed.


(** ** Clearing the context from dangerous hypotheses

A variant of the above problem with tactic databases is 
the fact that auto will use facts which are among the 
hypothesis of a goal to prove the goal. 
This may result in the same looping behaviours as before
which are proble;atic fro; an efficiency point of view. 
An option is to discard the problematic hypotheses from
from the context before using [auto]:

*)

Section Trying_auto.
Variable l1 : forall n m:nat, S n <= S m -> n <= m. 
Theorem unprovable_le2 : forall n m:nat, n <= m.
Time auto 10 with arith.
clear l1.
Time auto 10 with arith.
(**
Of course, we now have an issue : 
we have lost hypothesis l1... 
*)
Undo. Undo. 
try (clear l1; auto 10 with arith; fail).
(** Using in combination tactics [try] and [fail]
allows to ensure that hypothesis [l1] will be kept 
for those goals that [auto with arith] cannot solve. 
*)
Abort.
End Trying_auto.

(** ** [eauto]

In the same way as the need for witnesses of [apply] 
may be handled automatically via [eapply], working
with existential variables, postponing the choice
of the witnessm [auto] may be replace with [eauto]
for the same purpose:

*)

#[export] Hint Resolve Nat.le_trans 
Nat.mul_le_mono_r Nat.mul_le_mono_l: mul_base.

Theorem le_mult_mult_auto :
forall a b c d:nat, a <= c -> b <= d -> a*b <= c*d.
Proof.
intros a b c d H H0.
auto with mul_base. 
eauto with mul_base.
Qed.




(** * A tiny introduction to Coq's Module system. *)

(** Modules are used to structure data and 
theories in order to 
  - allow program (or proof) reuse in 
   various contexts and 
  - offer the ability to hide implementation 
   details and thus prevent users to rely on them 
   and therefore allow to improve the design of 
   the module without keeping the previous 
   implementation details.
Coq Module system is inspired from the one of OCaml 
and, while it is very powerful, we will only touch 
upon basic features of the module system. 
*)

(** A module is given in general by first 
specifying a _signature_ or _interface_ that 
specifies what data and logical information 
a module shall contain and possibly also 
specify / restrict how visible it may be 
from the outside of the module. 

Declaration of a signature is done via *)
Module Type Moduletest. 
(* ... *)
End Moduletest. 


(** The most basic use of Module is to prevent 
name cashes: 
*)

Module Mynat.
Inductive nat:= 
|O: nat
|S: nat -> nat.
End Mynat.

Module Mynat2.
Inductive nat:= 
|O: nat
|S: nat -> nat.
End Mynat2.

Print nat. 

Print Mynat.nat.

Print Mynat2.nat.

(** But modules can be used in much richer ways 
in order to structure data and logical theories, 
let us see some examples of use. *)


Require Import List.
Import ListNotations.


(** Example of "Module Type", also known as a 
_signature_, or an interface. *)

Module Type MYLIST.
 Parameter t : Type -> Type.
 Parameter empty : forall A, t A.
 Parameter cons : forall A, A -> t A -> t A.
 Parameter decons : forall A, t A -> option (A * t A).
 Parameter length : forall A, t A -> nat.
End MYLIST.


(** Example of an implementation of this signature.

    With the "<:" syntax below, Coq checks that the definitions
    in module MyList0 are compatible with MYLIST.
    But MyList0 is left unrestricted afterwards.
*)

Module MyList0 <: MYLIST.
 Definition t := list.
 Definition empty {A} : list A := [].
 Definition cons := @List.cons.
Definition decons {A} (l:list A) :=
  match l with
  | [] => None
  | x::l => Some (x,l)
  end.
 Definition length := @List.length.

 (* Note: the implementation can contain extra stuff not mentionned in 
the signature. *)
 Definition truc := 1 + 2.
End MyList0.

Print MyList0.t.

Check MyList0.truc.

Compute (MyList0.cons _ 1 MyList0.empty).



(** The problem is that the interface specification 
allows other implementations which do not correspond to 
our idea: *)

Module MyList1 <: MYLIST.
 Definition t := list.
 Definition empty {A} : list A := [].
 Definition cons {A} (a: A) (l: list A) := l.
 Definition decons {A} (l:list A) : option (A * t A):= None.
 Definition length {A} (l: list A) := 42.
End MyList1.

(** In fact, a signature can contain contain declaration 
of computational data as well as logical data as well as 
the declaration as a proper name space. *)

Module Type Moduletest'. 
 Parameter A: Set.
 Parameter inf sup: nat. 
 Axiom Bounds : inf <= sup.
End Moduletest'. 


(** We can use this to make the specification more precise, 
in the module signature: *)

Module Type MYLOGICALLIST.
 Parameter t : Type -> Type.
 Parameter empty : forall A, t A.
 Parameter cons : forall A, A -> t A -> t A.
 Parameter decons : forall A, t A -> option (A * t A).
 Parameter length : forall A, t A -> nat.

 Axiom empty_def: forall A, decons A (empty A) = None. 
 Axiom cons_decons: forall A, forall (a: A), forall (l: t A), 
                 decons A (cons A a l) = Some (a, l).
 Axiom length_cons : forall A, forall (a: A), forall (l: t A), 
    length A (cons A a l) = S(length A l).
End MYLOGICALLIST.

 
Module MyList <: MYLOGICALLIST.
(* data / computational component *)
 Definition t := list.
 Definition empty {A} : list A := [].
 Definition cons := @List.cons.
 Definition decons {A} (l:list A) :=
  match l with
  | [] => None
  | x::l => Some (x,l)
  end.
 Definition length := @List.length.

(* logical component *)
 Theorem empty_def: forall A: Type, @decons A (@empty A) = None.
  Proof. trivial. Qed.

 Theorem cons_decons: forall A, forall (a: A), forall (l: t A), 
                 decons (cons A a l) = Some (a, l).
 Proof. trivial. Qed.

 Theorem length_cons : forall A, forall (a: A), forall (l: t A), 
    @length A (@cons A a l) = S(@length A l).
  Proof. trivial. Qed.

End MyList.

Print MyList.t.

Compute (MyList.cons _ 1 MyList.empty).

Print MyList.length_cons.


(** Now, if we use the syntax ":" below instead of "<:" in the definition
    of a module, all internal details will be masked afterwards, hidden
    by MYLOGICALLIST, and only the information given by MYLOGICALLIST 
    will be available.
    In particular, this will prevent here any computation, since
    the body of the definitions will be inaccessible.
    So in Coq this ":" is usually far too restrictive, unlike in
    languages like OCaml.
*)

Module RestrictedMyList : MYLOGICALLIST := MyList.

Print RestrictedMyList.t.

Compute (RestrictedMyList.cons _ 1 (RestrictedMyList.empty _)).

Fail Check RestrictedMyList.truc.



(** A "functor" ("foncteur" in French) is a module parametrized
    by another module.
*)


(** Example: starting from [MYLOGICALLIST], one may propose some [head] and
    [tail] functions. *)

Module HeadTail (M:MYLOGICALLIST).
  Definition head {A} : M.t A -> option A :=
    fun l => match M.decons _ l with None => None | Some (x,l') => Some x end.
  Definition tail {A} : M.t A -> option (M.t A) :=
    fun l => match M.decons _ l with None => None | Some (x,l') => Some l' end.
End HeadTail.

(** For now, this does not create any new concrete functions. *)

Fail Print HeadTail.head.

(** But we can use this in a generic way on any implementation of MYLOGICALLIST. *)

Module MyListHdTl := HeadTail(MyList).

Print MyListHdTl.head.

Compute MyListHdTl.head [1;2].

(* But we do not have anymore access to the components from MyList... *)
Fail Print MyListHdTl.cons. 


(** We can even extend a module, via a notion of inclusion. *)

Module MyList2.
 Include MyList.
 Include HeadTail(MyList).
End MyList2.

Print MyList2.head.
Print MyList2.cons.
 
(** Lighter syntax for the same thing: *)

Module MyList3 := MyList <+ HeadTail.

(** Another example of functor: starting from a first module
    satisfying interface Foo, we could build another one for which
    the [length] function is working in constant time.
    For that we store somewhere this size, and update it after
    all operations. That's a typical example of time vs. space tradeoff.
*)

Module FastLength (M:MYLIST) <: MYLIST.
 Definition t A := (M.t A * nat)%type.
 Definition empty A := (M.empty A, 0).
 Definition cons A x (l:t A) :=
  let (l,n) := l in
  (M.cons A x l, S n).
 Definition decons A (l:t A) :=
   let (l,n) := l in
   match M.decons A l with
   | None => None
   | Some (x,l) => Some (x,(l,pred n))
   end.
 Definition length A (l:t A) := snd l.

End FastLength.


