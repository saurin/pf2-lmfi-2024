(** * Course 11: Typeclasses in Coq *)

(** In this lecture, we will study typaclasses 
in Coq. This lecture is largely inspired from 
the Software foundations' tutorial on Typeclasses, 
as well as on Sozeau and Casteran lecture notes 
on typeclasses. *)


From Coq Require Import Bool.Bool.
From Coq Require Import Strings.String.
From Coq Require Import Arith.Arith.
From Coq Require Import Lia.
Require Import List. 
Import ListNotations.
Local Open Scope string.

(** We have seen a lot of uses of Coq's polymorphism 
features in this class allowing us to reuse the same 
code or proof for various types. This notion of 
polymorphism is called _parametric_ polymorphism and it 
allows a single program to be reused with _any_ type 
as it was defined by considering a "generic" type, of 
which no specific property is assumed. 
In that view, we use type variables in place of actual 
types, and we can then instantiate the code with 
particular types when needed by applying a type 
argument, explicitly or implicitly. 

However, it is not always possible to define code 
generically and one may need to define a program 
depending on the type of the data it receives as input. 
In that case we rather speak of _ad hoc_ polymorphism. 
The classic example is that of a printing function 
which shall of course print the data in various ways 
depending on the input data. As far as we are concerned, 
instead of printing we will rather consider the question 
of converting a data to a String.

One simple option is to define one function for each such 
input base type: 

       - [showBool : bool -> string]
       - [showNat : nat -> string]
       - etc.

as well as combinators for structured types like [list] 
or pairs:

 - [showList : {A : Type} 
        (A -> string) -> (list A) -> string]
 - [showPair : {A B : Type}
        (A -> string) -> (B -> string) -> A * B -> string]

allowing us to define string converters for more complex 
types by assembling them from these pieces:

 - [showListOfPairsOfNats = showList (showPair showNat showNat)]
 *)

(** However, we can soon see the limits of such an approach 
and we need more structured and uniform way to define 
string conversion functions, both in the _naming_ of the 
function and the way it _assembles_ the data. 


_Typeclasses_ were first introduced in Haskell to provide 
a way to define more uniformly this kind of ad hoc 
polymorphic functions, using the typechecker in order to 
automatically construct functions in a "type-driven" manner
[Wadler and Blott 1989]. 

Coq's Typeclasses were introduced about 20 years later 
and they are very powerful tools, due to the rich type 
theory of Coq and in particular due to the fact that they 
can not only be used to automatically construct programs 
but also to construct proofs.

Note that typeclasses can make complex developments
much more elegant and easy to manage when used 
properly, but on the other hand they can cause a 
great deal of trouble when things go wrong!

We shall now introduce the basic features of Coq's 
typeclasses, explain their implementation, see some 
examples. *)


(** * Basics *)

(** ** Classes and Instances *)

(** To automate converting various kinds of data 
into strings, we begin by defining a typeclass 
called [Show]: *)

Class Show A : Type :=
  {
    show : A -> string
  }.

(** The [Show] typeclass can be thought of as 
"classifying" types whose values can be converted 
to strings -- that is, types [A] such that we can 
define a function [show] of type [A -> string].

Of course, Coq will not know at first that a given 
type, say _bool_ belongs to this class: we shall 
instruct the system that it is so by declaring that 
the type is an instance of the class. For instance 
(pun intended) [bool] is such a type by giving an 
[Instance] declaration that witnesses this function: *)

#[export] Instance showBool : Show bool :=
  {
    show := fun b:bool => if b then "true" else "false"
  }.


(** Here, the [export] pragma instructs Coq to export 
this instance whenever this module is loaded. Other 
options are [local] (which never exports the instance), 
and [global] (which always does). *)

Compute (show true).

Check show. 
(** Other types can similarly be equipped with [Show] 
instances -- including, of course, new types that we 
define. *)

Inductive primary := Red | Green | Blue.

#[export] Instance showPrimary : Show primary :=
  {
    show :=
      fun c:primary =>
        match c with
        | Red => "Red"
        | Green => "Green"
        | Blue => "Blue"
        end
  }.

Compute (show Green).

Check show. 

(** The [show] function is sometimes said to be 
_overloaded_, since it can be applied to arguments 
of many types, with potentially radically different 
behavior depending on the type of its argument. *)

(** Converting natural numbers to strings is 
conceptually similar, though it requires a tiny bit 
of programming: *)

Fixpoint string_of_nat_aux (time n : nat) (acc : string) : string :=
  let d := match n mod 10 with
           | 0 => "0" | 1 => "1" | 2 => "2" | 3 => "3" | 4 => "4" | 5 => "5"
           | 6 => "6" | 7 => "7" | 8 => "8" | _ => "9"
           end in
  let acc' := d ++ acc in
  match time with
    | 0 => acc'
    | S time' =>
      match n / 10 with
        | 0 => acc'
        | n' => string_of_nat_aux time' n' acc'
      end
  end.

Definition string_of_nat (n : nat) : string :=
  string_of_nat_aux n n "".

#[export] Instance showNat : Show nat :=
  {
    show := string_of_nat
  }.

Compute (show 42).

Check show. 

Compute (show true).

Check show. 

(** **** Exercise 1: showNatxBool

Write a [Show] instance for pairs of a nat and a bool. *)


(* FILL IN HERE

[] *)

(** Next, we can define functions that use the 
overloaded function [show] like this: *)

Definition showOne {A : Type} `{Show A} (a : A) : string :=
  "The value is " ++ show a.

Compute (showOne true).
Compute (showOne 42).
(* Compute (showOne (10,true)). *)

(** What is the role of the parameter [`{Show A}] ? 
It is a _class constraint_, which states that the 
function [showOne] is expected to be applied only to
types [A] that belong to the [Show] class.

Concretely, this constraint should be thought of as 
an extra parameter to [showOne] supplying _evidence_ 
that [A] is an instance of [Show] -- i.e., it is 
essentially just a [show] function for [A], which is 
implicitly invoked by the expression [show a]. *)

(** More interestingly, a single function can come 
with multiple class constraints: *)

Definition showTwo {A B : Type}
           `{Show A} `{Show B} (a : A) (b : B) : string :=
  "First is " ++ show a ++ " and second is " ++ show b.

Compute (showTwo true 42).
Compute (showTwo Red Green).

(** In the body of [showTwo], the type of the argument 
to each instance of [show] determines which of the 
implicitly supplied show functions (for [A] or [B]) 
gets invoked. *)

(** What happens if we forget the class constraints in 
the definitions of [showOne] or [showTwo]?  *)

Fail Definition showTwo {A B : Type}
            `{Show B} (a : A) (b : B) : string :=
  "First is " ++ show a ++ " and second is " ++ show b.


(** Of course, [Show] is not the only interesting 
typeclass.  There are many other situations where it 
is useful to be able to choose (and construct) specific 
functions depending on the type of an argument that is 
supplied to a generic function like [show].
Another typical example is _equality checkers_. *)

(** ** Equality checks 

Here is another basic example of typeclasses: a class [Eq]
describing types with a (boolean) test for equality. *)

Class Eq A :=
  {
    eqb: A -> A -> bool;
  }.

Notation "x =? y" := (eqb x y) (at level 70).

(** And here are some basic instances: *)

#[export] Instance eqBool : Eq bool :=
  {
    eqb := fun (b c : bool) =>
       match b, c with
         | true, true => true
         | true, false => false
         | false, true => false
         | false, false => true
       end
  }.

#[export] Instance eqNat : Eq nat :=
  {
    eqb := Nat.eqb
  }.

(** One possible confusion should be addressed here: 
Why should we need to define a typeclass for boolean 
equality when Coq's _propositional_ equality ([x = y]) 
is completely generic?  The answer is that, while it 
makes sense to _claim_ that two values [x] and [y] are 
equal no matter what their type is, it is not possible 
to write a decidable equality _checker_ for arbitrary
types.  In particular, equality at types like [nat->nat] 
is undecidable. *)

(** **** Exercise 2: boolArrowBool

There are some function types, like [bool->bool], for 
which checking equality makes perfect sense.  Write an 
[Eq] instance for this type. *)


(** ** Parameterized Instances: New Typeclasses from Old *)

(** What about a [Show] instance for pairs? *)

(** Since we can have pairs of any types, we will want 
to parameterize our [Show] instance by two types.  
Moreover, we will need to constrain both of these types 
to be instances of [Show]. *)

#[export] Instance showPair {A B : Type} `{Show A} `{Show B} : Show (A * B) :=
  {
    show p :=
      let (a,b) := p in
        "(" ++ show a ++ "," ++  show b ++ ")"
  }.

Compute (show (true,42)).

(** Similarly, here is an [Eq] instance for pairs... *)

#[export] Instance eqPair {A B : Type} `{Eq A} `{Eq B} : Eq (A * B) :=
  {
    eqb p1 p2 :=
      let (p1a,p1b) := p1 in
      let (p2a,p2b) := p2 in
      andb (p1a =? p2a) (p1b =? p2b)
  }.

(** ...and here is [Show] for lists: *)

Fixpoint showListAux {A : Type} (s : A -> string) (l : list A) : string :=
  match l with
    | nil => ""
    | cons h nil => s h
    | cons h t => append (append (s h) ", ") (showListAux s t)
  end.

#[export] Instance showList {A : Type} `{Show A} : Show (list A) :=
  {
    show l := append "[" (append (showListAux show l) "]")
  }.

(** **** Exercise 3:

Write an [Eq] instance for lists and [Show] and [Eq] 
instances for the [option] type constructor. *)


(** **** Exercise 4: boolArrowA

Generalize your solution to the [boolArrowBool] exercise 
to build an equality instance for any type of the form 
[bool->A], where [A] itself is an [Eq] type.  
Show that it works for [bool->bool->nat]. *)


(** ** Class Hierarchies *)

(** We often want to organize typeclasses into hierarchies.
For example, we might want a typeclass [Ord] for 
"ordered types" that support both equality and a 
less-or-equal comparison operator. *)

(** A possible-but-not-recommended way to do this is to 
define a new class with two associated functions: *)

Class OrdBad A :=
  {
    eqbad : A -> A -> bool;
    lebad : A -> A -> bool
  }.

(** The reason this is bad is because we now need to 
use a new equality operator ([eqbad]) if we want to test 
for equality on ordered values. *)

Definition lt {A: Type} `{Eq A} `{OrdBad A} (x y : A) : bool :=
  andb (lebad x y) (negb (eqbad x y)).

(** A much better way is to parameterize the definition 
of [Ord] on an [Eq] class constraint: *)

Class Ord A `{Eq A} : Type :=
  {
    le : A -> A -> bool
  }.

Notation "x <=? y" := (le x y) (at level 70).

Check Ord.

(** (The old class [Eq] is sometimes called a "superclass" 
of [Ord], since every instance of [Ord] is an instance 
of [Eq] as well. 
 *)

(** When we define instances of [Ord], we just have to 
implement the [le] operation as long as the class 
constraint is met. *)

#[export] Instance natOrd : Ord nat :=
  {
    le := Nat.leb
  }.

(** Functions expecting to be instantiated with an 
instance of [Ord] now have two class constraints, 
one witnessing that they have an associated [eqb] 
operation, and one for [le]. *)

Definition max {A: Type} `{Eq A} `{Ord A} (x y : A) : A :=
  if le x y then y else x.

(** What happens if we forget one of those class 
constraint?
 *)

Fail Definition max1 {A: Type} `{Eq A}  (x y : A) : A :=
  if le x y then y else x.


Definition max2 {A: Type} `{Ord A} (x y : A) : A :=
  if le x y then y else x.

(** **** Exercise 5: 

Define [Ord] instances for options and pairs. *)



(** **** Exercise 6:

For a little more practice, define an [Ord] instance 
for lists. *)


(** * How do Typeclasses actually work? *)


(** ** Implicit Generalization *)

(** The first thing to understand is exactly what the 
"backtick" notation means in declarations of classes, 
instances, and functions using typeclasses.  This is 
actually a quite generic mechanism, called _implicit 
generalization_, that was added to Coq to support 
typeclasses but that can also be used to good effect
elsewhere.

The basic idea is that unbound variables mentioned in 
bindings marked with [`] are automatically bound in 
front of the binding where they occur. *)

(** To enable this behavior for a particular variable, 
say [A], we first declare [A] to be implicitly 
generalizable: *)

Generalizable Variables A.

(** By default, Coq only implicitly generalizes 
variables declared in this way, to avoid puzzling behavior 
in case of typos.  There is also a [Generalize Variables 
All] command, but it's probably not a good idea to 
use it! *)

(** Now, for example, we can shorten the declaration 
of the [showOne] function by omitting the binding for 
[A] at the front. *)


Definition showOne1 `{Show A} (a : A) : string :=
  "The value is " ++ show a.

(** Coq will notice that the occurrence of [A] 
inside the [`{...}] is unbound and automatically 
insert the binding that we wrote explicitly before. *)

Print showOne1.
(* ==>
showOne1 =
fun (A : Type) (H : Show A) (a : A) => "The value is " ++ show a
 : forall A : Type, Show A -> A -> string

Arguments showOne1 {A}%type_scope {H} a
*)

(** The curly brack around A and H on the last line expresses
that they are "implicit" i.e. that the type argument [A] and 
the [Show] witness [H] are usually expected to be left 
implicit: whenever we write [showOne1], Coq will 
automatically insert two unification variables as the first 
two arguments.  This automatic insertion can be disabled 
by writing [@], so a bare occurrence of [showOne1] means the 
same as [@showOne1 _ _].  The "maximally inserted" part says 
that these arguments should be inserted automatically even 
when there is no following explicit argument. *)

(** In fact, even the [`{Show A}] form hides one bit of 
implicit generalization: the bound name of the [Show] 
constraint itself. You will sometimes see class constraints 
written more explicitly, like this... *)

Definition showOne2 `{_ : Show A} (a : A) : string :=
  "The value is " ++ show a.

(** ... or even like this: *)

Definition showOne3 `{H : Show A} (a : A) : string :=
  "The value is " ++ show a.

(** The advantage of the latter form is that it gives a 
name that can be used, in the body, to explicitly refer 
to the supplied evidence for [Show A].  This can be useful 
when things get complicated and you want to make your 
code more explicit so you can better understand and 
control what is happening. *)

(** We can actually go one bit further and omit [A] 
altogether, with no change in meaning (though, again, 
this may be more confusing than helpful): *)

Definition showOne4 `{Show} a : string :=
  "The value is " ++ show a.

Print showOne4. 

(*
showOne4 = fun (A : Type) (H : Show A)  (a : A) =>
"The value is " ++ show a
     : forall A : Type, Show A -> A -> string

Arguments showOne4  {A}%type_scope {H}  a
*)

(** The examples we have seen so far illustrate 
how implicit generalization works, but you may 
not be convinced yet that it is actually saving 
enough keystrokes to be worth the trouble of adding 
such a fancy mechanism to Coq.  Where things become 
more convincing is when classes are organized into 
hierarchies.  For example, here is an alternate 
definition of the [max] function: *)

Definition max1 `{Ord A} (x y : A) :=
  if le x y then y else x.
Print max1. 
Check max1. 

(** If we print out [max1] in full detail, we can 
see that the implicit generalization around 
[`{Ord A}] led Coq to fill in not only a binding 
for [A] but also a binding for [H], which it can
see must be of type [Eq A] because it appears as 
the second argument to [Ord].  (And why is [Ord] 
applied here to two arguments instead of just the 
one, [A], that we wrote?  Because [Ord]'s arguments 
are maximally inserted!) *)

Set Printing Implicit.
Print max1.
(* ==>
  max1 =
    fun (A : Type) (H : Eq A) (H0 : @Ord A H) (x y : A) =>
      if  x <=? y then y else x

   : forall (A : Type) (H : Eq A),
       @Ord A H -> A -> A -> A
*)

Check Ord.
(* ==> Ord : forall A : Type, Eq A -> Type *)

Unset Printing Implicit.

(** For completeness, a couple of final points about 
implicit generalization.  First, it can be used in 
situations where no typeclasses at all are involved.  
For example, we can use it to write quantified 
propositions mentioning free variables, following
the common informal convention that these are to 
be quantified implicitly. *)

Generalizable Variables x y.

Lemma commutativity_property : `{x + y = y + x}.
Proof. intros. lia. Qed.

Check commutativity_property.

(** The previous examples have all shown implicit 
generalization being used to fill in forall binders.  
It will also create [fun] binders, when this makes sense: *)

Definition implicit_fun := `{x + y}.

(** Defining a function in this way is not very natural, 
however.  In particular, the arguments are all implicit 
and maximally inserted (as can be seen if we print out 
its definition)... *)

Print implicit_fun.

(** ... so we will need to use @ to actually apply 
the function: *)

(* Compute (implicit_fun 2 3). *)
(* ==>
    Error: Illegal application (Non-functional construction):
    The expression "implicit_fun" of type "nat"
    cannot be applied to the term
     "2" : "nat"
*)

Compute (@implicit_fun 2 3).

(** Writing [`(...)], with parentheses instead of curly 
braces, causes Coq to perform the same implicit 
generalization step, but does _not_ mark the inserted 
binders themselves as implicit. *)

Definition implicit_fun1 := `(x + y).
Print implicit_fun1.
Compute (implicit_fun1 2 3).



(** ** Typeclasses are Records *)

(** Typeclasses and instances, in turn, are basically 
just syntactic sugar for record types and values 
(together with a bit of magic for using proof search to 
fill in appropriate instances during typechecking, as 
described below).

Internally, a typeclass declaration is elaborated into 
a parameterized [Record] declaration: *)

Print Show.
 
(** If you run the [Print] command yourself, with the
"Printing" flag set "All" (or in CoqIDE toolbar), you
will see that [Show] actually displays as a [Variant], 
this is Coq's terminology for a non-inductive type c
onstruction. Compare the following two definitions: *)

Variant VarTest (A: Type) :=
  V1 : A -> VarTest A
| V2 : VarTest A. 

Inductive IndTest (A: Type) :=
  I1 : A -> IndTest A
| I2 : IndTest A. 




(** Analogously, [Instance] declarations become record 
values: *)

Print showNat.
(* ==>
    showNat = {| show := string_of_nat |}
       : Show nat
*)

(** Note that the syntax for record values is slightly 
different from [Instance] declarations.  Record values 
are written with curly-brace-vertical-bar delimiters, 
while [Instance] declarations are written here with just 
curly braces.  (To be precise, both forms of braces are 
actually allowed for [Instance] declarations, and either 
will work in most situations; however, type inference 
sometimes works a bit better with bare curly braces.) *)

(** Similarly, overloaded functions like [show] are 
really just record projections, which in turn are just 
functions that select a particular argument of a 
one-constructor [Inductive] type. *)

Set Printing All.
Print show.
(* ==>
    show =
      fun (A : Type) (Show0 : Show A) =>
        match Show0 with
          | Build_Show _ show => show
        end
   : forall (A : Type), Show A -> A -> string

   Arguments A, Show are implicit and maximally inserted  *)
Unset Printing All.

(* ================================================================= *)
(** ** Inferring Instances *)

(** So far, all the mechanisms we've seen have been 
pretty simple syntactic sugar and binding munging.  
The real "special sauce" of typeclasses is the way 
appropriate instances are automatically inferred 
(and/or constructed!) during typechecking. *)

(** For example, if we write [show 42], what we 
actually get is [@show nat showNat 42]: *)

Definition eg42 := show 42.

Set Printing Implicit.
Print eg42.
Unset Printing Implicit.

(** ** How does this happen? *)

(** First, since the arguments to [show] are marked 
implicit, what we typed is automatically expanded 
to [@show _ _ 42].  The first [_] should obviously 
be replaced by [nat].  But what about the second?

By ordinary type inference, Coq knows that, to make 
the whole expression well typed, the second argument 
to [@show] must be a value of type [Show nat].  
It attempts to find or construct such a value using 
a variant of the [eauto] proof search procedure that
refers to a "hint database" called [typeclass_instances]. *)

(** **** Exercise 7: Printing the HintDb for type classes

Uncomment and execute the following command.  
Search for "For Show" in the output and have a look 
at the entries for [showNat] and [showPair]. *)

(* Print HintDb typeclass_instances.

    [] *)
Print HintDb typeclass_instances.

(** We can see what's happening during the instance 
inference process if we issue the 
[Set Typeclasses Debug] command. *)

Set Typeclasses Debug.
Check (show 42).
(* ==>
     Debug: 1: looking for (Show nat) without backtracking
     Debug: 1.1: exact showNat on (Show nat), 0 subgoal(s)
*)

(** In this simple example, the proof search 
succeeded immediately because [showNat] was in the 
hint database.  In more interesting cases, the proof 
search needs to try to assemble an _expression_
of appropriate type using both functions and constants 
from the hint database.  (This is very like what 
happens when proof search is used as a tactic to 
automatically assemble compound proofs by combining 
theorems from the environment.) *)

Check (show (true,42)).
(* ==>
     Debug: 1: looking for (Show (bool * nat)) without backtracking
     Debug: 1.1: simple apply @showPair on (Show (bool * nat)), 2 subgoal(s)
     Debug: 1.1.3 : (Show bool)
     Debug: 1.1.3: looking for (Show bool) without backtracking
     Debug: 1.1.3.1: exact showBool on (Show bool), 0 subgoal(s)
     Debug: 1.1.3 : (Show nat)
     Debug: 1.1.3: looking for (Show nat) without backtracking
     Debug: 1.1.3.1: exact showNat on (Show nat), 0 subgoal(s)      *)

Unset Typeclasses Debug.

(** In the second line, the search procedure decides to 
try applying [showPair], from which it follows (after a 
bit of unification) that it needs to find an instance 
of [Show Nat] and an instance of [Show Bool], each of 
which succeeds immediately as before. *)

(** In summary, here are the steps again: *)

(*    show 42
       ===>   { Implicit arguments }
    @show _ _ 42
       ===>   { Typing }
    @show (?A : Type) (?Show0 : Show ?A) 42
       ===>   { Unification }
    @show nat (?Show0 : Show nat) 42
       ===>   { Proof search for Show Nat returns showNat }
    @show nat showNat 42
*)


(** * Typeclasses and Proofs *)

(** Since programs and proofs in Coq are fundamentally 
made from the same stuff, the mechanisms of typeclasses 
extend smoothly to situations where classes contain not 
only data and functions but also proofs.

This is where things really get interesting for our
purpose! Even though it is a complex and broad topic, 
let us take a quick look at a few things. *)


(** ** Propositional Typeclass Members *)

(** The [Eq] typeclass defines a single overloaded 
function that tests for equality between two elements of 
some type. However, there is no insurrance that the
computed boolean value actually correctly reflects equality. 

We can extend this to a subclass that also comes with a 
proof that the given equality tester is correct, in the 
sense that, whenever it returns [true], the two values are 
actually equal in the propositional sense (and vice versa).
*)

Class EqDec (A : Type) {H : Eq A} :=
  {
    eqb_eq : forall x y, x =? y = true <-> x = y
  }.

(** To build an instance of [EqDec], we must now supply 
an appropriate proof. *)

SearchPattern (((_ =?_) = true) <-> _ = _).

Print Nat.eqb_eq.
Theorem nateqbeq : forall x y, x =? y = true <-> x = y.
Proof. 
induction x; induction y; split; simpl; intro H; try inversion H; try reflexivity.
- destruct (IHx y). f_equal. apply H0. apply H. 
- destruct (IHx x). rewrite <- H1. apply H2; reflexivity.
Qed.


#[export] Instance eqdecNat : EqDec nat :=
  {
    eqb_eq := Nat.eqb_eq
  }.

(** If we do not happen to have an appropriate proof 
already in the environment, we can simply omit it. 
Coq will enter proof mode and ask the user to use 
tactics to construct inhabitants for the fields. *)

#[export] Instance eqdecBool' : EqDec bool.
Proof.
  constructor.
  intros x y. destruct x; destruct y; simpl; unfold iff; auto.
Defined.

(** Given a typeclass with propositional members, 
we can use these members in proving things involving 
this typeclass. *)

(** Here, for example, is a quick (and somewhat 
contrived) example of a proof of a property that holds 
for arbitrary values from the [EqDec] class... *)

Lemma eqb_fact `{EqDec A} : forall (x y z : A),
  x =? y = true -> y =? z = true -> x = z.
Proof.
  intros x y z Exy Eyz.
  rewrite eqb_eq in Exy.
  rewrite eqb_eq in Eyz.
  subst. reflexivity. Qed.

(** There is much more to say about how typeclasses can 
be used (and how they should not be used) to support 
large-scale proofs in Coq.    
See the suggested readings below. *)


(** ** Substructures *)

(** Naturally, it is also possible to have typeclass 
instances as members of other typeclasses: these are 
called _substructures_.
Here is an example adapted from the Coq Reference Manual. *)

From Coq Require Import Relations.Relation_Definitions.

Class Reflexive (A : Type) (R : relation A) :=
  {
    reflexivity : forall x, R x x
  }.

Class Transitive (A : Type) (R : relation A) :=
  {
    transitivity : forall x y z, R x y -> R y z -> R x z
  }.

Generalizable Variables z w R.

Lemma trans3 : forall `{Transitive A R},
    `{R x y -> R y z -> R z w -> R x w}.
Proof.
  intros.
  apply (transitivity x z w). apply (transitivity x y z).
  assumption. assumption. assumption. Defined.

Print trans3. 


Class PreOrder (A : Type) (R : relation A) :=
  { PreOrder_Reflexive :: Reflexive A R ;
    PreOrder_Transitive :: Transitive A R }.

(** The syntax [:>] indicates that each [PreOrder] can 
be seen as a [Reflexive] and [Transitive] relation, so 
that, any time a reflexive relation is needed, a 
preorder can be used instead. *)

Lemma trans3_pre : forall `{PreOrder A R},
    `{R x y -> R y z -> R z w -> R x w}.
Proof. intros. eapply trans3; eassumption. Defined.



(** * Controlling Instantiation *)

(* ================================================================= *)
(** ** "Defaulting" *)

(** The type of the overloaded [eqb] operator... *)

Check @eqb.
(* ==>
     @eqb : forall A : Type, Eq A -> A -> A -> bool    *)

(** ... says that it works for any [Eq] type.  Naturally, we can use
    it in a definition like this... *)

Definition foo x := if x =? x then "Of course" else "Impossible".

(** ... and then we can apply [foo] to arguments of any [Eq] type.

    Right? *)

Fail Check (foo true).
(* ==>
     The command has indeed failed with message:
     The term "true" has type "bool" while it is expected
       to have type "bool -> bool". *)

(** Huh?! *)

(** Here's what happened:
      - When we defined [foo], the type of [x] was not specified, so
        Coq filled in a unification variable (an "evar") [?A].
      - When typechecking the expression [eqb x], the typeclass
        instance mechanism was asked to search for a type-correct
        instance of [Eq], i.e., an expression of type [Eq ?A].
      - This search immediately succeeded because the first thing it
        tried worked; this happened to be the constant [eqBoolBool :
        Eq (bool->bool)].  In the process, [?A] got instantiated to
        [bool->bool].
      - The type calculated for [foo] was therefore
        [(bool->bool)->(bool->bool)->bool].

    The lesson is that it matters a great deal _exactly_ what problems
    are posed to the instance search engine. *)

(** **** Exercise 8: debugDefaulting

Do [Set Typeclasses Debug] and verify that 
this is what happened.

    [] *)

(* Set Typeclasses Debug. *)
Fail Check (foo true).


Definition foo' {A: Type} (x : A) `{Eq A} := if x =? x then "Of course" else "Impossible".

Check (foo' true).


(** ** Manipulating the Hint Database *)

(** One of the ways in which Coq's typeclasses differ most from
    Haskell's is the lack, in Coq, of an automatic check for
    "overlapping instances."

    That is, it is completely legal to define a given type to be an
    instance of a given class in two different ways. *)

Inductive baz := Baz : nat -> baz.

#[export] Instance baz1 : Show baz :=
  {
    show b :=
      match b with
        Baz n => "Baz: " ++ show n
      end
  }.

#[export] Instance baz2 : Show baz :=
  {
    show b :=
      match b with
        Baz n => "[" ++ show n ++ " is a Baz]"
      end
  }.

Compute (show (Baz 42)).
(* ==>
     = "[42 is a Baz]"
     : string   *)

(** When this happens, it is unpredictable which instance will be
    found first by the instance search process; here it just happened
    to be the second. The reason Coq doesn't do the overlapping
    instances check is because its type system is much more complex
    than Haskell's -- so much so that it is very challenging in
    general to decide whether two given instances overlap.

    The reason this is unfortunate is that, in more complex
    situations, it may not be obvious when there are overlapping
    instances. *)

(** One way to deal with overlapping instances is to "curate" the hint
    database by explicitly adding and removing specific instances.

    To remove things, use [Remove Hints].
    Once again, [local], [global], and [export] pragmas ca apply: *)

#[export] Remove Hints baz1 baz2 : typeclass_instances.

(** To add them back (or to add arbitrary constants that have the
    right type to be intances -- i.e., their type ends with an applied
    typeclass -- but that were not created by [Instance] declarations),
    use [Existing Instance]: *)

#[export] Existing Instance baz1.
Compute (show (Baz 42)).
(* ==>
     = "Baz: 42"
     : string    *)

#[export] Remove Hints baz1 : typeclass_instances.

(** Another way of controlling which instances are chosen by proof
    search is to assign _priorities_ to overlapping instances: *)

#[export] Instance baz3 : Show baz | 2 :=
  {
    show b :=
      match b with
        Baz n => "Use me first!  " ++ show n
      end
  }.

#[export] Instance baz4 : Show baz | 3 :=
  {
    show b :=
      match b with
        Baz n => "Use me second!  " ++ show n
      end
  }.

Compute (show (Baz 42)).
(* ==>
     = "Use me first!  42"
     : string  *)

(** 0 is the highest priority.

    If the priority is not specified, it defaults to the number of
    binders of the instance.  (This means that more specific -- less
    polymorphic -- instances will be chosen over less specific
    ones.) *)

(** [Existing Instance] declarations can also be given explicit
    priorities. *)

#[export] Existing Instance baz1 | 0.
Compute (show (Baz 42)).
(* ==>
     = "Baz: 42"
     : string    *)


(** * Debugging *)

(* ================================================================= *)
(** ** Instantiation Failures *)

(** One downside of using typeclasses, especially many typeclasses at
    the same time, is that error messages can become puzzling.

    Here are some relatively easy ones. *)

Inductive bar :=
  Bar : nat -> bar.

Fail Definition eqBar :=
  (Bar 42) =? (Bar 43).

(* ===>
    The command has indeed failed with message:
    Unable to satisfy the following constraints:
       ?Eq : "Eq bar"  *)

Fail Definition ordBarList :=
  le [Bar 42] [Bar 43].

(* ===>
    The command has indeed failed with message:
    Unable to satisfy the following constraints:
      ?H : "Eq (list bar)"
      ?Ord : "Ord (list bar)" *)

(** In these cases, it's pretty clear what the problem is.  To fix it,
    we just have to define a new instance.  But in more complex
    situations it can be trickier. *)

(** A few simple tricks can be very helpful:
      - Do [Set Printing Implicit] and then use [Check] and [Print] to
        investigate the types of the things in the expression where
        the error is being reported.
      - Add some [@] annotations and explicitly fill in some of the
        arguments that should be getting instantiated automatically,
        to check your understanding of what they should be getting
        instantiated with.
      - Turn on tracing of instance search with [Set Typeclasses
        Debug.] *)

(** The [Set Typeclasses Debug] command has a variant that causes it
    to print even more information: [Set Typeclasses Debug Verbosity
    2.]  Writing just [Set Typeclasses Debug] is equivalent to [Set
    Typeclasses Debug Verbosity 1.] *)

(** Another potential source of confusion with error messages comes up
if you forget a [`].  For example: *)

Fail Definition max' {A: Type} {Ord A} (x y : A) : A :=
  if le x y then y else x.

Definition max' {A: Type} `{Ord A} (x y : A) : A :=
  if le x y then y else x.

(* ===>
     The command has indeed failed with message:
     Unable to satisfy the following constraints:
     UNDEFINED EVARS:
      ?X354==[A |- Type] (type of Ord) {?T}
      ?X357==[A0 Ord A x y |- Eq A] (parameter H of @le) {?H}
      ?X358==[A0 Ord A x y |- Ord A] (parameter Ord of @le) {?Ord}  *)

(** The [UNDEFINED EVARS] here is because the binders that are
    automatically inserted by implicit generalization are missing.*)


(** ** Nontermination *)

(** An even more annoying way that typeclass instantiation can go
    wrong is by simply diverging.  Here is a small example of how this
    can happen. *)

(** Declare a typeclass involving two types parameters [A] and [B] --
    say, a silly typeclass that can be inhabited by arbitrary
    functions from [A] to [B]: *)

Class MyMap (A B : Type) : Type :=
  {
    mymap : A -> B
  }.

(** Declare instances for getting from [bool] to [nat]... *)

#[export] Instance MyMap1 : MyMap bool nat :=
  {
    mymap b := if b then 0 else 42
  }.

(** ... and from [nat] to [string]: *)

#[export] Instance MyMap2 : MyMap nat string :=
  {
    mymap := fun n : nat =>
      if le n 20 then "Pretty small" else "Pretty big"
  }.

Definition e1 := mymap true.
Compute e1.

Definition e2 := mymap 42.
Compute e2.

(** Notice that these two instances don't automatically get us from
    [bool] to [string]: *)

Fail Definition e3 : string := mymap false.

(** We can try to fix this by defining a generic instance that
    combines an instance from [A] to [B] and an instance from [B] to
    [C]: *)


#[export] Instance MyMap_trans {A B C : Type} `{MyMap A B} `{MyMap B C} : MyMap A C :=
  { mymap a := mymap (mymap a) }.

(** This does get us from [bool] to [string] automatically: *)


Definition e3 : string := mymap false.
Compute e3.

(** However, although this example seemed to work, we are actually in
    a state of great peril: If we happen to ask for an instance that
    doesn't exist, the search procedure will diverge. *)

(*
Set Typeclasses Debug.
Definition e4 : list nat := mymap false.
*)

(** **** Exercise 9:  non termination

    Why, exactly, did the search diverge?  Enable typeclass debugging,
    uncomment the above [Definition], and see what gets printed.  (You
    may want to do this from the command line rather than from inside
    an IDE, to make it easier to kill.) *)



