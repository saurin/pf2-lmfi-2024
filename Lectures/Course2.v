(** * Course2 : Fixpoints and Inductive types in Coq **)

(** * Preliminaries

The Coq file for today's course is available at 

#<a href="https://gitlab.math.univ-paris-diderot.fr/saurin/pf2-lmfi-2024/-/blob/main/Lectures/Course2.v">https://gitlab.math.univ-paris-diderot.fr/saurin/pf2-lmfi-2024/-/blob/main/Lectures/Course2.v</a>#.

And the standalone exercise sheet is available at: 
#<a href="https://gitlab.math.univ-paris-diderot.fr/saurin/pf2-lmfi-2024/-/blob/main/TP/TP2.v">https://gitlab.math.univ-paris-diderot.fr/saurin/pf2-lmfi-2024/-/blob/main/TP/TP2.v</a>#.


Today, we will study in more details how one can 
use Coq's inductive types and how one can build
recursive definition over inductive types with the
Fixpoint command. 

Recall that last week, we studied the 
following basic commands:

 - [Definition] : binds some name with a 
   (non-recursive) Coq term;
 - [Fixpoint] : same, for a recursive definition;
 - [Inductive] : creates a new inductive type 
   and its constructors;
 - [Check] : displays the type of a Coq term;
 - [Print] : displays the body of a definition 
   or the details of an inductive type;
 - [Compute] : reduces a term (i.e. determines 
   its normal form) and prints it.
 - [Section] : the Section mechanism.

 As well as the basic constructs on types derived 
 from the simply typed lambda-calculus. 


*)

(* *)


(** ** nat

Coq provides a type [nat] for natural numbers. 
By default, typing [0], [1] and any other positive 
numeric constant gives you a [nat]. 
Beware : this is a "unary" encoding (Peano 
numerals), hence dramatically slow.
Those Peano's numbers are built from a constant 
[O] (the upper-case letter "o") and a function 
symbol [S] for the successor: the use of the usual 
notation for natural number is a pretty-printing 
feature.


In order to get access to operations on natural 
numbers, we load the [Arith] library (for more 
informations on the standard library, visit 
#<a href="https://coq.inria.fr/distrib/current/refman/language/coq-library.html">the 
corresponding section of the reference manual</a>#):
**)

Require Import Arith.

(**
Some operations defined on [nat]:
- addition [+],
- multiplication [*],
- euclidean division [/],
- modulo [x mod y].
- boolean comparisons of numbers : [x =? y] or 
  [x <? y] or [x <=? y].

We will see later how to perform efficient 
arithmetical operations (binary encoding) and 
how to handle negative numbers (type [Z]).

*)

(* *)

(** * Recursivity

[Fixpoint] allows to reuse the function we are 
actually defining in itself !

Before accepting a [Fixpoint], Coq checks that 
this definition is syntactically decreasing. 
For that, a criterion of _structural decrease_ is 
used, we will detail it later. In short, recursive 
calls must be done on _strict sub-terms_. 
Practically, this means using a construction 
[match ... with ... end] for accessing the 
previous integer (for instance).
**)


Fail Definition factorialfail (n:nat) :=
 match n with
 | 0 => 1
 | S m => (factorialfail m) * n
 end.


Fixpoint factorial (n:nat) :=
 match n with
 | 0 => 1
 | S m => (factorial m) * n
 end.

(**
This criterion is quite restrictive. For 
instance here, replacing [factorial m] with 
[factorial (n-1)] est refused, even though we 
could prove later that [m = (n-1)] in this case.
**)

Fixpoint failctorial (n:nat) :=
 match n with
 | 0 => 1
 | S m => (failctorial ((S m)-1)) * n
 end.

(* *)

(** **** Exercise 5 : Usual functions on natural 
numbers.

Define the following functions of type 
[nat  -> nat -> nat] (without using the ones 
of Coq standard library of course!):

- [addition]
- [multiplication]
- [subtraction]
- [power]
- [gcd]

We recall that the Ackermann-Péter function, 
AP, is defined as: 
- AP(0,n) = n+1
- AP(m+1, 0) = AP(m, 1)
- AP(m+1, n+1) = AP(m, AP(m+1, n)).

Try defining [AP], of type [nat -> nat -> nat], 
in the most natural way, based on its definition. 
What problem do you encounter?

What possible workaround can you imagine?

*)

(* *)

(** ** Recursive definitions

**** Exercise 6 : Fibonacci

- Define a function [fib] such that 
  [fib 0 = 0], 
  [fib 1 = 1] then 
  [fib (n+2) = fib (n+1) + fib n].  
  (you may use a [as] keyword to name some subpart 
  of the [match] pattern ("motif" en français)).
- Define an optimized version of [fib] that 
  computes faster that the previous one by using 
  Coq pairs.
- Same question with just natural numbers, no pairs. 
  Hint: use a special recursive style called 
  "tail recursion".
- Load the library of binary numbers via 
  [Require Import NArith]. Adapt you previous 
  functions for them now to have type [nat -> N].
  What impact does it have on efficiency ?
  Is it possible to simply obtain functions of 
  type [N -> N] ?
*)

(* *)

(** **** Exercise 7 : Fibonacci though matrices

- Define a type of 2x2 matrices of numbers 
  (for instance via a quadruple).
- Define the multiplication and the power of 
  these matrices.
  Hint: the power may use an argument of type 
  [positive].
- Define a fibonacci function through power of 
  the following matrix:

<<
1 1
1 0
>>
*)

(* *)

(** **** Exercise 8 : Fibonacci decomposition 
of numbers

We aim here at programming the Zeckendorf 
theorem in practice : every number can be 
decomposed in a sum of Fibonacci numbers, 
and moreover this decomposition is unique as 
soon as these Fibonacci numbers are distinct 
and non-successive and with index at least 2.

Load the list library: *)

Require Import List.
Import ListNotations.

(**
- Write a function [fib_inv : nat -> nat] such that 
  if [fib_inv n = k] then [fib k <= n < fib (k+1)].
- Write a function [fib_sum : list nat -> nat] such 
  that [fib_sum [k_1;...;k_p] = fib k_1 + ... + 
  fib k_p].
- Write a function [decomp : nat -> list nat] 
  such that [fib_sum (decomp n) = n] and [decomp n] 
  does not contain 0 nor 1 nor any redundancy nor 
  any successive numbers.
- (Optional) Write a function 
  [normalise : list nat -> list nat] which receives 
  a decomposition without 0 nor 1 nor redundancy, 
  but may contains successive numbers, and builds 
  a decomposition without 0 nor 1 nor redundancy 
  nor successive numbers. You might assume here 
  that the input list of this function is sorted 
  in the way you prefer. 

**)

(* *)


(** * First-class function and partial application

Functions may be given as arguments to other 
functions, or come as answers.

Functions may receive less that the expected 
number of arguments.
*)

Check Init.Nat.add. 

Definition plus3 := Init.Nat.add 3.

Check plus3.

Definition apply := fun A B (x:A) (f: A -> B) 
                    => f x.

Check apply. 

Definition k := 
fun (g : nat -> nat -> nat) h (x y z : nat) 
 => g x (S (h y z)).

Check k. 



(** * General recursivity and logical consistency

Coq is logically sound as long as we cannot 
produce a _closed_ proof of [False], a type which 
is normally empty, since it is an inductive 
type with no constructor: *)

Print False. 

(** Here _closed_ means without variables nor 
axioms in the typing environment. Without even 
knowing how [False] is defined in <<Coq>>, a fully 
general recursion would give us such a proof. 
Reminder : in <<Coq>> there is no syntactic 
distinction between proofs and programs.

[[
Fixpoint loop (n:nat) : False := loop n
Definition boom : False := loop 0.
]]

Obviously such a definition is rejected by 
<<Coq>>. Here is the <<OCaml>> equivalent of 
this code (no question of logical soundness in 
this case): 

[[
let rec loop (n:int) : 'a = loop n
let any : 'a = loop 0 
(* Type-checking ok, but then the evaluation 
loops as expected *)
]]

Similarly, <<Coq>> relies crucially on the 
property that a closed term in an _inductive_ 
type (see next section) will evaluate (we say also 
"reduce") to one of the constructors of this type, 
followed by the right number of arguments. 
This allows to derive properties such as : 
- all boolean expression is either equal to [true] 
  or to [false],  
- all natural number of type [nat] is either zero 
  or a successor, 
- etc.

Once again, an unrestricted general recursion 
would break this property. For example:

[[Fixpoint flipflop (b:bool) := negb (flipflop b).
Definition alien : flipflop true.]]

If [flipflop] were accepted by Coq (it is not!), 
we would have the equation [flipflop true = negb 
(flipflop true)], and hence [alien = negb alien]. 
This [alien] cannot hence be [true], nor [false].
*)

Fail Fixpoint flipflop (b:bool) := negb (flipflop b).


(* *)

(** * Inductive types

The keyword [Inductive] allow to enrich the system 
with a new type definition, expressed via several 
_constructor_ rules. The general syntax of a 
inductive type declaration is :

[[
Inductive t :=
| C₁ : A₁₁ -> ... -> A₁ₚ -> t
| ...
| Cₙ : Aₙ₁ -> ... -> Aₙₖ -> t
]]

The [Cᵢ] are _constructors_ of type [t], they 
may require some arguments (or not), but anyway 
they always have [t] as result type (after the 
rightmost arrow).

In future lectures we will study both restructions 
to inductive type definitions (such as the 
positivity constraint) and additional flexibility
not considered here (such the the possibility for 
[t] itself to have arguments, turning it into 
an _inductive type scheme_ 


Today, we shall simply consider and manipulate 
some examples of that later.

Basic examples (already in the Coq standard 
library, no need to copy-paste them).

[[
Inductive unit : Set := tt : unit.

Inductive bool :=
| true : bool
| false : bool.

Inductive nat :=
| O : nat
| S : nat -> nat.
]]

*)

Print unit. 
Print bool. 
Print nat. 



(* *)

(** * Match

The [match] operator (or _pattern-matching_) is 
a case analysis, following the different possible 
constructors of an inductive type.
It is very similar to OCaml's <<match>>, except 
for little syntactic differences 
([=>] in "branches" instead of [->], 
final keyword [end]).

[[
match ... with
| C₁ x₁₁ ... x₁ₚ => ...
| ...
| Cₙ xₙ₁ ... xₙₖ => ...
end
]]

The _head_ of a match (what is between [match] and 
[with]) should be of the right inductive type, the 
one corresponding to constructors [C₁] ... [Cₙ].

Usually, the _branches_ (parts after [=>]) contains 
codes that have all the same type. 
We will see later that this is not mandatory 
(see session on _dependent types_ ).

Computation and match : when the head of a match 
starts with a inductive constructor [Ci], a 
_iota-reduction_ is possible. It replaces the whole 
match with just the branch corresponding to 
constructor [Ci], and also replaces all variables 
[xi₁]...[xiₚ] by concrete arguments found in match 
head after [Ci].

Example:
*)

Compute
 match S (S O) with
 | O => O
 | S x => x
 end.


(** This will reduce to [S O] (i.e. number 1 with 
nice pretty-printing). This computation is actually 
the definition of [pred] (natural number 
predecessor) from TP0, applied to [S (S O)] i.e. 
number 2.


*)
(* *)
(** * Fixpoint definitions

The [Fixpoint] construction allows to create 
recursive functions in Coq. 
Beware, as mentionned earlier, some recursive 
functions are rejected by Coq, which only accepts 
_structurally decreasing recursive functions_.

The keyword [Fixpoint] is to be used in 
replacement of [Definition], see examples below 
and exercise to follow.

Actually, there is a more primitive notion 
called [fix], allowing to define an _internal_ 
recursive function, at any place of a code. 
And [Fixpoint] is just a [Definition] followed 
by a [fix]. More on that later, but anyway, 
favor [Fixpoint] over [fix] when possible, 
it is way more convenient. 

A [Fixpoint] or [fix] defines necessarily a 
function, with at least one (inductive) argument 
which is distinguished for a special role : the 
_decreasing argument_ or _guard_. 
Before accepting this function, Coq checks that 
each recursive call is made on a syntactic _strict 
subterm_ of this special argument. Roughly this 
means any subpart of it is obtained via a [match] 
on this argument (and no re-construction afterwards). 
Nowadays, Coq determines automatically which argument 
may serve as guard, but you can still specify it 
manually (syntax [{struct n}]).

Computation of a [Fixpoint] or [fix] : when the 
guard argument of a fixpoint starts with an 
inductive constructor [Ci], a reduction may occur 
(it is also called _iota-r\u00e9duction_, just as for 
[match]). This reduction replaces the whole 
fixpoint with its body (what is after the [:=]), 
while changing as well in the body the name of the 
recursive function by the whole fixpoint (for 
preparing forthcoming iterations).

*)
(* *)
(** * Some usual inductive types

** nat : natural numbers represented as Peano 
integers
*)

Print nat.

(** ** Binary representation of numbers
*)

Require Import ZArith.
Print positive.
Print N.
Print Z.

(** Nota bene : the "detour" by a specific type 
[positive] for strictly positive numbers allows 
to ensure that these representations are canonical, 
both for [N] and for [Z]. In particular, there is 
only one encoding of zero in each of these types 
([N0] in type [N], [Z0] in type [Z]).

** Coq pairs
*)

Print prod.

Definition fst {A B} (p:A*B) := match p with
 | (a,b) => a
 end.

Definition fst' {A B} (p:A*B) :=
 let '(a,b) := p in a.

(** ** The option type 

The option type over a type A is a datatype 
providing the option to return an element of A, 
or none. It is useful to treat cases where there 
are exceptional situations, typically some object 
being undefined. The option type is defined in 
Coq as an inductive type with two constructors, 
[Some] and [None]: *)

Print option.

(** ** The list type *)

Print list.

Require Import List.
Import ListNotations.

Check (3 :: 4 :: []).

Fixpoint length {A} (l : list A) :=
 match l with
 | [] => 0
 | x :: l => S (length l)
 end.



(** **** Exercise 9: Classical exercises on lists

Program the following functions, without using the 
corresponding functions from the Coq standard library :

 - [length]
 - concatenate ([app] in Coq, infix notation [++])
 - [rev] (for reverse, a.k.a mirror)
 - [map : forall {A B}, (A->B)-> list A -> list B]
 - [filter : forall {A}, (A->bool) -> list A -> list A]
 - at least one [fold] function, either [fold_right] or [fold_left]
 - [seq : nat -> nat -> list nat], such that 
  [seq a n = [a; a+1; ... a+n-1]]

Why is it hard to program function like [head], [last] 
or [nth] ? How can we do ?
*)

(* *)
(** **** Exercise 10:  Some executable predicates on lists

 - [forallb : forall {A}, (A->bool) -> list A -> bool].
 - [increasing] which tests whether a list of numbers is 
  strictly increasing.
 - [delta] which tests whether two successive numbers of 
  the list are always apart by at least [k].
 
*)

(* *)

(** **** Exercise 11: Powerset

Write a [powerset] function which takes a list [l] and returns 
the list of all subsets of [l].
For instance [powerset [1;2] = [[];[1];[2];[1;2]]]. 
The order of subsets in the produced list is not relevant.

*)

(** **** Exercise 12: Mergesort

- Write a [split] function which dispatch the elements of 
 a list into two lists of half the size (or almost). It is 
 not important whether an element ends in the first or second 
 list. In particular, concatenating the two final lists will 
 not necessary produce back the initial one, just a permutation 
 of it.
- Write a [merge] function which merges two sorted lists into a 
 new sorted list containing all elements of the initial ones. 
 This can be done with structural recursion thanks to a inner 
 [fix] (see [ack] in the session 3 of the course). 
 - Write a [mergesort] function based on the former functions.
*)




(** ** A first example of a dependent type 

Remember that unit is the inductive type with 
one constructor and a single element. *)


Fixpoint pow n : Type :=
 match n with
 | 0 => unit
 | S n => (nat * (pow n))%type
 end.

(** **** Exercise 13: Lists alternating elements 
of two types.

By taking inspiration from the definition of 
lists above, define an inductive type [ablists] 
depending on two types [A] and [B] which is 
constituted of lists of elements of types 
alternating between [A] and [B].
*)

(* *)


(** ** Trees in Coq

There is no predefined type of trees in Coq 
(unlike [list], [option], etc). 
Indeed, there are zillions of possible variants, 
depending on your precise need. Hence each user 
will have to define its own (which is not so 
difficult). For instance here is a version with 
nothing at leaves and a natural number on nodes.
*)

Inductive tree :=
| leaf
| node : nat -> tree -> tree -> tree.

(** **** Exercise 14: Binary trees with distinct 
internal and external nodes.

By taking inspiration from the definition of lists 
above, define an inductive type [iotree] depending 
on two types [I] and [O] such that every internal 
node is labelled with an element of type I and 
every leaf is labelled with an element of type O. 
*)

(* *)



(** * Positivity constraints

Some inductive declarations are rejected by Coq, once again for preserving 
logical soundness. Roughly speaking, the inductive type being currently 
declared cannot appear as argument of an arguments of a constructor of 
this type. This condition is named _strict positivity_. 

Illustration of the danger, in OCaml:

[[
(* First, a "lambda-calcul" version *)
type lam = Fun : (lam -> lam) -> lam (* In the type of Fun, the leftmost 
"lam" would be a non-positive occurrence in Coq *)
let identity = Fun (fun t -> t)
let app (Fun f) g = f g
let delta = Fun (fun x -> app x x)
let dd = app delta delta (* infinite evaluation, even without "let rec" ! *)


(* Second, a version producing a infinite computation in any type (hence 
could be in Coq's False) *)
type 'a poly = Poly : ('a poly -> 'a) -> 'a poly
let app (Poly f) g = f g
let delta = Poly (fun x -> app x x)
let dd : 'a = app delta delta
]]

In Coq, this term [dd] would be a closed proof of [False] if these kinds of 
inductive types would be accepted. Once again, this is also closely related 
with the fact that Coq is strongly normalizing (no infinite computations).

*)

(* *)



(** * Advanced Inductive Types

** Ordinals

We can encode in Coq (some) ordinals, via the following type : *)

Inductive ord :=
 | zero : ord
 | succ : ord -> ord
 | lim : (nat->ord) -> ord.

(**
These are a version of tree ordinals: can you see in which sense it is the case?

They are often referred to as _Brouwer ordinals_ and correspond countable ordinals.


Note that this inductive type _does_ satisfy the strict positivity 
constraint: constructor [lim] has an argument of type [nat->ord], 
where [ord] appears indeed on the right. Having instead 
[lim:(ord->nat)->ord] would be refused by Coq.

We can embed in this type the usual natural numbers of type [nat].
For instance via a mixed addition [add : ord -> nat -> ord] :
*)

Fixpoint add a n :=
 match n with
 | 0 => a
 | S n => succ (add a n)
end.

Definition nat2ord n := add zero n.

(** Now, we could use constructor [lim] and this [add] 
function to go beyond the usual numbers.*)

Check (add zero).

Definition omega := lim (add zero).

Definition omegaplusomega := lim (add omega).

Fixpoint omegan n :=
 match n with
 | 0 => zero
 | S n => lim (add (omegan n))
 end.

Check omegan.
 
Definition omegasquare := lim omegan.

Inductive ord2 :=
 | zeroa : ord2
 | succa : ord2 -> ord2
 | lima : (nat->ord2) -> ord2
 | limb : (ord->ord2) -> ord2.


(** Be careful, the standard equality of Coq is not very 
meaningful on these ordinals, since it is purely syntactic. 
For instance [add zero] and [add (succ zero)] are two different 
sequences (numbers starting at 0 vs. numbers starting at 1). 
So Coq will allow proving that [lim (add zero) <> 
lim (add (succ zero))] (where [<>] is the negation of the 
logical equality [=]). But we usually consider the limits of 
these two sequences to be two possible descriptions of [omega], 
the first infinite ordinal. We would then have to define and 
use a specific equality on [ord], actually an equivalence 
relation (we also call that a _setoid equality_).

** Trees of variable arity

Let us encode a type of trees made of nodes having a natural 
number on them, and then an arbitrary number of subtrees, 
not just two like last week's [tree]. *)

Inductive ntree :=
 | Node : nat -> list ntree -> ntree.

(** Note that this inductive type need not have a "base" 
constructor like [O] for [nat] or [leaf] for last week [tree]. 
Instead, we could use [Node n [] ] for representing a leaf.

An example of program over this type:*)

Require Import List.
Import ListNotations.

(** Addition of all elements of a list of natural numbers *)

Fixpoint sum (l:list nat) : nat :=
 match l with
 | [] => 0
 | x::l => x + sum l
 end.

(** List.map : iterating a function over all elements of a list *)

Check List.map.

(** How many nodes in a ntree ? *)

Fixpoint ntree_size t :=
 match t with
 | Node _ ts => 1 + sum (List.map ntree_size ts)
 end.

(** Why is this function [ntree_size] accepted as strictly decreasing? 
Indeed [ts] is a subpart of [t], but we are not launching the recursive 
call on [ts] itself. Fortunately, here Coq is clever enough to enter 
the code of [List.map] and see that [ntree_size] will be launched on 
subparts of [ts], and hence transitively subparts of [t]. But that trick 
only works for a specific implementation of [List.map] (check with your 
own during the practical session).


* Internal recursive function : fix

Is the Ackermann function structurally decreasing ?

 - [ack 0 m = m+1]
 - [ack (n+1) 0 = ack n 1]
 - [ack (n+1) (m+1) = ack n (ack (n+1) m)]

Not if we consider only one argument, as Coq does. Indeed, neither 
[n] nor [m] (taken separately) ensures a strict decrease. But there 
is a trick (quite standard now) : we could separate this function 
into an external fixpoint (decreasing on [n]) and an internal 
fixpoint (decreasing on [m]), and hence emulate a lexicographic 
ordering on the arguments. The inner fixpoint uses the [fix] 
syntax : *)

Fixpoint ack n :=
 match n with
 | 0 => S
 | S n =>
   fix ack_Sn m :=
   match m with
   | 0 => ack n 1
   | S m => ack n (ack_Sn m)
   end
 end.

Compute ack 3 5.

(** * Induction Principles

For each new inductive type declared by the user, Coq automatically 
generates particular functions named induction principles. Normally, 
for a type [foo], we get in particular a function [foo_rect]. This 
function mimics the shape of the inductive type for providing an 
induction dedicated to this type. For instead for [nat] : *)

Check nat_rect.
Print nat_rect.

(** Deep inside this [nat_rect], one finds a [fix] and a [match], 
and this recursion and case analysis is just as generic as it could 
be for [nat] :
we could program on [nat] without any more [Fixpoint] nor [fix] 
nor [match], just with [nat_rect]! For instance: *)

Definition pred n : nat := nat_rect _ 0 (fun n h => n) n.
(* Definition add n m : nat := nat_rect _ m (fun _ h => S h) n.*)

(** In these two cases, the "predicate" [P] needed by [nat_rect] 
(its first argument [_]) is actually [fun _ => nat], meaning that 
we are using [nat_rect] in a non-dependent manner (more on that in 
a forthcoming session).

* Pseudo Induction Principles (Skip in class!)

Example of [Pos.peano_rect] and [N.peano_rect] (mentioned in the 
solution of TD1) : we could manually "hijack" the (binary) recursion 
on type [positive] for building a peano-like induction principle 
following (apparently) a unary recursion. Check in particular that 
[Pos.peano_rect] is indeed structurally decreasing. *)

(*
Require Import PArith NArith.
Check Pos.peano_rect.
Check N.peano_rect.
Print Pos.peano_rect.
Print N.peano_rect.

(** A cleaned-up version of [peano_rect] : *)

Open Scope positive.

Fixpoint peano_rect
  (P : positive -> Type)
  (a : P 1)
  (f : forall {p}, P p -> P (Pos.succ p))
  (p : positive) : P p :=
  let Q := fun q => P (q~0) in
  match p with
  | q~1 => f (peano_rect Q (f a) (fun _ h => f (f h)) q)
  | q~0 => peano_rect Q (f a) (fun _ h => f (f h)) q
  | 1 => a
  end.
*)
(** The inner call to [peano_rect] builds [P (q~0)] by starting at [P 2] 
(justified by [f a]) and going up [q] times two steps by two steps 
(cf [fun _ h => f (f h)]). *)

(* *)








(** * Practice: TP 2


(** **** Exercise 5 : Usual functions on natural 
numbers.

Define the following functions of type 
[nat -> nat -> nat] (without using the ones of 
Coq standard library of course!):

- [addition]
- [multiplication]
- [subtraction]
- [power]
- [gcd]

We recall that the Ackermann-P\u00e9ter function, AP, 
is defined as: 
- AP(0,n) = n+1
- AP(m+1, 0) = AP(m, 1)
- AP(m+1, n+1) = AP(m, AP(m+1, n)).

Try defining [AP], of type [nat -> nat -> nat], 
in the most natural way, based on its definition. 
What problem do you encounter?

What possible workaround can you imagine?

*)

(* *)

(** ** Recursive definitions

**** Exercise 6 : Fibonacci

- Define a function [fib] such that [fib 0 = 0], 
 [fib 1 = 1] then [fib (n+2) = fib (n+1) + fib n].  
 (you may use a [as] keyword to name some subpart 
 of the [match] pattern ("motif" en fran\u00e7ais)).
- Define an optimized version of [fib] that 
  computes faster that the previous one by using 
  Coq pairs.
- Same question with just natural numbers, no pairs. 
  Hint: use a special recursive style called 
  "tail recursion".
- Load the library of binary numbers via 
  [Require Import NArith].
  Adapt you previous functions for them now to 
  have type [nat -> N]. What impact does it have 
  on efficiency ? Is it possible to simply obtain 
  functions of type [N -> N] ?
*)

(* *)

(** **** Exercise 7 : Fibonacci though matrices

- Define a type of 2x2 matrices of numbers 
  (for instance via a quadruple).
- Define the multiplication and the power of 
  these matrices.
  Hint: the power may use an argument of type 
  [positive].
- Define a fibonacci function through power of 
  the following matrix:

<<
1 1
1 0
>>
*)

(* *)

(** **** Exercise 8 : Fibonacci decomposition 
of numbers

We aim here at programming the Zeckendorf theorem 
in practice : every number can be decomposed in a 
sum of Fibonacci numbers, and moreover this 
decomposition is unique as soon as these Fibonacci 
numbers are distinct and non-successive and with 
index at least 2.

Load the list library: *)

Require Import List.
Import ListNotations.

(**
- Write a function [fib_inv : nat -> nat] such 
  that if [fib_inv n = k] then [fib k <= n < fib 
  (k+1)].
- Write a function [fib_sum : list nat -> nat] 
  such that [fib_sum [k_1;...;k_p] = fib k_1 + 
  ... + fib k_p].
- Write a function [decomp : nat -> list nat] 
  such that [fib_sum (decomp n) = n] and 
  [decomp n] does not contain 0 nor 1 nor any 
  redundancy nor any successive numbers.
- (Optional) Write a function [normalise : 
  list nat -> list nat] which receives a 
  decomposition without 0 nor 1 nor redundancy, 
  but may contains successive numbers, and builds 
  a decomposition without 0 nor 1 nor redundancy 
  nor successive numbers. You might assume here 
  that the input list of this function is sorted 
  in the way you prefer. 

**)

(* *)


(** **** Exercise 9: Classical exercises on lists

Program the following functions, without using the 
corresponding functions from the Coq standard library :

 - [length]
 - concatenate ([app] in Coq, infix notation [++])
 - [rev] (for reverse, a.k.a mirror)
 - [map : forall {A B}, (A->B)-> list A -> list B]
 - [filter : forall {A}, (A->bool) -> list A -> list A]
 - at least one [fold] function, either [fold_right] or [fold_left]
 - [seq : nat -> nat -> list nat], such that 
  [seq a n = [a; a+1; ... a+n-1]]

Why is it hard to program function like [head], [last] 
or [nth] ? How can we do ?
*)

(* *)
(** **** Exercise 10:  Some executable predicates on lists

 - [forallb : forall {A}, (A->bool) -> list A -> bool].
 - [increasing] which tests whether a list of numbers is 
  strictly increasing.
 - [delta] which tests whether two successive numbers of 
  the list are always apart by at least [k].
 
*)

(* *)

(** **** Exercise 11: Powerset

Write a [powerset] function which takes a list [l] and returns 
the list of all subsets of [l].
For instance [powerset [1;2] = [[];[1];[2];[1;2]]]. 
The order of subsets in the produced list is not relevant.

*)

(** **** Exercise 12: Mergesort

- Write a [split] function which dispatch the elements of 
 a list into two lists of half the size (or almost). It is 
 not important whether an element ends in the first or second 
 list. In particular, concatenating the two final lists will 
 not necessary produce back the initial one, just a permutation 
 of it.
- Write a [merge] function which merges two sorted lists into a 
 new sorted list containing all elements of the initial ones. 
 This can be done with structural recursion thanks to a inner 
 [fix] (see [ack] in the session 3 of the course). 
 - Write a [mergesort] function based on the former functions.
*)



(* *)

(** ** Some inductive types

**** Exercise 13: Binary trees with distinct 
internal and external nodes.

By taking inspiration from the definition of 
lists above, define an inductive type [iotree] 
depending on two types [I] and [O]such that 
every internal node is labelled with an element 
of type I and every leaf is labelled with an 
element of type O. 
*)

(* *)

(** **** Exercise 14: Lists alternating elements 
of two types.

By taking inspiration from the definition of 
lists above, define an inductive type [ablists] 
depending on two types [A] and [B] which is 
constituted of lists of elements of types 
alternating between [A] and [B].
*)

