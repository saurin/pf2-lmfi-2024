(** * Course6: Equality Proofs and Proofs by Induction in Coq *)


(** * Preliminaries

The Coq file supporting today's course is 
available at 

#<a href="https://gitlab.math.univ-paris-diderot.fr/saurin/pf2-lmfi-2024/-/blob/main/Lecture/Course6.v">https://gitlab.math.univ-paris-diderot.fr/saurin/pf2-lmfi-2024/-/blob/main/Lecture/Course6.v</a>#.


And the standalone exercise sheet is available at: 
#<a href="https://gitlab.math.univ-paris-diderot.fr/saurin/pf2-lmfi-2024/-/blob/main/TP/TP6.v">https://gitlab.math.univ-paris-diderot.fr/saurin/pf2-lmfi-2024/-/blob/main/TP/TP6.v</a>#.



Today, we start the study of Coq as a proof assistant.

*)


(** ** Encounter with the third kind: Prop *)

(** Since the start of the semester, we worked with a first 
universe hierarchy (Type(i), i in |N) (and Set is Type(0). 
*)

Check 0.
Check nat.
Check Set. 
(** Alias for the first level Type(0) *)
Check Type.

(** The various levels of the hierarchy are 
used to "type types": *)

Check nat.
Check nat -> nat.
Inductive Fin : nat -> Set :=
 | Zero n : Fin (S n)
 | Succ n : Fin n -> Fin (S n).
Check forall (a: nat), Fin a -> Fin a. 
(** [Set] is [Type(0)] *)
Check forall (A: Set), A -> A. 
(** Here, [Type] is [Type(1)] *)

Check Set -> Set. 
(** Here, [Type] is [Type(1)] *)

Check Type -> Type. 
(** Here, [Type -> Type : Type]
should be read [Type(i) -> Type(j) : Type(max(i,j)+1]
Since [Type(i) : Type(i+1)] and [Type(j) : Type(j+1)] *)

Inductive List (A: Type): Type :=
|Nil: List A
|Cons: A -> List A -> List A.
Check List.




(** Another universe : Prop
   The universe of logical statement and proofs *)

(** For proofs *)

Check I. 
(** is the canonical proof of True, hence I : True *)
Check True.
Check Prop.


(** *** Back to the type hierarchy
<<
                Type(...)
                   |
                 Type(3)
                   |
                 Type(2)
                   |
                 Type(1)
               /         \
             /            \
           /               \
Set=Type(0)                Prop
 /   |     \               |   \
nat bool bool->nat       True  False
 |   |       \             |
 O   true    \             I     -  (no closed proof of False)
      (fun (x:nat) => 1)           
>>

An upward link between T and S represents the fact that one 
can derive [T : S] in Coq type system.

In Coq, the arrow type [A->B] is actually not a primitive 
construction, it is a particular case of a _product_ 
[∀x:A,B] (Coq syntax [forall x:A, B]). When variable [x] 
does not occur in [B] (non-dependent product), we write 
[A->B]. For a first example of dependent product, 
see the definition of identity below.

Roughly, the typing rule for a product looks like :

- If [Γ ⊢ A:Type] and [Γ+x:A ⊢ B:Type] then 
  [Γ ⊢ (forall x:A,B) : Type]

In reality, one must take care of the indices of the [Type] 
universes : the rightmost index is _the max of the other 
two indices_:

- If [Γ ⊢ A: Type(i)] and [Γ+x:A ⊢ B:Type(j)] then 
  [Γ ⊢ (forall x:A,B) : Type(max(i,j))]

Or, written as an inference rule: 

<<
Γ ⊢ A:Type(i)         Γ+x:A ⊢ B:Type(j)
________________________________________(Prod-Type)
  Γ ⊢ (forall x:A,B) : Type(max(i,j))
>>

[Prop] has some specificities that we shall 
investigate below in more details. 


*)

(** *** Terminology about Set vs Prop

- In Set: 
  - A specification is any type [S] of the sort [Set]
  - A program is any term [t] the type of which is a 
    specification.
- In Prop: 
  - A proposition or statement is any type P of the sort 
    [Prop]
  - A proof is any term [t] the type of which is a 
    proposition. 

To sum up: _A program is an inhabitant of a specification 
while a proof is an inhabitant of a proposition_.
*)

(** ** Rules for forming dependent product in the Calculus 
of Constructions:


#<img src="https://www.irif.fr/_media/users/saurin/teaching/lmfi/coq-2023/cc-dep-prod-formation-rule.png" 
width="100%" 
alt="Dependent product formation rules">#

#<img src="https://www.irif.fr/_media/users/saurin/teaching/lmfi/coq-2023/cc-triples.png" 
width="100%" 
alt="Sort triples for dependent product formation rules">#


The above lines respectively correspond to: 
- simple types
- impredicativity of [Prop]
- dependency
- higher-order

*)

Check forall (A: Set), A -> A.
Check forall (A: Prop), A -> A.


(* *)


(** ** Stating theorems and proving them using Coq
*)
 

(** *** Entering the proof mode 

The user indicates she wants to start a proof 
with a command such as : 

[Lemma and_commut :
 forall A B : Prop, A /\ B <-> B /\ A.]

This command give a name (here [and_commut]) to the 
lemma, this will allow later to refer to this lemma later. 
Instead of [Lemma], one can use similar keywords such as 
[Theorem] or [Proposition] or [Fact]: they are all 
equivalent form the point of view of Coq and are used to 
help the human reader to follow a formal proof development, 
as in common mathematical texts. 


After that, the command [Proof.] is to be used to mark 
the actual beginning of the proof itself (actually not 
mandatory but strongly recommanded).
*)

(* *)
(** *** Subgoals and tactics 

When a proof is started, the proof mode of Coq will display 
in the higher right part one or several _subgoals_ that are 
to be proved separately, one by one. These subgoals are 
essentially sequents of the natural deduction logic, 
written vertically : the (named) hypothesis comes first on 
top and the conclusion is below the bar. In the upper part 
one can also find the variable declarations.

The proof is to be done by _tactics_, that are orders given 
by the user to progress in the proof of the current goal, 
by transforming this goal in various ways. Tactics have 
lowercase names. For instance, the [intro] tactic performed 
on a goal of the form [A -> B] will introduce an hypothesis 
[H : A] in the context, and replace the conclusion by 
[B]. Each inference rule of the natural deduction logic 
will correspond to one tactic (or more), but some tactics 
allow to perform more advanced proof steps, for instance 
solving linear inequalities in the Presburger arithmetic 
(tactic [lia]).

Each tactic may generate several subgoals (corresponding 
to premises of the underlying logical rule), or on the 
contrary it may completely solve the current goal and 
hence remove it. The proof is finished when all subgoals 
are proved. In this case, the command [Qed] is to be used 
(from _Quod erat demonstrandum_) to conclude the proof 
and leave the proof mode. 
Here comes a possible complete proof for the previous 
statement:
*)

Lemma and_commut :
  forall A B : Prop, A /\ B <-> B /\ A.
Proof.
 intros. 
Show Proof. split.
Show Proof.
 - intros. destruct H. 
Show Proof. split. assumption. assumption.
 - intros. destruct H. split; assumption.
Qed.

Print and_commut.

(** When tactics are separated by dots, Coq will execute 
them steps-by-steps. One can also use semi-colon [;] 
to _chain_ tactics : the second tactic is applied to 
all subgoals resulting from the first tactic on the 
current goal, and so forth. For instance, [split;assumption] 
applies [assumption] on the two subgoals created by [split].

Before a tactic, one may optionally write a _bullet_, i.e. 
one of the character [-] or [+] or [*]. These bullets 
help organizing the proof in a hierarchical way, with 
delimitation of each sub-part (enforced by [Coq]).

Such a _proof script_ is to be saved in a file with a 
[.v] extension, as done in the previous lectures for instance 
[myproofs.v]. Then it can be _compiled_ via the unix command 
[coqc myproofs.v]. If the content of this file is correct, 
then a binary file [myproofs.vo] is produced, allowing 
later a fast reload of our proofs (via the [Require Import] 
command).
*)

(* *)

(** ** A detailed study of [Coq]'s built-in logical propositions.

We shall now redo what we did in the first part of 
this lecture, but using Coq natively defined types. 
You should experience some _déjà vu_ but there will be 
some difference (mainly for implication and quantification).
*)

Print True.
 
Lemma my_first_proof : True.
Proof.
(* apply I. *)
exact I.
Qed.

Print my_first_proof.

(** Just about the same as this:
[Definition my_first_proof_bis : True := I.]
*)

(** Operators, for building more advanced statements *)

(** *** ->, implication *)

Check nat -> nat.

Check True -> True.

Lemma proof2 : True -> True.
Proof.
exact (fun _ => I).
Qed.

(** or step by step : *)

(** intro : the tactic of introduction of -> :
<<
  A |- B
-----------(->intro)
 |- A-> B 
>>
*)

Lemma proof3 : True -> True.
Proof.
intro.
Show Proof. 
(** to inspect the proof term being built *)
assumption. 
(** the _axiom rule_ of natural deduction *)
Qed.

Print proof3.

(** *** forall, the dependent version of the ->

Same introduction tactic : intro. *)

Check forall (A:Prop), A -> A.
Check forall (A:Set), A -> A.

Lemma identity : forall (A:Prop), A -> A.
Proof.
intros A a.
Show Proof. 
assumption.
Qed.

Print identity.

(** forall and its non-dependent version -> are the
   only primitive operators.

To introduce them, one has the following introduction 
tactics: [intro] / [intros] / [intros ...]
It is possible to name the hypothesis or quantified 
variables, otherwise, Coq name them automatically: 
it can be convenient but may result in less maintainable
formal proofs. 

On the other hand, in order to used an implicative of 
universally quantified statement, one uses their 
elimination: [apply H.]
*)

Lemma test : forall (A B : Prop), (A->B)->A->B.
Proof.
intros A B f a.
apply f.
(** assumption. *) 
(** apply a.*) 
exact a.
Qed.


Print test.

(** By default, apply work on the goal to be prove, 
it is backward-chaining, proving from the conclusion 
to the hypothesis. 

But one can also specify that apply should be 
used on an hypothesis, reasoning in forward-chaining
mode, from the hypotheses to the conclusion: *)

Lemma test' : forall (A B : Prop), (A->B)->A->B.
Proof.
intros A B f a.
apply f in a as b.
(** assumption. *) 
(**apply a.*) 
exact b.
Qed.

Print test. 
Print test'.

Lemma test'' : forall (A B : Prop), (A->B)->A->B.
Proof.
auto.
Qed.

Print test''.


(** Other operators *)

(** *** True *)

Check True.
Print True. 
Check I. 

(** *** False *)

Check False. 
Print False. 
(** no closed construction *)


Lemma efql : False -> forall (A:Prop), A.
Proof.
intro fa.
intro A.
Show Proof. 
destruct fa.
Show Proof.  
(** elimination of a False hypothesis *)
Qed.

Print efql. 

(** Compare with the predifined : *)

Check False_ind. 
Print False_ind.

Check False_rect. 
Print False_rect.

 
(** negation is a shortcut for ...->False *)

Check ~True.
Check True -> False.

(**
Lemma attempt : ~True.
Proof.

 unfold "~" in *. (* not a mandatory step *)
 intro.
*)

(** *** /\, conjunction

- introduction is done via the [split] tactic
- elimination via [destruct ...] *)

Parameter A B : Prop.

Lemma conj : A/\B -> B/\A.
Proof.
 intro H.
 destruct H. 
(** or [destruct H as [HA HB].]*)
 split.
 - assumption.
 - assumption.
Qed.

(** Check and_ind. *)


(** Available bullets for structuring a proof script :   [-] [+] [*]
*)


(** *** \/, disjunction

- introduction is done via the [left] and [right] tactics
- elimination via [destruct ...]
*)

Lemma disj : A\/B -> B\/A.
Proof.
 intro H.
Show Proof. 
 destruct H.
Show Proof. 
(** or  [destruct H as [HA | HB].] *)
 - right. Show Proof. assumption. Show Proof. 
 - left. assumption.
Show Proof. 
Qed.


(** *** <-> equivalence

A<->B is just a shortcut for (A->B)/\(B->A) 

*)

Lemma disj_equiv: A\/ B <-> B \/ A.
Proof. 
split; intro H;
 destruct H; [right | left | right | left]; assumption.
Qed.
(* 
Lemma disj_equiv_fail: A\/ B <-> B \/ A.
Proof. 
split; intro H;
 destruct H; (right || left); assumption.
Qed.
*)

Lemma disj_equiv': A\/ B <-> B \/ A.
Proof. 
split; intro H;
 destruct H; ((right; assumption) || (left; assumption)).
Qed.


(** exists : introduction via : exists ...
            elimination via : destruct ...
*)

Lemma example_exists : exists x : nat, x = 0.
Proof.
 exists 0. reflexivity.
Qed.


(* *)
(** ** Summary of elementary tactics

- [assumption] if the current goal is exactly one of the 
 hypothesis (cf. the _axiom rule_ in logic).
- For all primitive connectors (quantification [∀], 
 implication [->]):
   - introduction via [intro] (or one of its variants 
   [intros], [intro x], [intros x y ...])
   - elimination via [apply H] (where [H] is the name of 
   the hypothesis to eliminate).
- The other connectors (which are actually inductive 
  definitions) may ideally be introduced by [constructor] 
  and eliminated by [destruct H].
  But the introduction frequently requires more ad-hoc 
  tactics:
   - [split] for a conjunction [/\] (written <</\>>)
   - [left] and [right] for a disjunction [\/] (written 
     <<\/>>)
   - [exists ...] (written <<exists>>) for a quantification 
   [∃] (where [...] is the place where the existential 
   witness is given)
   - No introduction tactic for [False] !
   - For [True] (seldom used in Coq), the introduction can 
   be done via [constructor], but nothing to eliminate.
- Some abbreviations:
   - A negation [~A] (written <<~A>>) is just a shortcut 
   for [A->False]. 
   Hence introduction via [intro] (or [intros a], giving a 
   name forces the introduction) and elimination via 
   [apply H] or [destruct H] (whether one wants to focus 
   on the underlying [->] or [False]).
   - An equivalence [A<->B] (written <<A<->B>>) is just a 
   shortcut for [(A->B)/\(B->A)], hence is manipulated as a 
   conjunction of implications.
- Some automatic tactics : [trivial], [easy], [auto], [eauto], 
  [intuition], [firstorder]. See the Coq documentation for 
  more details. 

In today practical session, you will first try to prove the 
statements with no or little automatisation, and then 
experiment with these tactics in a second time. Indeed, it 
can be helpful later to know how to proceed steps-by-steps, 
since real-life proofs are seldom doable by just automatic 
tactics.


*)

(** ** Some more tactics


In Coq, a definition [d] could be replaced by its body 
thanks to the tactic [unfold d].

The equality is handled by tactics:
 - [reflexivity]
 - [symmetry]
 - [transitivity ...] (where you need to give the 
  intermediate term) and 
 - [rewrite ...] (give the equality name or lemma to use 
  as a rewrite rule) or 
 - [rewrite <- ...] (right-to-left rewriting)

Inductive reasoning will be studied at length in coming 
lectures. Just to state it immediately: 
 - [induction ...] (to do inductive reasoning, to be 
  investigated in the next lectures).


*)


(** * Summary of elementary tactics

In the previous lecture, we considered some elementary tactics, 
with no or only very little automatisation.

#<img src="https://www.irif.fr/_media/users/saurin/teaching/lmfi/coq-2023/basic-types-cpm.png" 
width="100%" 
alt="Some basic types (from CPM)">#


#<img src="https://www.irif.fr/_media/users/saurin/teaching/lmfi/coq-2023/propositional-form-cpm.png" 
width="100%" 
alt=" Writing of propositional formulas (from CPM)">#


#<img src="https://www.irif.fr/_media/users/saurin/teaching/lmfi/coq-2023/quantif-cpm.png" 
width="100%" 
alt="Quantifiers (from CPM)">#


Here is a reminder:

- [assumption] if the current goal is exactly one of the 
 hypothesis (cf. the _axiom rule_ in logic).
- For all primitive connectors (quantification [∀], 
 implication [->]):
   - introduction via [intro] (or one of its variants [intros], 
   [intro x], [intros x y ...])
   - elimination via [apply H] (where [H] is the name of 
   the hypothesis to eliminate).
- The other connectors (which are actually inductive definitions) 
  may ideally be introduced by [constructor] and eliminated by 
  [destruct H].
  But the introduction frequently requires more ad-hoc tactics:
   - [split] for a conjunction [/\] (written <</\>>)
   - [left] and [right] for a disjunction [\/] (written <<\/>>)
   - [exists ...] (written <<exists>>) for a quantification [∃] 
   (where [...] is the place where the existential witness 
   is given)
   - No introduction tactic for [False] !
   - For [True] (seldom used in Coq), the introduction can 
   be done via [constructor], but nothing to eliminate.
- The [simpl] tactic performs a conversion on the goal. 
- In Coq, a definition [d] could be replaced by its body 
  thanks to the tactic [unfold d].
- Some abbreviations:
   - A negation [~A] (written <<~A>>) is just a shortcut for [A->False]. 
   Hence introduction via [intro] (or [intros a], giving a 
   name forces the introduction) and elimination via [apply H]
   or [destruct H] (whether one wants to focus on the 
   underlying [->] or [False]).
   - An equivalence [A<->B] (written <<A<->B>>) is just a shortcut for [(A->B)/\(B->A)], 
   hence is manipulated as a conjunction of implications.
- Some automatic tactics : [trivial], [easy], [auto], [eauto], 
  [intuition], [firstorder]. See the Coq documentation for 
  more details. 

#<img src="https://www.irif.fr/_media/users/saurin/teaching/lmfi/coq-2023/intro-elim-cpm.png" 
width="100%" 
alt="Tactics for introducting and eliminating logical connectives (from CPM)">#

*)

(** * Today's program

Today, we will mostly focus in equality and induction 
plus some additional useful features. Here is a little 
warm-up betfore we enter the details: 


The equality is handled by tactics:
 - [reflexivity]
 - [symmetry]
 - [transitivity ...] (where you need to give the intermediate 
 term) and 
 - [rewrite ...] (give the equality name or lemma to use as a 
 rewrite rule) or 
 - [rewrite <- ...] (right-to-left rewriting)

Inductive reasoning will be studied at length in coming 
lectures. Just to state it immediately: 
 - [induction ...] (to do inductive reasoning, to be 
  investigated in the next lectures).


*)

(** * Managing the interactive proof progresses

 - [Undo]: goes back one tactic
 - [Restart]: restart the proof from the original goal
 - [Abort]: Aborts the current proof. 
 - [Admitted]: gives up the current proof and declares the initial goal as an axiom.
 - Focusing using bullets or [i: { tac }]
 - Applying a tactic to the i-th goal: [i:tac], ...
 - Modifying the goal list: [all: swap i j]
 - Getting information: 
   - [Show] display the current goals;
   - [Show Proof] displays the current proof term (with holes for open goals).
**)

(** * Composing tactics 




#<img src="https://www.irif.fr/_media/users/saurin/teaching/lmfi/coq-2023/composition-tacticals-cpm.png" 
width="100%" 
alt="Tacticals for composing tactics (from CPM)">#

Some more: 

- [tac ;[tac1 |. . .|tacn]]: applies tac to the goal which generates n subgoals
  and taci is applied to the i-th subgoal.

- [idtac]: always succeed, leaving the goal unchanged.

- [fail]: always fail (useful to implement a tactic saying: 
  "I solve the goal immediatly or I fail" with [tac; fail]. 
  Indeed, if [tac] fails, it fails, if [tac] succeeds producing at least one subgoal, 
  it fails and if [tac] suceeds producing no subgoal, it succeeds. 

- [try]: it behaves like [tac || idtac]. It is often useful to do [tac ; try tac'.]
  Another example is [tac; try (tac'; fail).] this tries applying 
  [tac'] to the subgoals generated by [tac] but only to those which are completely 
  solved by [tac'], leaving the other ones unchanged.


**)


(** * A tiny bit of automation: [auto] and [trivial]

[auto] recursively composes [intros], [apply] and [assumptions] until a given depth
(by default, it is 5). Either it completely solves the goal, or it leaves the 
goal unchanged.

[trivial] is less powerfull that [auto] but it emphasizes that concluding is easier.

*)

(** * Searching for Lemmas


*)

About nat.

About O.



Search 0.

Search "_comm".

(** It is possibl to search by describing a pattern 
of what you are looking for: *)

Require Import ZArith.

SearchPattern (_ + _ <= _)%Z.
SearchPattern (_ * _ <= _ * _)%Z.

(** also non linear patterns: *)

SearchPattern (?X1 * _ <= ?X1 * _)%Z.

SearchPattern (?X1 * _ <= ?X1 * _).



(** Searching only for equalities: *)

SearchRewrite (1 * _)%Z.

SearchPattern (_ = 1 * _ )%Z.

(** * Equality: eq *)

(**
#<img src="https://www.irif.fr/_media/users/saurin/teaching/lmfi/coq-2023/equality-tactics-cpm.png" 
width="80%" 
alt="Tactics for equality (from CPM)">#

**)


Print eq.

(** Syntactic equality : only x is equal to x 
<<
Inductive eq (A : Type) (x : A) : A -> Prop :=  eq_refl : eq A x x.
>> 
constructor is the reflexivity rule. 
tactic "reflexivity" *)

Lemma compute_equal : 2+2 = 4.
Proof.
simpl. 
reflexivity.
Qed.

Check eq_ind.

(** The Leibniz principle, or the rewrite principle :

<<
eq_ind
     : forall (A : Type) (x : A) (P : A -> Prop),
       P x -> forall y : A, x = y -> P y
>>

*)
(** match on a equality is a form of rewrite
   the rewrite tactic proceed by a match on the equality *)

Print eq_ind.

Lemma eq_sym A (x y : A) : x = y -> y = x.
Proof.
 intro H. Show Proof.  
destruct H. Show Proof. reflexivity.
Qed.
Print eq_sym.

(** ** rewriting equalities 

If [H : a = b] is in the context, then [rewrite H]
rewrites occurrences of  [a] into [b]

On the other hand, [rewrite <- H]
rewrite [b]s into [a]s.

*)

Open Scope Z_scope. 

Theorem Zmult_distr_1: forall n x: Z, n * x + x = (n+1)* x.
Proof.
intros n x. 
Check Zmult_plus_distr_l.
rewrite Zmult_plus_distr_l.
rewrite Zmult_1_l.
reflexivity.
Qed.

(** It is possible to rewrite in an hypothesis with 
[rewrite H in H'].
*)


(** ** The [pattern] tactic:

When there are multiple occurrences of a terme 
and one wants to rewrite just one, the pattern tactic comes in handy:

[pattern t at i j k.]

*)

Theorem regroup : forall x: Z, x+x+x+x+x = 5 * x.
Proof.
pose Zmult_1_l as H1.
pose Zmult_distr_1 as H2.
intro x. 
(* rewrite <- H1. *)
pattern x at 1.
rewrite <- H1.
repeat rewrite H2.
reflexivity.
(* trivial. *)
(* auto.*)
Qed.


(** ** Conditional rewrites 

When using a rewrite on a conditional goal, 
that may generate subgoals. 
*)


Require Import Arith.
Open Scope nat_scope.

Section Condrewrite.
Hypothesis le_lt_S_eq : forall n p: nat,
n <= p -> p < S n -> n=p.
 
Lemma cond_rewrite_example: forall n: nat, 
8 < n+6 -> 3+n < 6 -> n*n = n+n.
Proof.
intros n H H0.
pose (le_lt_S_eq 2 n) as H2.
rewrite <- H2.
reflexivity. 
(* simpl; auto. *)
apply Nat.add_le_mono_l with (p := 6).
rewrite Nat.add_comm in H; simpl; auto with arith.
apply Nat.add_le_mono_l with (p :=3); auto with arith.
Qed.

End Condrewrite.




(** * nat and induction *)

Print nat.
Check nat_ind.
Print nat_ind. (* fixpoint + match *)


Lemma test_induction1 : forall n:nat, n=n.
intro n. 
elim n.
- reflexivity.
- intros n0 Hrec.
reflexivity.
Qed.

Lemma test_induction2 : forall n:nat, n=n.
intro n. 
destruct n.
- reflexivity.
- reflexivity.
Qed.

Lemma test_induction3 : forall n:nat, n=n.
induction n.
- reflexivity. 
- reflexivity.
Show Proof.
Abort.


Lemma test_induction4 : forall H: Prop, H -> forall n:nat, n=n.
induction n.
- reflexivity. 
- reflexivity.
Show Proof.
Abort.


Print "+".

(**
<< 
2+2 = Nat.add (S (S 0)) 2.
    = match S (S 0) with
      | 0 => m
      | S p => S (add p 2)
     (* unfold rule for fixpoint : a fixpoint applied to a constructor can
        unfold once *)
    = S (add (S 0) 2)
    = ...
    = 4
>>
*)
Compute 2+2.

Require Import Arith.
Check Nat.add_succ_l.

Lemma non_compute_proof : 2+2 = 4.
Proof.
 rewrite Nat.add_succ_l.
 rewrite Nat.add_succ_l.
 rewrite Nat.add_0_l.
 reflexivity.
Qed.
Print non_compute_proof.
Print Nat.add_succ_l.


Lemma compute_proof : 2+2 = 4.
(* in coq, most of the time we're modulo computation :
   2+2 just the same as 4 *)
 simpl. (* force a computation *)
 reflexivity.
Set Printing Implicit.
Show Proof.
Check (@eq_refl nat 4).
(* 2+2=4 and 4=4 are the *same* statement (modulo computation) :
    (what we call *convertible* )
   they can be proved by the *same* proof term *)
Qed.

Lemma compute_proof' : 2+2 = 4.
reflexivity.
Qed.

Definition compute_proof'' : 2+2 = 4 := eq_refl.
                                 (* or more precisely @eq_refl nat 4 *)

