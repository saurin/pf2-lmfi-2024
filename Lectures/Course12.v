
Module Axioms.
 
Axiom EM : forall P : Prop, P \/ ~ P.

  
Theorem NNPP : forall P : Prop, ~ ~ P -> P.
Proof.
  intros.
  destruct (EM P) as [HP|nHP].
  - assumption.
  - exfalso.
    apply H; assumption.
Qed.

Check Choice. About Choice.

End Axioms.

Module Forward.

Theorem imp_trans2 : forall P Q R S : Prop,
  (P -> Q) -> (Q -> R) -> (R -> S) -> P -> S.
Proof.
  intros P Q R S.
  intros HPQ HQR HRS HP.
  apply HRS.
  apply HQR.
  apply HPQ.
  exact HP.
Qed.


Theorem imp_trans2' : forall P Q R S : Prop,
  (P -> Q) -> (Q -> R) -> (R -> S) -> P -> S.
Proof.
  intros P Q R S HPQ HQR HRS HP. 
  apply HPQ in HP as HQ.
  apply HQR in HQ as HR.
  apply HRS in HR as HS.
  exact HS.
Qed.

Require Import Rbase Classical.
Open Scope R_scope.

Theorem double_fixpoint_0 :
  forall (x : R), x + x = x -> x = 0.
Proof.
  intros x H.
  Check Rplus_eq_reg_l.
  Check Rplus_0_r.
  apply (Rplus_eq_reg_l x).
  rewrite Rplus_0_r.
  exact H.
Qed.


Theorem double_fixpoint_0' :
  forall (x : R), x + x = x -> x = 0.
Proof.
  intros x H.
  pose Rplus_eq_reg_l as H1.
  pose Rplus_0_r as H2.
  rewrite <-H2 in H.
  apply H1 in H. 
  exact H.
Qed.


End Forward.

Section Coercion.
(*Borrowed from Assia Mahboubi course, where you will also find
additional explanations *)

  (*http://www-sop.inria.fr/teams/marelle/vu-2020/lesson6.html*)
Check false : bool.

From mathcomp Require Import ssreflect ssrbool.

Check false : Prop.

Set Printing Coercions.

Check false : Prop.

Print is_true.

Variables (A B C: Set) (a : A).

Variable f : A -> B.
Variable g : B -> C.

Fail Check a : C.

Coercion f : A >-> B.
Coercion g : B >-> C. 
Check a : C.

Unset Printing Coercions.

End Coercion.


Section Unification.

From mathcomp Require Import ssreflect ssrbool ssrnat.
  
Record pos_nat : Set :=
  PosNat {val : nat; pos_val : 1 <= val}. (*Dependent pair*)

Lemma pos_S (x : nat) : 1 <= S x.
Proof. by []. Qed.

Definition pos_nat_S (n : nat) : pos_nat
  := PosNat (S n) (pos_S n).

Check pos_nat_S.
 
Fail Lemma pos_nat_add (x y : pos_nat) : 1 <= x + y.

(* The coercion allows to see any pos_nat as a natural number *)
Coercion val : pos_nat >-> nat.

(* Now the following type checks and we can prove it *)
Lemma pos_add (x y : pos_nat) : 1 <= x + y.
Proof.
  Check addn_gt0. rewrite addn_gt0.
  apply/orP.
  left. 
  destruct x. 
  simpl. 
  assumption. 
Qed. 


(* But coercions are not enough. We also want a "canonical" positive
natural number preserved through the usual operations on natural
numbers *)

Definition pos_nat_add (x y : pos_nat) : pos_nat
  := PosNat (val x + val y) (pos_add x y).


Variable P : nat -> Prop.


Hypothesis posP1 : forall n : nat, 0 < n -> P n.

Hypothesis posP2 : forall p : pos_nat, P p.

Set Printing Coercions.

About posP2.


Lemma Pexample1 (x : nat) : P (S x + 3).
Proof.
apply posP1.
rewrite addn_gt0.
simpl. compute. reflexivity. 
Qed.


Lemma Pexample2 (x : nat) : P ((S x) + 3).
Proof.
Fail apply: posP2.
Abort.

Goal forall x y : pos_nat, val x + val y = val (pos_nat_add x y).
by [].
Qed.

Canonical pos_nat_add.
Check pos_nat_add. 

Lemma Pexample2' (x y : pos_nat) : P (x + y).
Proof.
apply: posP2.
Qed.


Lemma Pexample2 (x : nat) : P ((S x) + 3).
Proof.
Fail apply: posP2.
Abort.


Goal forall n : nat, S n = val (pos_nat_S n).
Proof. by []. Qed.

Check pos_nat_S. 
Canonical pos_nat_S.

Lemma Pexample2'' (n : nat) : P (S n).
Proof.
apply: posP2.
Qed.

Lemma Pexample2 (x : nat) : P ((S x) + 3).
Proof.
apply: posP2.
Qed.

End Unification.


Module MathComp.
(* Inspired from the MathComp Winter School: 
http://www-sop.inria.fr/teams/marelle/advanced-coq-17/lesson5.html
*)
Section CanStructure. 

From mathcomp Require Import all_ssreflect ssrbool ssrnat.
Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Record mixin (A:Type) := Mixin {
                             my_dot : A -> A -> A;
                             my_one : A;
dot_assoc : forall x y z:A,
my_dot x (my_dot y z) = my_dot (my_dot x y) z;
unit_left : forall x, my_dot my_one x = x;
unit_right : forall x, my_dot x my_one = x;
 }.

Definition my_class := mixin.

Structure monoid := Monoid {
 dom :> Type;
 class:>  mixin dom}.                       

Record my_abb_mixin (m: monoid) := Abb_Mixin {                                       
com : forall x y , @my_dot m (class m) x  y = @my_dot m (class m) y x}.                                    

Record abb_class (A : Type) := Abb_class {
base_abb :>  my_class A ; (* the :> adds a coercion *)
abb_mixin :> my_abb_mixin (@Monoid A (base_abb))}.

Structure abb_monoid := Abb_monoid {
  dom_abb  :> Type;
  class_abb :> abb_class dom_abb}.

Canonical abb_mono_mono (s : abb_monoid) : monoid  := 
  @Monoid (dom_abb s) (base_abb (class_abb s)).


Definition dot {s : monoid} := my_dot (class s).
Definition one {s : monoid} := my_one (class s).
Notation "x * y" := (dot x y).
Notation "1" := one.


Lemma inverse_left (s : monoid) (x :s) : 1 * x = x. 
Proof.
   by case: s x ; move => //= ? [].
Qed.       


End CanStructure.  


Section tactics.

Require Import Rdefinitions.
From mathcomp Require Import all_ssreflect ssralg ssrint ssrnum finmap matrix.
From mathcomp Require Import rat interval zmodp vector fieldext falgebra.
From mathcomp Require Import mathcomp_extra boolp classical_sets functions.
From mathcomp Require Import cardinality set_interval.

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Import Order.TTheory GRing.Theory Num.Theory.


(* Feel free to play with file while I'm talking !*)



(** MOVES **)


Lemma divand : forall (m n : nat), 2 %| n /\ 3 %| m -> 6 %| n * m .
Proof.
move=> m n. move=> H.
move: H. move=> [A B].
Admitted.

Lemma divor : forall (m n : nat), 2 %| n \/ 3 %| m -> 6 %| n * m .
Proof.
move=> m n. move=> H.
move: H. move=> [A | B].
Admitted.


Lemma div1 : forall (n : nat), 6 %| n -> 2 * 3 %| n .
Proof.
move=> n n6.
have H : 2 * 3 = 6. move=> //=.
move=> {H}. (* let's forget about H to prove H in another way *)
have H : 2 * 3 = 6 by []. (* even quicker *)
rewrite H.
move=> //.
Admitted.


Lemma div2 : forall (n m : nat), 6 %| n -> 2*3 %| n .
Proof.
move=> n m n6. move=> {m}. (* m is not needed *)
Admitted.

Lemma div3: forall (n m : nat), 6 %| n -> 2*3 %| n .
Proof.
move=> n _ n6. (* m is not needed, let's forget about it *)
Admitted.

(** REWRITES **)

(* One can rewrite thanks to an equality, going forward or backward *)
Lemma rewrite1 (n m : nat) : n = m -> 1 + n = 1 + m.
Proof.
move=> H. rewrite H. by []. (* n is replaced by m *)
Qed.

Lemma rewrite_1 (n m : nat) : n = m -> 1 + n = 1 + m.
Proof.
move=> H. rewrite -H. by []. (* now m is replaced by n *)
Qed.

(* We can start using ; to chain several commands on the same goal *)
Lemma rewrite2 (n m : nat) : n = m -> 1 + n = 1 + m.
Proof.
move=> H; rewrite H => //=.
Qed.

(* Notice that //= can be put directly after a rewrite in place *)
Lemma rewrite3 (n m : nat) : n = m -> 1 + n =  1 + m.
Proof.
move=> H; rewrite H //=.
Qed.

(* The prefered way is to conclude a proof writing "by ..." *)
Lemma rewrite4 (n m : nat) : n = m -> 1 + n =  1 + m.
Proof.
by move=> H; rewrite H.
Qed.

(* In fact, we do not want to bother introducing H, just write "->" instead of H*)
Lemma rewrite_directly (n m : nat) : n = m -> 1 + n =  1 + m.
Proof.
by move=> ->.
Qed.

(* And it works backward also "<-" *)
Lemma rewrite_directly_backward (n m : nat) : n = m -> 1 + n = 1 + m.
Proof.
by move=> <-.
Qed.

(* Sometimes we want to chain several lemmas, and we can*)

Lemma rewrite_a_lot (n m p q : nat) : n = m -> q = p -> p + n = q + m.
Proof.
by move=> -> ->.
Qed.

(* This is not the only thing you might want to do. Sometimes you do
need to rewrite thanks to an auxiliary hypothesis or lemma. You might
need to explicitely name the lemma, but for maintenance purpose you
might not want to *)
Local Open Scope ring_scope.

Lemma subterm_selection (R : ringType) (p q : R) :
  (p + q + 1) * q = q * (q + p) + q.
Proof.
rewrite addrC.
rewrite (addrC q).
rewrite [_ + q]addrC.
rewrite [in q * _]addrC.
rewrite [X in _ = _ * X + _]addrC.
rewrite [in RHS]addrC.
Abort.


(* Let's go into vector spaces for a bit, and let's learn how to use "Search" *)


Search
(* Maybe on words I may want to find in the name of the lemma *) "mul"
(* then maybe a list of patterns I want to find *)(_ * _) (2 + _ = 1 +_).
(*(* then maybe a library into which I want to specifically look *) in topology.*)


Lemma scalar_mult (R : fieldType) (E : lmodType R) (z : E) (r : R) :
r != 0 -> z = r *: (r^-1 *: z).
Proof.
move=> r0.
rewrite /(_ *: _). (*we might now want to do that, let's wrap it again*)
rewrite -/(_ *: _). (*and again*)
rewrite -!/(_ *: _).
(*we could just have written :rewrite -!/(_ *: _).*)
(* But now we know how this is called "scale". It is likely to be present in the
lemmas on that notion *)
Search "scale" (_ *: _).
Search (_ *: (_ *: _)).
rewrite scalerA.
Search (_ / _ = 1).
rewrite mulfV.
rewrite scale1r //.
by [].
(* In short, one could have used the ? sign to try to rewrite scale1r on all
subgoals before concluding with //*)

(*by move=> r0; rewrite scalerA mulfV ?scale1r.*)
Qed.

(*Now thanks to rewrite and to an externa lemma we can proove our first lemma*)
Check Gauss_dvd.
Check dvdn_mulr.
Check dvdn_mull.

Lemma divand_full m n : 2 %| n /\ 3 %| m -> 6 %| n * m .
Proof.
move=> [dvd2n dvd3n].
rewrite (@Gauss_dvd 2 3)//.
by rewrite dvdn_mulr// dvdn_mull.
Qed.


(** SEARCH **)

Close Scope ring_scope.

(** VIEWS AND APPLY **)


Lemma applied (P Q : Prop): (P -> Q) -> P -> Q.
Proof.
move=> H HP; apply: H. by [].
(* by move=> H HP; apply: H *)
Qed.

(* One can also use views, meaning applying lemmas directly to hypothesis, while
introducing them. This is done thanks to "/lemma" *)

Lemma applied_view (P Q : Prop): (P -> Q) -> P -> Q.
Proof.
move=> H. move=> /H. by [].
(* by move=> H /H.*)
Qed.

(*Views can also be used with equivalences. You don't need to chose implication to use*)
Lemma applied_eq (P Q : Prop): (P <-> Q) -> P -> Q.
Proof.
by move=> H /H.
Qed.

(* Sometimes it's easier to feed an argument to a lemma than to apply the lemma.
This is done thanks to /(_ a) when the lemma is on top of the stack *)
Lemma applied_arg (P Q : Prop): P -> (P -> Q) -> Q.
Proof.
move=> HP /(_ HP).
by [].
Qed.

(* Look at what + is doing *)

Lemma applied_plus (P Q : Prop): (P -> Q) -> P -> Q.
Proof.
move=> + HP. move=> /(_ HP).
by [].
Qed.

(** CASE **)

(* case=> destructs an hypothesis while putting it in the context,
case:_ destruct inductive proposition while taking it from the context *)

Lemma ex_elim (m : nat) : (exists n : nat, m = S n) -> 0 < m.
Proof.
move=> [k hk]. (* `k` is the witness, `hk` is its property *)
rewrite hk. by []. (* That's something hard encoded in the ssrnat library *)
Qed.

Lemma case_bool (b1 b2 : bool) : b1 || ~~ b1.
Proof. by case: b1. Qed.

Lemma case_nat (n: nat) : (n= 0) \/ (0<n).
Proof. case: n; first by left. by move=> n; right. Qed.

(* Let's switch to more intricate objects *)

(** Number Theory **)
(* Prime numbers *)

From mathcomp Require Import all_ssreflect ssrbool ssrnat.

(* First, let's talk about small scale reflection, an important feature in Mathcomp. 
Several lemmas make a bridge between the boolean and the Propositional world*)

Print reflect. 
Check andP.
Check iffP.
Check idP.

Fixpoint eqn m n :=
  match m, n with
  | 0, 0 => true
  | j.+1,k.+1 => eqn j k
  | _, _ => false
  end.
Arguments eqn !m !n.

Locate prime.

(* Below, the commented proof is a short version of what's above. You
can comment the proof and uncomment the short proof, and see that it
works *)

Lemma myeqP m n : reflect (m = n) (eqn m n).
Proof.
Check iffP. Check idP.
apply: (iffP idP). 
 elim: m n; first by case.
 move=> n IHn m eq_n1m.
 case: m eq_n1m => // m eq_n1m1.
 congr (_.+1).
 exact: IHn.
move  => ->.  elim: n. by []. by []. 
(*apply: (iffP idP) => [|->]; last by elim: n.
by elim: m n => [|m IHm] // [|n] // /IHm->.*)
Qed.

Lemma test_leqW i j k :
  (i <= k) && (k.+1 <= j) -> i <= j.+1.
Proof.
Check leqW. Check leq_trans. 
(* move=> /andP. case. move=> /leqW. move=> leq_ik1. *)
move=> /andP[/leqW leq_ik1 /leqW leq_k1j1].
exact: leq_trans leq_ik1 leq_k1j1.
Qed.

Check predn. 
  
Lemma test_leq_cond p : prime p -> p.-1.+1 + p = p.*2.
Proof.
move=> pr_p.   
Search _ predn in ssrnat.
rewrite prednK.
  by rewrite addnn.
Search _ prime leq 0.
by apply: prime_gt0.
(*by move=> pr_p; rewrite prednK ?addnn // prime_gt0.*)
Qed.

End tactics.


Section analysis.
(** Analysis **)

(* To compile this part on your computer you'll need to install Mathcomp-Analysis*)

(*
opam repo add coq-released https://coq.inria.fr/opam/released
opam install coq-mathcomp-analysis
*)

Require Import Rdefinitions.
From mathcomp Require Import all_ssreflect ssralg ssrint ssrnum finmap matrix.
From mathcomp Require Import rat interval zmodp vector fieldext falgebra.
From mathcomp Require Import mathcomp_extra boolp classical_sets functions.
From mathcomp Require Import ereal reals signed topology prodnormedzmodule normedtype.



Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Import numFieldTopology.Exports.
Import Order.TTheory GRing.Theory Num.Theory.

Local Open Scope classical_set_scope.
Local Open Scope ring_scope.

(* Continuity uses the limits --> notation, wich is just about filter inclusion.
The notation F --> x where F is a filter. This is a notation for (nbhs x) `<=`
F, the canonical filter of neighborhoods of x is included in F *)

(* The notation f @` A is used to denote the image of A : set E by f : E -> F *)

(* Continuity at x for a function f is defined as : f `@ x --> f x, meaning the
filter of neighborhoods of (f x) is included in the image by f of the filter of
neighborhoods of x *)

Lemma add_continuous_without_near (R : numFieldType)  (V : normedModType R): 
continuous (fun z : V * V => z.1 + z.2).
Proof.
move=> []. (* Let's destruct the continuous predicate*)
move=> x y /=. (* Let's introduce points*)
move=>A.
(* I'm just introdcing the appropriate neighborhood as I want to work on the statement
 that it is a neighborhood *)
move=>/nbhs_ballP. (* This allows me to say that A contains a ball around (x,y)*)
move=> [] /=.  (*And I just destruct the statement *)
move=> r r0 H.
apply/nbhs_ballP=>/=.
exists (r/2).
   Search ( 0 < _ /_). (* now I see that all I need is divr_gt0, 
   and that the other results should be infered from the context or computed.*) Search "divr_gt0". 
   by apply: divr_gt0. 
(* Now we need to prove an inclusion of sets: this is what the notaion `<=` is for*)
move=> /= z. 
(* Let's destruct what a ball is a product space is*)
move=> [] /=. 
(* We might want to rewrite this ball with a norm. This is done trough the
ball_normE lemma and computation *) 
Check ball_normE.
rewrite -ball_normE /=.
move=> Bx By.
apply: H. 
rewrite -ball_normE /=.
(* Now I'll use a lemma allowing me to split the norm in two, but before that
I'll need to do some rewriting *) Search "opprD". 
rewrite opprD.
rewrite addrACA.
by rewrite normm_lt_split.
Qed.

(* In short and without near, this would give:*)
Lemma add_continuous_short (R : numFieldType)  (V : normedModType R): 
continuous (fun z : V * V => z.1 + z.2).
Proof.
move=> [x y /=] A => /nbhs_ballP [/= r r0] H.
apply/nbhs_ballP=>/=.
exists (r/2); first by apply: divr_gt0.
move=> /= z []; rewrite -ball_normE /= => Bx By. 
by apply: H; rewrite -ball_normE /= opprD addrACA normm_lt_split.
Qed.

(* With near, we avoir or delay the explicit handling of (r/2)*)
Lemma add_continuous_with_near (R : numFieldType)  (V : normedModType R): 
continuous (fun z : V * V => z.1 + z.2).
Proof.
move=> [/= x y]; apply/cvgrPdist_lt =>/= e e0.
near=> a b => /=.
rewrite opprD addrACA normm_lt_split //.
  by near:a; apply: cvgr_dist_lt => //; apply: divr_gt0.
by near:b; apply: cvgr_dist_lt => //; apply: divr_gt0.
(*[near: a|near: b]; apply: cvgr_dist_lt => //; apply: divr_gt0.*)
Unshelve. all: by end_near. Qed.

(*For this short proof, we have not gained a lot. The posNum type allows to
automatically infer positivity and to avoir the two last lines*)

Lemma add_continuous_with_near_and_pos (R : numFieldType)  (V : normedModType R): 
continuous (fun z : V * V => z.1 + z.2).
Proof.
move=> [/= x y]; apply/cvgrPdist_lt=> _/posnumP[e]; near=> a b => /=.
by rewrite opprD addrACA normm_lt_split.
Unshelve. all: by end_near. Qed.


(*For short proofs this might not seem important. 
For long proofs, it makes a huge difference*)

Lemma continuous_bounded (R : numFieldType) (V W : normedModType R)
    (x : V) (f : {linear V -> W}) :
  {for 0, continuous f} -> bounded_near f (nbhs x).
Proof.
rewrite /prop_for /continuous_at linear0 /bounded_near => f0.
near=> M; apply/nbhs0P.
 near do rewrite /= linearD (le_trans (ler_normD _ _))// -lerBrDl.
apply: cvgr0_norm_le; rewrite // subr_gt0.
by []. (* This is were it happens*)
Unshelve. all: by end_near. Qed.
  


End analysis.
      

End MathComp.
