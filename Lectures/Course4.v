(** * Course4 : Introducing dependent types in Coq **)


(** * Preliminaries

The Coq file supporting today's course is 
available at 

#<a href="https://gitlab.math.univ-paris-diderot.fr/saurin/pf2-lmfi-2024/-/blob/main/Lecture/Course4.v">https://gitlab.math.univ-paris-diderot.fr/saurin/pf2-lmfi-2024/-/blob/main/Lecture/Course4.v</a>#.


And the standalone exercise sheet is available at: 
#<a href="https://gitlab.math.univ-paris-diderot.fr/saurin/pf2-lmfi-2024/-/blob/main/TP/TP4.v">https://gitlab.math.univ-paris-diderot.fr/saurin/pf2-lmfi-2024/-/blob/main/TP/TP4.v</a>#.



Today, we shall move to a new aspects of Coq's 
type theory, which distinguishes from most 
programming languages and makes it so expressive: 
_dependent types_.
We shall start investigating the structure of 
Coq's type system and especially the need for 
a hierarchy of universes in which types can 
live.

Recall that from the start of the semester, we 
studied the following basic commands and constructs:

 - [Definition] : binds some name with a 
   (non-recursive) Coq term as well as the 
   [let ... in] local definition construct;
 - [Fixpoint] : same, for a recursive definition
   as well as the [fix] local recursive definition
   construct;
 - [Inductive] : creates a new inductive type 
   and its constructors;
 - [Check] : displays the type of a Coq term;
 - [Print] : displays the body of a definition 
   or the details of an inductive type;
 - [Compute] : reduces a term (i.e. determines 
   its normal form) and prints it.
 - [Section] : the Section mechanism.
 - [fun args => body] and [fix ident args := body] 
   to define nameless functions (resp. recrusive 
   functions)
 - we touched upon _inductive predicates_ but we will
   come back to them later when your understnding of 
   Coq's type theory will be sufficient to fully grasp 
   what they are.  

 As well as the basic constructs on types derived 
 from the simply typed lambda-calculus. 

*)

(* *)





(** * Some more details on Coq types

Contrarily to most type systems in which type 
are concepts of a different kind than terms, 
there is no essential syntactic differences 
between Coq types and the other terms : 
in Coq everything is term (unlike in OCaml, 
for instance).

Morally, a type is just something that may occur 
on the right of a typing judgment [Γ ⊢ t:σ].

Now, since a type is in particular a Coq term, 
it should have a type. In Coq a important property 
is that whenever [Γ ⊢ t:σ] and [Γ ⊢ σ:s], 
then [s] is necessarily a _sort_ (or universe), 
i.e. one of the constants [Type] or [Set] or 
[Prop]. This gives us a concrete definition 
of a Coq type : 
_a type is anything whose type is a sort_.

- [Type] is the most general sort in Coq.
- [Prop] is used to express logical statements, we 
  will encounter it in the second half of this 
  course.
- [Set] is a (deprecated) alias for a [Type] _of 
  the lowest level_.
 
To avoid paradoxes coming from [Γ ⊢ Type : Type], 
actually [Type] is not just _one_ universe but a 
hierarchy of universes: 
[[
Type₀ : Type₁ : Type₂ : ...
]]
Normally these indices are hidden to the users, 
and we will not say more about that today.


In Coq, the arrow type [A->B] is actually not a 
primitive construction, it is a particular case of 
a _product_ [∀x:A,B] (Coq syntax [forall x:A, B]). 
When variable [x] does not occur in [B] 
(non-dependent product), we write [A->B]. For a 
first example of dependent product, we have already 
seen the identity last week the first argument of 
which was a type specifying the type of the produced 
identity function.

Roughly, the typing rule for a product looks like :

- If [Γ ⊢ A:Type] and [Γ+x:A ⊢ B:Type] then 
  [Γ ⊢ (forall x:A,B) : Type]

In reality, one must take care of the indices of 
the [Type] universes : the rightmost index is _the 
max of the other two indices_:

- If [Γ ⊢ A: Type(i)] and [Γ+x:A ⊢ B:Type(j)] 
then [Γ ⊢ (forall x:A,B) : Type(max(i,j))]

Or, written as an inference rule: 

<<
Γ ⊢ A:Type(i)         Γ+x:A ⊢ B:Type(j)
________________________________________(Prod-Type)
  Γ ⊢ (forall x:A,B) : Type(max(i,j))
>>


(Note that this typing rule has a particular case 
for [Prop] that we will not detail here but will 
come back to it in a future part of the course.)

We can now generalize the typing rules for 
functions and applications:

- If [Γ+x:A ⊢ t:B] then [Γ ⊢ (fun x => t): 
  (forall x:A, B)]
- If [Γ ⊢ t:(forall x:A,B)] and [Γ ⊢ u:A] 
  then [Γ ⊢ (t u) : B{x:=u}]

Or, written as inference rules: 

<<
       Γ+x:A ⊢ t:B
__________________________________(Lam)
 Γ ⊢ (fun x => t):(forall x:A, B)

Γ ⊢ t:(forall x:A,B)         Γ ⊢ u:A
_____________________________________(App)
       Γ ⊢ (t u) : B{x:=u}
>>


Note that for a non-dependent product, we recover 
the former rules for [A->B]. In particular for 
(App), if [x] does not occur in [B] then 
[B{x:=u} = B].

*)

(* *)


(** * Some explanations about implicit arguments *)

(** You already noticed that the type notations in [Coq] 
can be very heavy especially to handle type instanciation 
in case of polymorphic types and all the more when 
starting working with dependent types as we shall do today. 

An essential feature of Coq to work smoothly with those types is
the ability to declare some arguments as being implicit and let Coq 
infer the corresponding argument. 
*)

Definition compose A B C (f: B -> C) (g: A -> B) : A -> C :=
fun x => f (g x).

Check compose.
Print compose. 

(** An option, is, at the time of the definition, to specify 
which argument is implicit: *)

Definition compose_impl {A B C} (g: B -> C) (f: A -> B) : A -> C :=
fun x => g (f x).

Print compose_impl. 

(** Another option is to automatically declare arguments 
as implicit when they can be inferred as such: *)

Set Implicit Arguments.

Definition compose' A B C (f: B -> C) (g: A -> B) : A -> C :=
fun x => f (g x).

Check compose'.
Print compose'. 

 Definition thrice (A:Set)(f:A->A) := 
compose A A A f (compose A A A f f).

Definition thrice' (A:Set)(f:A->A) := 
compose' f (compose' f f).

Check thrice'.
Print thrice'.

Unset Implicit Arguments.

(** One can deactivate implicit arguments by using [@]: *)

Check compose_impl.

Check @compose_impl.

(** It can also happen that one need to explicitly provide explicit 
arguments, for instance because of a partial application of arguments: *)

Check (compose' S).

Check (compose' (A := nat) S).




(** * Doing arithmetics without inductive types

We have seen so far that inductive types are 
very convenient to represent lots of data 
structures and in particular to work with natural 
numbers; be they represent as unary Pean integers 
or using some numeric basis. 

We shall do some arithemtic without inductive types 
using an encoding of natural numbers due to Church.
For this, we come back to Exercise 3 of the exercise 
list and some extensions.
*)

(** **** Exercise 3 : Church numerals

Encode in Coq the Church numerals, for which the number 
<<n>> is represented by <<λf.λx.(f (f (... (f x))))>> where 
<<f>> is applied <<n>> times.

More precisely, define (without using [nat] nor any other inductive type):

 - a type [church : Type]
 - two constants [zero] and [one] of type [church]
 - a function [church_succ] of type [church->church]
 - two functions [church_plus] and [church_mult] of type [church->church->church]
 - a function [church_power]
 - a test [church_iszero]

Also define two functions [nat2church : nat -> church] and [church2nat : church -> nat] 

*)


(** We first complete exercise 3 of TP1, providing missing definitions of the 
following terms:

- two constants [zero] and [one] of type [church]
- a function [church_succ] of type [church->church]
- two functions [church_plus] and [church_mult] of type [church->church->church]
- a function [church_power]
- a test [church_iszero]
- two functions [nat2church : nat -> church] and [church2nat : church -> nat] 
 converting between Coq inductive definition of nats and the Church encoding 
of natural numbers. 
*)

(* *)

(** ** Predecessor and subtraction

In addition, we define:
- [church_pred] of type [church -> church], which associates zero to zero and 
to any other church numeral, associates its predecessor 
*)

(** Using [church_pred], try and define a function [church_minus] of type 
[church -> church -> church] which computes the difference between to nats: 
it should return [zero] if the first argument is less or equal to the 
second one and the difference otherwise.

Analyze carefully why universe polymorphism is needed in this case! 
(try defining this first without setting universe polymorphism...)

*)


(* *)

(** Let us first analyze the type of Church numerals:
the statement of the exercise tells us that church is 
a function of two arguments, [f] and [x], which iterates 
[f] a certain number of time over [x]. 

Therefore [f] shall have a type of the form [X -> Y] 
and necessarily [X] must be the type of [x]. Moreover, 
since [f] can be iterated, the type of its codomain shall 
be the same as the type of its domain: [X]=[Y]. 
Moreover, we want this type to apply on any pair of arguments 
the first of which is iterable on the second, and we thus 
introduce a universal quantification (dependent product type)
ending up with [forall X, (X->X)->(X->X)] as a type of 
Church numerals.

*)

Definition church := forall X, (X->X)->(X->X).

(** in defining [zero, one, ...], it is not 
needed to give the type of each argument (and neither must 
we provide a name for the type argument [X]) 
when the type of the constant is given : *)

Definition zero : church := fun _ f x => x.
Definition one : church := fun _ f x => f x.

(** in fact there is another candidates for one: *)

Definition onebis : church := fun _ f => f.
Definition two : church := fun _ f x => f (f x).

Definition succ : church -> church :=
 fun n => fun _ f x => n _ f (f x).

Definition succ' : church -> church :=
 fun n => fun _ f x => f (n _ f x).


Compute succ one.

Definition church2nat (n:church) := n _ S 0.
Check church2nat.

Compute (church2nat (succ one)).


Compute church2nat zero.
Compute church2nat one.
Compute church2nat onebis.
Compute church2nat two.

Fixpoint nat2church (n:nat) :=
 match n with
 | O => zero
 | S m => succ (nat2church m)
 end.


Compute nat2church 3.
Compute church2nat (nat2church 100).

(** In the following, we will use the ability of Coq 
to not require that we actually provide the actual 
type when it can be inferred to lighten the definitions.

But to be fully informative and in order to understand 
what happens with church_power, we provide below the 
definition of those functions providing explicitly the 
type instanciations: *)

Definition church_plus (n m:church) : church :=
  fun _ f x => n _ f (m _ f x).


Compute church_plus one two.
Compute church2nat (church_plus (nat2church 13) (nat2church 10)).

Definition church_mult (n m:church) : church :=
 fun _ f =>  n _ (m _ f).

Compute church_mult one two.
Compute church2nat (church_mult (nat2church 13) (nat2church 10)).

Definition church_pow (n m:church) : church :=
 fun _ =>  m _ (n _).

Check church_pow. 
Print church_pow.

Compute church_pow two two.
Compute church2nat (church_pow (nat2church 2) (nat2church 5)).


Fixpoint is_zero_nat (n:nat) :=
 match n with
 | O => true
 | S m => false
 end.


(** To compute whether n is equal to zero of not, we 
pass two arguments to n. The first one will be iterated 
n times on the second one. 
- Therefore, if n is zero, the first function is not 
iterated and the second argument is returned directly, 
this second argument should be [true]. 
- Otherwise, if n is not zero, the function will be 
iterated at least one and we would like is_zero n to 
return false, therefore it is sufficient that the 
first argument of n is the the constant function 
always returning zero, independently of its argument: 
[fun _ => false]. *)


Definition is_zero (n : church) : bool :=
  n _ (fun _ => false) true.

Compute is_zero zero.
Compute is_zero one.

(** Explicit typing of the above functions: *)

Definition church_plus_explicit (n m:church) : church :=
  fun X f x => n X f (m X f x).


Definition church_mult_explicit (n m:church) : church :=
 fun X f =>  n X (m X f).

Definition church_pow_explicit (n m:church) : church :=
 fun X =>  m (X->X) (n X).

Definition is_zero_explicit (n : church) : bool :=
  n bool (fun (b:bool) => false) true.




(** how to compute the predecessor?

we want that, to [zero], it associates [zero]
and, to [fun f x => f (f ... x)], it associates
[fun f x => f ... x] *)

Definition church_pred (n:church) : church :=
 fun _ f x =>
   n _ (fun g h => h (g f)) (fun _ => x) (fun u => u).

(* Variable church_pred : church -> church.*)

Definition church_pred_explicit (n:church) : church :=
 fun X f x =>
   n ((X->X)->X) (fun g h => h (g f)) (fun _ => x) (fun u => u).

Compute church2nat (church_pred zero).
Compute church2nat (church_pred one).
Compute church2nat (church_pred (nat2church 13)).

(** Attention, donne [universe inconsistency] si les univers
    polymorphes ne sont pas activés. *)


 
Definition church_minus (n m : church) : church :=
  m _ church_pred n.

Print church_minus.

Compute church2nat (church_minus two two).
Compute church2nat (church_minus (nat2church 10) (nat2church 7)).



(* *)

(** ** Universe constraints


Some (ab)uses of Coq universes are rejected by the system, since they 
endanger the logical soundness. The reason is similar to 
Russel's paradox, it is known as the 
#<a href="https://coq.inria.fr/library/Coq.Logic.Hurkens.html">Hurkens' paradox</a># 
in type theory.

A concrete example of _universe inconsistency_ will be considered during 
the practical session in the last extension of the exercise dealing with 
Church numerals and Church arithmetic, when defining [church_minus]. 

Since [church] is [forall X, (X->X)->(X->X)] here we would 
like to form [church church] and have it equivalent to 
[(church->church)->(church->church)]. This amounts to replacing variable 
[X] (of a certain [Type_i]) by the whole [church] itself, but here 
[church] can only be in [Type_(i+1)] or more (try this typing yourself!). 
This [church church] application is hence not doable when universe 
levels are fixed at the time [church] is defined. A solution here 
with a modern Coq is to activate _universe polymorphism_ 
*)

Set Universe Polymorphism.

(**

and to let Coq pick universe levels when a definition is _used_, 
not when it is defined. This helps greatly in practice (but not always).

*)

(* *)




(*************************************************************************)
(*************************************************************************)
(*************************************************************************)
(*************************************************************************)

(** * Dependent types *)

(** Programming with dependent type is one of the key features of Coq.
    Such programming is also possible in other languages such as
    Haskell and OCaml (see the GADT types, for Generalized Abstract
    DataTypes). But here the ability to mix types and values can be
    very powerful. But first, what is a dependent type ? *)

(** ** A first example : tuples *)

(** First, let us remind the type of pairs that we studied last week.  *)

(** That is a type named [prod], with syntax [ * ] for it
    (in scope delimited if necessary by [%type]).
    The constructor is named [pair], syntax [( , )]. *)

Check (1,2).
Check pair 1 2.
(** same as [(1,2)] *)

Print prod.
Check (prod nat nat).
Check (nat * nat)%type.
(** same as [(prod nat nat)] *)

Check (nat -> nat*nat).
(** no [%type] needed when a type is expected *)

(** For triples and other n-tuples, Coq provides some "syntactic sugar":
    [nat*nat*nat] is synonym to [(nat * nat) * nat] and
    [(1,2,3)] is actually [((1,2),3)]. *)

Check (1,2,3).
Check ((1,2),3).
(** the same... *)

Unset Printing Notations.
Check (1,2,3).
Set Printing Notations.

(** How to write the type of n-uples for a given n ? *)

(** First try, via an inductive type *)

Inductive n_tuple_ind (A:Type) :=
 | Nothing
 | More : A -> n_tuple_ind A -> n_tuple_ind A.

Arguments Nothing {A}.
(** Makes 1st argument of [Nothing] be implicit *)

Arguments More {A}.
(** Same for [More] *)

Check (More 1 (More 2 (More 3 Nothing))).

(** Actually, [n_tuple_ind] is just a clone of the types of lists...
    And we cannot speak easily of a particular size (e.g. triple),
    since the type system does not distinguish a couple for a triple. *)

Check (More 1 (More 2 Nothing)). (* : n_tuple_ind nat *)

(** Better : let us "program" the desired type, starting from the
    number [n]. The obtained type _depends_ from the value of this number:
    that's hence a dependent type. (Remember the [power] type considered in Lecture 2 and compare the following type with it.) *)

Fixpoint n_tuple (A:Type) (n: nat) :=
 match n with
 | 0 => A
 | S n => ((n_tuple A n) * A)%type
 end.

(** Nota: here the [%type] above is mandatory otherwise Coq interprets
    [ * ] as a multiplication by default, instead of the pair of types.
*)

Locate "*".

Check n_tuple.
(** Type -> nat -> Type *)

Compute (n_tuple nat 0).
(** Synonym for nat *)

Compute (n_tuple nat 1).
(** Synonym for nat*nat, e.g. the type of couples *)

Compute (n_tuple nat 5).
(** Sextuples of numbers, beware of the "of-by-one" *)

(** And we could even reuse the earlier "syntactic sugar" of Coq
    n-uples for building examples. *)

Check (1,2,3,4,5,6) : n_tuple nat 5.
Check (1,2,3,4,5,6) : (nat * nat * nat * nat * nat * nat)%type.

(** More generally, we could also "program" some n-uple examples: *)

Fixpoint ints n : n_tuple nat n :=
  match n with
  | 0 => 0
  | S n' => ((ints n'), n)
  end.

Fail Fixpoint intsa n :=
  match n with
  | 0 => 0
  | S n' => ((intsa n'), n)
  end.


(** Note that in the previous [match] the two branches have different
    types :
    - it is [nat] in the first case and
    - it is [(n_tuple nat n' * nat)] in the second case.
    This is a dependent match, whose typing is quite
    different from all earlier [match] we have done, where all branches
    were having the same common type.
    Here Coq is clever enough to notice that these different types are
    actually the correct instances of the claimed type [n_tuple nat n],
    respectively [n_tuple nat 0] and [n_tuple nat (S n')].
    But Coq would not be clever enough to guess the output type
    [n_tuple nat n] by itself, here it is mandatory to write it. *)

Compute ints 5.

Compute ints 99 : n_tuple nat 99.
(** a 100-uple *)

(** A good way to notice a function over a dependent type:
    There's a [forall] in its type: *)

Check ints.
(** forall n, n_tuple nat n *)

(** Indeed, no way to write this type with the non-dependent arrow
    [nat -> n_tuple nat ???], since we need here to name the left
    argument which is used at the end. *)

(** We will see below that another solution is possible to represent
    n-uples : the inductive type [vect] of Coq "vectors", i.e.
    lists of a given size. *)

(** ** Perfect binary trees *)

(** With the same approach, we could represent binary tree of depth [n]
    which are perfect (e.g. all leaves are at the same depth).
    Here we put a [nat] data at leaves. *)

Fixpoint bintree (A:Type) n :=
 match n with
 | 0 => A
 | S n => (bintree A n * bintree A n)%type
 end.

Check ((1,2),(3,4)) : bintree nat 2.

(** Just visualize the "," as indicating a binary node. *)

(** For instance, let's sum all data in such a bintree *)

Fixpoint sumtree n : bintree nat n -> nat :=
  match n with
  | 0 => fun a => a
  | S n => fun '(g,d) => sumtree n g + sumtree n d
  end.

(** Now, if we want to put some data on the nodes rather than
    on the leaves: *)

Set Universe Polymorphism.
(** To avoid a nasty issue with universes. *)

(** A singleton type for leaves (already provided in Coq):
   Only one value in type [unit], namely [Tt]. *)
Inductive unit : Type := Tt.

Fixpoint bintree' (A:Type) n :=
 match n with
 | 0 => unit
 | S n => (bintree' A n * A * bintree' A n)%type
 end.

Check ((Tt,1,Tt),2,(Tt,3,Tt)) : bintree' nat 2.

Fixpoint sumtree' n : bintree' nat n -> nat :=
  match n with
  | 0 => fun Tt => 0
  | S n => fun '(g,a,d) => sumtree' n g + a + sumtree' n d
  end.


(** Nota : here we used Coq triples, which are not primitives
    (pairs of pairs). We could also have defined an ad-hoc
    inductive type for triples. *)

(** Once again, using a [Fixpoint] here for [bintree] and [bintree']
    is only one of the possible solutions, we could also have defined
    an inductive dependent type, see later. *)

(** ** Functions of arity [n] *)

(** Definition of a type of functions with [n] arguments (in [nat])
    and a answer (in [nat]). *)

Fixpoint narity n :=
 match n with
 | 0 => nat
 | S n => nat -> narity n
 end.

Compute narity 5.

(** Example of usage : let us create an n-ary addition function.
    Beware : the first argument is [n], indicating how many more
    arguments are to be expected (and then summed). But this first
    argument [n] is not added itself to the sum. *)

(** In a first time, it is convenient to consider having at least
    a number to sum, this way we can use it as an accumulator. *)

Fixpoint narity_S_sum n : narity (S n) :=
 match n with
 | 0 => fun a => a
 | S n => fun a b => narity_S_sum n (a+b)
 end.

(** We can now generalize for any possible [n]. *)

Definition narity_sum n : narity n :=
 match n with
 | 0 => 0
 | S n => narity_S_sum n
 end.

Compute narity_sum 4 5 1 2 3 : nat.
(** 4 numbers to sum, and then 5 + 1 + 2 + 3 = 11 *)

(* *)

(** ** A type for "number or Boolean" *)

(** We can control (for instance via a [bool]) whether a type
    is [nat] or [bool]. *)

Definition nat_or_bool (b:bool) : Type :=
  if b then nat else bool.

(** An example of value in this type. *)

Definition value_nat_or_bool (b:bool) : nat_or_bool b :=
  match b return (nat_or_bool b) with
  | true => 0
  | false => false
  end.

Definition value_nat_or_bool1 (b:bool)  :=
  match b return (nat_or_bool b) with
  | true => 0
  | false => false
  end.

Definition value_nat_or_bool2 (b:bool) : nat_or_bool b :=
  match b  with
  | true => 0
  | false => false
  end.


Fail Definition value_nat_or_bool3 (b:bool)  :=
  match b  with
  | true => 0
  | false => false
  end.


(** This [match b ...] is roughly equivalent to [if b then 0 else false]
    but writing a [if] here would not give Coq enough details.
    Even putting the explicit output type isn't enough, we add to use
    an extra syntax [return ...] to help Coq. *)

(** This [nat_or_bool] type is not so helpful in itself. But we could
    use a similar idea when writing the interpretor of an abstract
    language, where the interpretation results may be in several types.

    Fixpoint expected_type (e:abstract_expression) : Type := ...

    Fixpoint interpret (e:abstract_expression) : expected_type e := ...
*)

(** To compare, a more basic way to proceed (non-dependently) would
    be an inductive type representing all the possible outcomes, see
    below. But accessing values in this type is cumbersome (just like
    accessing the value in an [option] type). *)

Inductive natOrbool :=
 | Nat : nat -> natOrbool
 | Bool : bool -> natOrbool.

(* What if we have a [natOrBool] and want to access the [nat] in it ?
   What if we are actually in front of a [bool] ? That would be an
   error we would have to handle, while the solution based on
   dependent types is immune from this issue. *)



(* *)
(** * Inductive dependent types *)

(** ** Vectors *)

(* *)

(** We shall now study another encoding of n-tuples.
    We almost reuse the inductive definition of lists, but we add an
    extra parameter representing the length of the list so that the 
type itself is equiped with the information of the length of the list. *)

Inductive vect (A:Type) : nat -> Type :=
 | Vnil : vect A 0
 | Vcons n : A -> vect A n -> vect A (S n).

Print vect_rect.

Print list_rect.

(** Notice that this is a inductive type definition, but the type
being defined depends on values of type [nat]: it is a 
_inductive dependent type_: *)

Check vect. 

(** [vect
     : Type -> nat -> Type] 
*)

(** This type vect is implemented in Coq standard library, 
see file [Vector.v].

One can declare the domain arguments of the constructors 
(Vnil and Vcons) as being implicit arguments:
*)


Arguments Vnil {A}.

Arguments Vcons {A}.


(** Let us see the encoding of triple (0,2,3) : *)

Check (Vcons 2 0 (Vcons 1 42 (Vcons 0 3 Vnil))).

(** The first argument of each constructor (numbers 2, 0, 1) 
 indicates the lengths of each sub-vector. Since this is 
pretty predictable, and hence "boring", we could
    even hide the [n] argument of [Vcons]. As follows: *)

Arguments Vcons {A} {n}.

Check (Vcons 0 (Vcons 2 (Vcons 3 Vnil))).

(** But this [n] argument is still there internally and 
can be printed by requiring to print all information
(in CoqIDE, this is managed in the "View" panel): *)

Set Printing All.
Check (Vcons 0 (Vcons 2 (Vcons 3 Vnil))).
(** [@Vcons nat 2 0 (@Vcons nat 1 2 (@Vcons nat 0 3 (@Vnil nat)))
     : vect nat 3] *)

Check (cons 0 (cons 2 (cons 3 nil))).
(** [0 :: 2 :: 3 :: @nil nat)%list] *)

Unset Printing All.

(* *)
(** ** Conversion bewteen vectors and regular lists. *)

(** Since vectors are lists with some additional information, 
one can of course define a conversion function from vectors to lists: *)

Require Import List.
Import ListNotations.

Fixpoint v2l {A} {n} (v : vect A n) : list A :=
  match v with
  | Vnil => []
  | Vcons x v => x::(v2l v)
  end.

(** But it is also possible to define the conversion in the other 
direction: *)

Fixpoint l2v {A} (l: list A) : vect A (length l) :=
  match l with
  | [] => Vnil
  | x :: l' => Vcons x (l2v l')
  end.

(** In the previous definition, we benefited greatly from 
the implicit argument management offered by Coq. Try to define 
the same conversion function without declaring the [nat] argument 
as implicit... *)

(* *) 
(** The following definition (re-)computing the length of a vector
    is actually useless since we already known this length, it is
    the parameter [n] mentionned in the type [vect].
    Later, we could prove that
    [forall A n (v:vect A n), length v = n]. *)

Fixpoint length {A} {n} (v: vect A n) : nat :=
 match v with
 | Vnil => 0
 | Vcons _ v => 1 + length v
 end.


(** We could more simply define it as: *)

Definition length' {A} {n} (v: vect A n) : nat := n.

