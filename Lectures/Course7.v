(** * Course7: Understanding how the tactics for inductive types work + introduction to the Module system *)



(** * Tactics for inductive types:



Several handy tactics on an inductive type :

 - [case] : performs case analyis on an inductive hypothesis, 
  without induction;
 - [destruct] : a more elaborate tactic to do case analysis;
 - [elim] : perform inductive reasoning; 
 - [induction] tactic: perform an improved inductive reasoning;
 - [simpl] : applies computation rules;
 - [rewrite H] : rewrite a term t with a term u when [H] has type [t=u], 
  variant: [rewrite <- H];
 - [injection] : all inductive constructors are injective, from 
  [H : S x = S y] then [injection H] provides [x = y];
 - [discriminate] : all inductive constructors are orthogonal, 
  from [H : S x = O] then [discriminate H] or just [discriminate] 
  proves [False] (hence anything);
 - [f_equal] : proves a goal [f x = f y], as long as sub-goal 
  [x = y] could then be proved. Sort of dual to [injection], 
  except that it works for any function [f], not only for 
  inductive constructors.
*)



(** * More on tactics on inductive types. 

Let us examine the tactics on inductive types in more details
*)


(** ** Case analysis *)

Section CaseAnalysis.
Parameter A B C D : Prop.

Lemma Case1: (A -> B \/ C) -> D.
Proof. 
intro H. case H. 
Show 1. 
Show 2. 
Show 3. 
Restart. 
intro H. destruct H.
Show 1. 
Show 2. 
Show 3. 
Restart. 
intro H. destruct H  as [H1 | H2]. 
Show 3. 
Abort. 

End CaseAnalysis.

Print nat_ind.

(** ** [elim]

[elim] tactic establishes a link between the inductive type and the associated induction
principle by using this induction principle. 

In its most basic form, [elim t] receives an argument [t] of an inductive type [T], 
checks the sort of its goal (typically [Prop]) and chooses the corresponding 
induction principle. In the case of [Prop], the chosen induction principle
is [T_ind], for [Set] it is [T_rec] and for [Type] it is [T_rect].
They all have the same form, ending with a formula [∀ x:T, P x]. 

To use this, the tactic need to determine in which manner the goal is a 
function of the term [t] and applies the induction principle. 

The [pattern] tactic offers how to simply detemrine this and when induction principle 
[T_ind] is chosen, [elim t] behaviour is close to [pattern t; apply T_ind.]


Note that [elim] can also be used on an identifier which 
is a function returning in an inductive type. 

*)

(** ** [induction n]

Built on top of [elim]. When [n] is not in the context, 
[induction n] corresponds to [intros until v;elim v]

Elaborate behaviour of [induction n] when [n] is in the context. 

*)

Require Import Arith. 

Theorem le_plus_minus' : forall n m:nat, m <= n -> n = m+(n-m).
Proof.
induction n. 
Show 2. 
+ intros m Hm. 
pose Nat.le_0_r as H'.
destruct (H' m) as [H0 H1]. rewrite H0.
- reflexivity.
- assumption.
+ intros m Hm. 
 SearchPattern (_ <= S _).
destruct m as [| n'].
- simpl. reflexivity.
- simpl. f_equal. apply (IHn n').
SearchPattern (S ?X <= S ?Y -> ?X <= ?Y). 
apply le_S_n. assumption.
Qed.



(** ** How do the tactics discriminate and injection work internally ?
*)

Require Import List.
Import ListNotations.

(** *** discriminate : all inductive constructors are orthogonal *)

(** for nat : *)

Lemma orthognal_S_O_discr : forall n,  S n = 0 -> False.
Proof. intros. discriminate. Qed.




Definition discr (m : nat) : Prop :=
 match m with
 | O => True
 | S _ => False
 end.

Compute discr 0. (* True *)
Compute discr 1. (* False *)

Lemma orthognal_S_O : forall n,  S n = 0 -> False.
(* said otherwise : forall n,  S n <> 0. *)
(* reminder : <> is the negation of = *)
(*            ~ A is A -> False *)
Proof.
 intros.
 discriminate.
 Undo.
(* assert (discr (S n) = False).
reflexivity.
 rewrite <- H0.*)
 change (discr (S n)). (* converts the statement into something convertible*)
 rewrite H.
 simpl.
 constructor. (* or: exact I *)
Show Proof.
Qed.

(** for list *)

Definition discr_cons_nil {A}(l:list A) :=
 match l with
 | nil => True
 | cons _ _ => False
 end.

Lemma orthognal_cons_nil {A}: forall (x:A) l,  cons x l = nil -> False.
Proof.
 discriminate.
 Undo.
 intros.
 change (discr_cons_nil (cons x l)).
 rewrite H. simpl.  constructor.
Qed.



(** *** injection : injectivity for free for any inductive constructor *)

(* for nat : *)

Lemma inject_S_inj : forall n m,  S n = S m -> n = m.
Proof.
 intros.
 injection H.
 trivial. 
Qed.


(* We need a projection of the first argument of S:
   That's the predecessor Nat.pred ! *)

Definition exhibit_succ (n:nat) : nat :=
 match n with
 | S x => x
 | O => O
 end.

Compute exhibit_succ 5. (* 4 *)

Lemma inject_S : forall n m,  S n = S m -> n = m.
Proof.
 intros.
(* injection H. *)
 change (exhibit_succ (S n) = exhibit_succ (S m)).
 rewrite H.
 reflexivity.
Show Proof.
Qed.

(* Same, for lists.
   First, a projection of the first argument of cons
   (We need a way to fill the other constructor, here nil,
    with something of the right type, here (default:A)
*)

Definition proj1_cons {A} (l:list A) (default:A) :=
 match l with
 | cons x l => x
 | nil => default
 end.


(* 
Definition projk_Ci {Ak} (t: T) (default:Ak) :=
 match l with
 | Ci t0 ... tk ... tn => tk
 | _ => default
 end.

 change (projk_Ci (Ci t0 ... tn) tk = projk_Ci (Ci u0 ... un) tk).
*)

Lemma inject_cons {A}: forall (x x':A) l l',
    x::l = x'::l' -> x=x'.
Proof.
 intros.
 injection H; intros; assumption.
Undo.  
 change (proj1_cons (x::l) x = proj1_cons (x'::l') x).
 rewrite H.
 reflexivity.
Qed.

(* Second, a projection of the second argument.
   Easier, no need for "default" this time. *)

Definition proj2_cons {A} (l:list A) :=
 match l with
 | cons x l => l
 | nil => nil
 end.


Lemma inject_cons' {A}: forall (x x':A) l l',
    x::l = x'::l' -> l=l'.
Proof.
 intros.
 change (proj2_cons (x::l) = proj2_cons (x'::l')).
 rewrite H.
 reflexivity.
Qed.





(** * The [Inversion] tactic for inductive types:


*)


(** * A few words about the inversion tactics 

Already seen : injection and discriminate :

  - [injection H]  where [H : S x = S y] gives [x = y].
  - [injection H]  where [H : x::l = x'::l'] gives both [x=x'] and [l=l'].
  - [discriminate H] where [H : O = S x] conludes the goal
    (since this case is impossible).
*)


(** The tactic [inversion] is a generalization of both,
   trying to recover from an inductive predicate
   what situations may have led to this concrete predicate.
*)



Inductive even : nat -> Prop :=
 | even_O : even O
 | even_SS n : even n -> even (S (S n)).

Lemma even_2 : even 2.
Proof. 
 apply even_SS.
 apply even_O.
Qed.

Lemma even_plus4 : forall n, even n -> even (4+n).
Proof.
 intros.
 apply even_SS.
 apply even_SS.
 assumption.
Qed.

(** Up to now, [even_2] and [even_plus4] are direct proofs,
   no need for inversion. *)

Lemma not_even_one : ~even 1.
Proof.
 intro.
 (* Here, destruct H (or induction H) would forget that our 
    number is 1... *)
 destruct H. Show 1. Show 2. Undo.
(** Before destruct H, we need to save all details ourselves
    (for instance via "remember"). *)
 remember 1 as m.
 destruct H.
 - discriminate.
 - discriminate.
Qed.

(** inversion is here nicer than this remember + destruct,
   and way more general *)

Lemma not_even_one_bis : ~even 1.
Proof.
 intro.
 inversion H.
Qed.

Lemma even_plus3 n : even (3+n) -> even (1+n).
Proof.
 intro H.
 inversion H. 
 (* subst. (* if you want to get rid of remaining equations *)*)
 assumption.
Qed.

(** Since equality is also an inductive predicate, inversion 
   also works on equality hypothesis (and subsumes both 
   [injection] and [discriminate]). *)

Lemma test_inj x : S x = 0 -> False.
Proof.
intro H.
inversion H.
Qed.

(** ** Why is it called inversion? *)
 
Theorem even_SS_inv : forall n: nat, even (S(S n)) -> even n.
Proof.
intros n H. inversion H. trivial.
Qed.

(** * How does [inversion] work?

Let us try to prove manually the previous theorem. 
*)

Theorem even_SS_inv_bis : forall n: nat, even (S(S n)) -> even n.
Proof.
intros n H. 
generalize (refl_equal (S(S n))).
pattern (S(S n)) at -2.
elim H.
- intro H0. discriminate.
- intros n0 H0 H1 H2. 
injection H2. intro H3. 
rewrite H3 in H0. assumption.
Qed.

(** The same can be done to show that [1] is not [even]: *)


Lemma not_even_one_ter : ~even 1.
Proof.
intro H.
generalize (refl_equal 1).
pattern 1 at -2.
elim H. 
- intro H0. discriminate.
- intros n0 H0 H1 H2. discriminate H2.
(* or shorter: *) 
Restart. 
 intro H; generalize (refl_equal 1); pattern 1 at -2.
elim H; discriminate.
Qed.







(** * A tiny introduction to Coq's Module system. *)

(** Modules are used to structure data and 
theories in order to 
  - allow program (or proof) reuse in 
   various contexts and 
  - offer the ability to hide implementation 
   details and thus prevent users to rely on them 
   and therefore allow to improve the design of 
   the module without keeping the previous 
   implementation details.
Coq Module system is inspired from the one of OCaml 
and, while it is very powerful, we will only touch 
upon basic features of the module system. 
*)

(** A module is given in general by first 
specifying a _signature_ or _interface_ that 
specifies what data and logical information 
a module shall contain and possibly also 
specify / restrict how visible it may be 
from the outside of the module. 

Declaration of a signature is done via *)
Module Type Moduletest. 
(* ... *)
End Moduletest. 


(** The most basic use of Module is to prevent 
name cashes: 
*)

Module Mynat.
Inductive nat:= 
|O: nat
|S: nat -> nat.
End Mynat.

Module Mynat2.
Inductive nat:= 
|O: nat
|S: nat -> nat.
End Mynat2.

Print nat. 

Print Mynat.nat.

Print Mynat2.nat.

(** But modules can be used in much richer ways 
in order to structure data and logical theories, 
let us see some examples of use. *)


Require Import List.
Import ListNotations.


(** Example of "Module Type", also known as a 
_signature_, or an interface. *)

Module Type MYLIST.
 Parameter t : Type -> Type.
 Parameter empty : forall A, t A.
 Parameter cons : forall A, A -> t A -> t A.
 Parameter decons : forall A, t A -> option (A * t A).
 Parameter length : forall A, t A -> nat.
End MYLIST.


(** Example of an implementation of this signature.

    With the "<:" syntax below, Coq checks that the definitions
    in module MyList0 are compatible with MYLIST.
    But MyList0 is left unrestricted afterwards.
*)

Module MyList0 <: MYLIST.
 Definition t := list.
 Definition empty {A} : list A := [].
 Definition cons := @List.cons.
Definition decons {A} (l:list A) :=
  match l with
  | [] => None
  | x::l => Some (x,l)
  end.
 Definition length := @List.length.

 (* Note: the implementation can contain extra stuff not mentionned in 
the signature. *)
 Definition truc := 1 + 2.
End MyList0.

Print MyList0.t.

Check MyList0.truc.

Compute (MyList0.cons _ 1 MyList0.empty).



(** The problem is that the interface specification 
allows other implementations which do not correspond to 
our idea: *)

Module MyList1 <: MYLIST.
 Definition t := list.
 Definition empty {A} : list A := [].
 Definition cons {A} (a: A) (l: list A) := l.
 Definition decons {A} (l:list A) : option (A * t A):= None.
 Definition length {A} (l: list A) := 42.
End MyList1.

(** In fact, a signature can contain contain declaration 
of computational data as well as logical data as well as 
the declaration as a proper name space. *)

Module Type Moduletest'. 
 Parameter A: Set.
 Parameter inf sup: nat. 
 Axiom Bounds : inf <= sup.
End Moduletest'. 


(** We can use this to make the specification more precise, 
in the module signature: *)

Module Type MYLOGICALLIST.
 Parameter t : Type -> Type.
 Parameter empty : forall A, t A.
 Parameter cons : forall A, A -> t A -> t A.
 Parameter decons : forall A, t A -> option (A * t A).
 Parameter length : forall A, t A -> nat.

 Axiom empty_def: forall A, decons A (empty A) = None. 
 Axiom cons_decons: forall A, forall (a: A), forall (l: t A), 
                 decons A (cons A a l) = Some (a, l).
 Axiom length_cons : forall A, forall (a: A), forall (l: t A), 
    length A (cons A a l) = S(length A l).
End MYLOGICALLIST.

 
Module MyList <: MYLOGICALLIST.
(* data / computational component *)
 Definition t := list.
 Definition empty {A} : list A := [].
 Definition cons := @List.cons.
 Definition decons {A} (l:list A) :=
  match l with
  | [] => None
  | x::l => Some (x,l)
  end.
 Definition length := @List.length.

(* logical component *)
 Theorem empty_def: forall A: Type, @decons A (@empty A) = None.
  Proof. trivial. Qed.

 Theorem cons_decons: forall A, forall (a: A), forall (l: t A), 
                 decons (cons A a l) = Some (a, l).
 Proof. trivial. Qed.

 Theorem length_cons : forall A, forall (a: A), forall (l: t A), 
    @length A (@cons A a l) = S(@length A l).
  Proof. trivial. Qed.

End MyList.

Print MyList.t.

Compute (MyList.cons _ 1 MyList.empty).

Print MyList.length_cons.


(** Now, if we use the syntax ":" below instead of "<:" in the definition
    of a module, all internal details will be masked afterwards, hidden
    by MYLOGICALLIST, and only the information given by MYLOGICALLIST 
    will be available.
    In particular, this will prevent here any computation, since
    the body of the definitions will be inaccessible.
    So in Coq this ":" is usually far too restrictive, unlike in
    languages like OCaml.
*)

Module RestrictedMyList : MYLOGICALLIST := MyList.

Print RestrictedMyList.t.

Compute (RestrictedMyList.cons _ 1 (RestrictedMyList.empty _)).

Fail Check RestrictedMyList.truc.



(** A "functor" ("foncteur" in French) is a module parametrized
    by another module.
*)


(** Example: starting from [MYLOGICALLIST], one may propose some [head] and
    [tail] functions. *)

Module HeadTail (M:MYLOGICALLIST).
  Definition head {A} : M.t A -> option A :=
    fun l => match M.decons _ l with None => None | Some (x,l') => Some x end.
  Definition tail {A} : M.t A -> option (M.t A) :=
    fun l => match M.decons _ l with None => None | Some (x,l') => Some l' end.
End HeadTail.

(** For now, this does not create any new concrete functions. *)

Fail Print HeadTail.head.

(** But we can use this in a generic way on any implementation of MYLOGICALLIST. *)

Module MyListHdTl := HeadTail(MyList).

Print MyListHdTl.head.

Compute MyListHdTl.head [1;2].

(* But we do not have anymore access to the components from MyList... *)
Fail Print MyListHdTl.cons. 


(** We can even extend a module, via a notion of inclusion. *)

Module MyList2.
 Include MyList.
 Include HeadTail(MyList).
End MyList2.

Print MyList2.head.
Print MyList2.cons.
 
(** Lighter syntax for the same thing: *)

Module MyList3 := MyList <+ HeadTail.

(** Another example of functor: starting from a first module
    satisfying interface Foo, we could build another one for which
    the [length] function is working in constant time.
    For that we store somewhere this size, and update it after
    all operations. That's a typical example of time vs. space tradeoff.
*)

Module FastLength (M:MYLIST) <: MYLIST.
 Definition t A := (M.t A * nat)%type.
 Definition empty A := (M.empty A, 0).
 Definition cons A x (l:t A) :=
  let (l,n) := l in
  (M.cons A x l, S n).
 Definition decons A (l:t A) :=
   let (l,n) := l in
   match M.decons A l with
   | None => None
   | Some (x,l) => Some (x,(l,pred n))
   end.
 Definition length A (l:t A) := snd l.

End FastLength.



