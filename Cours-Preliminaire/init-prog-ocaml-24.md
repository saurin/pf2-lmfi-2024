 Introduction to Functional Programming in OCaml
================================================

**M2 LMFI - 2024-2025**

*NB:* The content of this introductory lecture is based on Pierre Letouzey's work, adapted and expanded with additional details and exercises.

## First, log in !

Log in on some lab computer. Warning : the login to use is *not* directly your university credentials (`@u-paris.fr`), you need to visit first https://stp.math.univ-paris-diderot.fr/ to get a login specific to these lab rooms.

In case of trouble, there is a "guest" account without password, but with restricted web access later, ask me or a fellow student at that moment.

Open a browser and retrieve this document via https://link.infini.fr/prelim-ocaml.


## Kickstart

For today, a basic OCaml programming environment will be good enough : http://try.ocamlpro.com. No local installation required, directly try there an OCaml phrase such as `let x = 1+2`. Also experiment with the "Editor" tab and its bottom buttons.

Of course, there are other OCaml environments that you could try later :

 - Quite raw but still handy sometimes : the OCaml "toplevel" (interpréteur in French) can be launched in a Terminal window (a.k.a shell, or console) by typing `ocaml` in it. The OCaml prompt `#` appears, and you can then submit some OCaml phrase (here finished by the legacy `;;` terminator). `Ctrl+c` interrupts a running computation, `Ctrl+d` terminates the toplevel.

 - For more serious OCaml programming, favor a code editor such as `emacs` :
   - Launch emacs and open a new file such as `exo.ml` (be sure to use a name with the `.ml` extension otherwise emacs won't detect OCaml code).
   - In the `Tuareg` menu on the left, in submenu `Interactive Mode`, pick `Run OCaml Toplevel` and press Enter to confirm the proposed toplevel.
   - Any phrase written in `exo.ml` can then be evaluated by putting the cursor in it and typing `Ctrl+c Ctrl+e` (or `Evaluate phrase` in the previous submenu).

**Nota Bene** : no need anymore to use the old phrase terminator `;;` (except in direct use of the toplevel) as long as *all* your phrases start with `let`, even test phrases. So instead of `1+2;;` it is now recommanded to write something like `let x = 1+2` or `let _ = 1+2`.

OCaml provides many other tools for more advanced use : compilers (including native ones), debuggers, profilers, etc. But that's another story.


## The functional core of OCaml

OCaml directly derives from the lambda-calculus (due to Church in the 1930s) and its three basic constructions:

 - variables ; `x`, `y`, `z`...
 - function abstraction : `λx.t` (written `fun x -> t` in OCaml)
 - function application : `t u` (same in OCaml).

Note that in `λx.t` or `fun x -> t`, the function abstraction binds the occurrences of `x` in `t`, just as other usual binding notations in math (function notation (x -> f(x)), quantification (∀x.A(x)) as well as integral or summation operators) and the actual name of the variable does not matter as long as it is updated consistantly in the expression.

In other communities, the function application may be written `t(u)` (regular math style) or `(t)u` (Krivine style).
Here in OCaml and many other functional languages we favor a light style, with parenthesis only in case of ambiguity. 
To do so, some priority convention apply:

 - Application associates to the left. For instance `x y z` is a shortcut for `(x y) z` : one function `x` applied to two successive arguments `y`, then `z`. On the other hand, `x (y z)` is quite different : `y` will be a function receiving `z`, and the result is given to function `x`.
 - The scope of a function abstraction (ie. `fun x`) extends as much as possible. For instance `fun x -> x y` corresponds to `fun x -> (x y)` that is to the function which receives a function `x` as an argument and applies it to  `y`, which is different from `(fun x -> x) y` which corresponds to the identity function applied to `y`, which computes to `y`.
 - Important rule of thumb : in function applications, put parenthesis around every argument that is not a variable or a constant.
 - Other important shortcut : `fun x y -> t` is `fun x -> fun y -> t`.


There is *only one computation rule* in the λ-calculus : *β-reduction*, where `(λx.t) u` gives / reduces to / evaluates to `t{x:=u}` (for some reasonable definition of substitution that we shall not detail today -- essentially substitution shall prevents the capture of free variables -- wait and see the Proof theory course for details). 

For instance `λx.x` is the identity function while `λx.y` is a constant function returning `y` whatever its argument. Indeed, whatever term `u` we consider, `(λx.x)u` reduces to `u` while `(λx.y)u` reduces to `y`.

In the λ-calculus, this β-reduction may occur anywhere, in any order: any term `t` containing `(λx.u) v` as a subterm can give rise to a computation step lead to a term `t'` corresponding to `t` except for the subtem `(λx.u) v` (the *redex*, for *red*ucible *ex*pression) is replaced by `u{x:=v}` (the *reduct*). On the contrary, a term containing no such subterm (that is with no redex) is called a *normal form*. `t` is said to *have a normal form* if it can be reduced to a normal form.

An important theoretical property of β-reduction is the *confluence* property, stating that the order of computation does not matter from the point of view of evaluation: if `t` β-reduces to some term `u` by some reductions steps as well as to some other term `v` by using other reduction steps, there is a term `w` such that both `u` and `v` can be reduced to `w`. 
In particular, if a term has a normal form, this normal form is unique (prove it!), which ensures consistency of the λ-calculus (See Thierry Joly's course for more details.)

The λ-calculus admits non-terminating programs: a famous example of a term whose reduction is infinite is `Δ Δ` with `Δ = λx.(x x)`,
since `Δ Δ` reduces to `(x x){x:=Δ} = Δ Δ`. From that, *fixpoint combinators* can be created and used for defining recursive functions in raw λ-calculus (but that's quite tricky, see the Y combinator for instance and, once more, Thierry Joly's lectures).

#### Exercise 1: reduce λ-terms

On paper, reduces the following terms to normal form (if any): 

 - `(λx.x)λy.y`
 - `λz.(λx.λy.x)z`
 - `λy.(λx.λy.x)y`
 - `((λf.λg.λz.(f (g z))) λx.x)λy.y`
 - Let `Δf` be `λx.(f (x x))`, reduce `λf.(Δf Δf)` by two steps of β-reduction



## Typing

By default, the λ-calculus is said *raw* or *pure* or *untyped* : you can try applying anything to anything and look if it breaks.

Some untyped programming languages based on λ : Lisp or Scheme. 

On the contrary, as most recent functional programming languages, OCaml is *strongly typed* : the non-well-typed terms will be rejected in OCaml during *type-checking*, before running any computation.

The main type constructor is the *functional arrow* `->`.

First, simply-typed λ-calculus : just take λ-calculus plus a typing judgement `Γ ⊢ t:T` described by the rules below. Here Γ is a *typing context*, i.e. a finite list of variables associated with their respective types.

 - If `x:T` is in Γ then `Γ ⊢ x:T`
 - If `Γ+x:τ ⊢ t:σ` then `Γ ⊢ (λx.t):(τ→σ)`
 - If `Γ ⊢ t:(τ→σ)` and `Γ ⊢ u:τ` then `Γ ⊢ (t u) : σ`

Note that by forgetting all the terms in the previous rules and keeping only the types (and contexts being hence just list of types), we recover the logical rules of the minimal logic : the axiom rule and the implication intro/elim rules. That is the start of the Curry-Howard correspondence (more on that in the Proof Theory course and in PF2).

OCaml typing system is an extension of these basic rules, for now the main addition is a notion of type variables (`'a` in OCaml syntax) allowing for *polymorphism* (same generic code may be reused in similar situations, examples to come below).

We will see soon that OCaml also provides way more than the basic λ-calculus terms and types. In particular, unlike raw λ-calculus, OCaml does provide primitive integers 0 1 2... -1 -2 ... of type `int` .

#### Exercise 2: predict OCaml answers

```ocaml
(* In OCaml, comments are written between (* ... *) and they can be nested... *)
let _ = fun x -> x
let _ = fun x -> y
let _ = fun x -> 2
let _ = (fun x -> x) 2
let _ = 2 3
let _ = (fun x -> x) 2 3
let _ = fun f -> f 2
let _ = fun f -> f 2 3
let _ = fun f -> f (f 2)
let _ = fun x -> x x
```
#### Exercise 3: Type-check the following terms and compare their types with axioms of Hilbert proof systems:
```ocaml
let a1 = fun x y -> x
let a2 = fun g f x -> g x (f x)
```

#### Optional Exercise 3': Hilbert-style proofs in OCaml

You have seen with Patrick Simonetta that (A -> A) can be derived from the first two axioms of Hilbert systems.
Try and define an OCaml term, the type of which is `'a -> 'a`, by using only terms `a1` and `a2` above as well as function applications.

Note that thanks to the polymorphism of OCaml types, two occurrences of, say `a1` may be used with different types.

## Computations in a typed setting

In the simply typed λ-calculus, computations interact nicely with types :

 - The type is preserved during reduction (a property known as "type preservation" or subject reduction"): if `t` has type `A` and `t` reduces to `u` then `u` has type `A`.
 - Strong normalization : a well-typed term has no infinite reduction (hence `Δ Δ` cannot be well-typed).
 - Strong normalization + confluence together imply that reducing a well-typed term `t` as long as possible in any possible manner always ends on the unique normal form `t₀` of `t`, which can be considered as the *value* of `t`.

OCaml has the same β-reduction rule, but in a controlled order which is named *call-by-value* (CBV, aka *eager* aka *strict*) : one β-reduces `(λx.t) u` only when argument `u` is *already fully reduced*. Moreover this reduction is *weak* , that is one never reduces inside a `fun` (or a `λ`): `fun x -> (fun y -> y)x` is fully evaluated in OCaml.

OCaml satisfies a subject reduction property, which is critical to exclude a whole family of runtime errors just by static typing (e.g. no "segfaults" in OCaml). Roughly, "if it types, it runs ok" (at least as long as you do not use *exceptions* or exception-aware functions, but we won't touch that today!).

Of course, there is no direct equivalent of `Δ Δ` or fixpoint combinators (`λf.Δf Δf`) in OCaml due to typing:

#### Exercise 4: Try to evaluate / type-check the following:
```ocaml
let _ = fun f -> (fun x -> f (x x)) (fun x -> f (x x))
```

 But OCaml is *not* strongly normalizing, on the contrary it provides a notion of general recursivity (`let rec`, see below). Actually, normalizing languages are too restrictive to be of general use, they are not *Turing-complete* (see Cours on Coq later).


## A first OCaml extension : let abbreviations

OCaml provides a mechanism for global and local definitions. Indeed, you can give a name `x` to a term `t`: 

 - Globally via `let x = t`. Here `x` can be used during the rest of the session.

 - Locally via `let x = t in u`. Here `x` can only be used inside `u` but not after.


Lexical scoping : at a given program point, only some abbreviations may be visible (early global ones or unfinished local ones).

If the same variable name appears in several `let`, the most recent abbreviation wins, the others are hidden. This way, you may appear to "change" an abbreviation, while `let` are actually *immutable*.

*Note:* `let x = t in u` may be emulated in λ-calculus through `(λx.u) t`. For a finished program, you can turn all global `let` in local `let .. in`.

To name functions, either `let` followed by `fun` or use a dedicated syntax with parameters before the `=` sign :

```ocaml
let identity = fun x -> x
let identity x = x
```

More examples : function projections (which may actually encode pseudo-boolean values) :

```ocaml
let proj1 x y = x
let proj2 x y = y
let pseudoif b x y = b x y
```

#### Exercise 5: OCaml scoping

Predict OCaml answers after each phrase (`and` allows *simultaneous* abbreviations)

```ocaml
let x = 2
```

```ocaml
let x = 3 in
let y = x + 1 in
x + y
```

```ocaml
let x = 3 in
let y = let x = 4 in x + 1 in
x + y
```

```ocaml
let x = 3
and y = x + 1 in
x + y
```

```ocaml
let x = 3
let f y = y + x
let _ = f 2
let x = 0
let _ = f 2
```

#### Exercise 6: recover parenthesis

Question: add the needed parenthesis for this code to type-check and run :

```ocaml
let sum x y = x + y
let _ = sum sum sum 2 3 4 sum 2 sum 3 4
```

## First data types : boolean and integers

The untyped λ-calculus allows to encode (i.e. emulate) all needed datatype (it is actually Turing-complete). But these encodings are *complex*, often *inefficient* and often *incompatible with typing*. Instead OCaml provides primitive data types and data structures instead.

First, the `bool` type and its `true` and `false` constants, and a primitive `if ... then ... else ...` construction.
Some boolean functions :

  - negation : `not`
  - logical "and" : `&&`
  - logical "or" : `||`

Note : evaluating `&&` or `||` is quite particular, it obeys *lazy* rules (Call-by-need) while the rest of OCaml is Call-by-value. That means that the right conjunct / disjunct will be evaluated *only* if needed to determine the boolean result.

Boolean answers will typically be obtained after an equality test `x = y` or `x <> y` or a comparison `x < y` or `x <= y`, etc.

As already said, OCaml provides a type `int` of "machine" integers (hence with large but *finite* bounds, beware of overflows).
If needed, there exists a library of arbitrary size numbers.
Basic operations on integers : addition `+`, multiplication `*`, euclidean division `/`, modulo `x mod y`. Beware that `/` is a first example of function that may fail with an *exception* : `3 / 0`. 

*Note*: floating point numbers have their own type `float` and their own operations `+.`, `*.`, explicit conversion can be performed via conversion function such as `float_of_int`. (We will not use them in the following.)


#### Exercise 7: Boolean tautologies

  - Write a function `checktauto : (bool->bool)->bool` which test whether a one-argument boolean function is a tautology, i.e. always answers `true`.

  - Same with `checktauto2` and `checktauto3` for functions with 2, then 3 arguments. This can be done by brute-force case enumeration, but there are clever ways to do it, for instance reuse `checktauto`.

#### Optional exercise 7': Testing equality of boolean functions

  - What does the following function `f` ? Write an equivalent `g` function with just some `if` instead of `match`. Verify via `checktauto3` that `f` and `g` are indeed observationally equal.

```ocaml
let f x y z = match x, y, z with
  | _ , false , true -> 1
  | false , true , _ -> 2
  | _ , _ , false -> 3
  | _ , _ , true -> 4
```

#### Optional exercise 7'': encoding `&&` and `||` with `if-then-else`

  - Write Ocaml expressions equivalent to `a && b` and `a || b` using only the `if then else` construct. 

  - Note that due to the call-by-value evaluation of OCaml, defining a functions doing lazy conjunction and lazy disjunction is not directly doable. 


## Recursivity 

The `let rec` construction allows to reuse in itself the function that we are currently writing !

Beware of loops during evaluation. Check your halting case(s) (for instance when n is 0) and do recursive call on decreasing values.

Example of non-terminating functions:
```ocaml
let rec bad n = print_int n; bad (n-1) + 1
let rec skip () = skip ()
let rec f x = f x
```

#### Exercice 8: Write some usual recursive functions

 - Define a function `fact: int -> int` which computes the factorial. 

 - Define a function `fib: int -> int` which computes the Fibonacci numbers. (Beware, the basic recursive function has an exponential behavior (why ?). How to avoid that ?)

 - Define a function `gcd: int -> int -> int` computing the greatest common divisor.

 - Power function on integers. How to limit the number of multiplications done ?

## Higher-order functions and partial application

In functional programming, functions are *first-class citizens*. In particular, they may be passed as arguments of other functions (think of the `checktauto` defined above), or returned as output of a function call.

Example : the function composition

```ocaml
let compose f g = fun x -> f (g (x))
```

Or equivalently :

```ocaml
let compose f g x = f (g (x))
```

Note and understand the type inferred by OCaml : `('a->'b)->('c->'a)->'c->'b`

Moreover a function expecting several arguments may receive only some of them, leading to a function expecting the missing arguments. That is called partial application.


#### Exercise 9: functions over functions

Question: write a `sigma` function such that `sigma f a b` computes the sum of all values returned by function `f` (of type `int->int`) between integers `a` et `b`. What is the type of `sigma` ?

Question: write a `iter` function such as `iter f n x` computes the n-th iterate of `f` at point `x`. What is the type of `iter` ? Use this `iter` to propose a new implementation of the power function on integers.

Question: devise a situation of over-application, i.e. a function receiving more arguments than initially expected


## OCaml Pairs

A pair `(a,b)` regroups two elements `a` and `b`. These two elements may be of different types (or not).
The type of this pair is written `τ * σ` when types of `a` and `b` are respectively `τ` and `σ`.

OCaml also provides triplets, quadruplets, n-uplets, all built on the same principle, for instance `(1,2,true)` is a `int * int * bool`.

For using a pair `p`, either project it (using predefined functions `fst` et `snd`),
or access all components at once (via a syntax such as `let (x,y) = p in ...`).

Example : computing two following Fibonacci numbers efficiently.

```ocaml
let rec fibs n =
 if n = 0 then (1,1)
 else
    let (a,b) = fibs (n-1) in
    (b, a+b)
    
let fib n = fst (fibs n)
```

The Curry-Howard correspondence can be extended to pairs.
Actually the typing rules of pairs `(a,b)` and of projections `fst` and `snd` mimic the logical rules introducing and eliminating conjunctions in natural deduction.

#### Curried and uncurried functions.

If an OCaml function has several arguments, the habit is rather to separate these arguments as seen last week : `fun arg1 arg2 arg3 -> ...`.
This leads to a type of the form `typ_arg1 -> typ_arg2 -> ... -> typ_res`. This kind of function is said *curried* (after logician Haskell Curry).

But another style is to regroup all arguments in a n-uplet : `fun (arg1,arg2,...) -> ...`.
This leads to a type of the form `typ_arg1 * typ_arg2 * ... * typ_argn -> typ_res`. This function is said *uncurried*. In this case, the function cannot be partially applied easily. Conversely, it might sometimes be convenient to treat all these arguments as a unique one.

#### Exercise 10: let's curry and uncurry

Question : write a function `curry` which converts a function of type `'a*'b -> 'c` into a function of type `'a -> 'b -> 'c`. That is `curry` shall have type `('a*'b -> 'c) -> 'a -> 'b -> 'c`.

Write the converse `uncurry` function.

## OCaml Lists

An OCaml list is a structure regrouping many elements of the same type. 

Example: `[1;2;3;2]` is an `int list`.

A list is an ordered structure : the positions of elements in a list are relevant, `[2;1]` is not the same than `[1;2]` nor `[1;2;1]`.
A list may be of arbitrary length, starting from 0 (the empty list is `[]`). 

The basic constructors of lists are the empty list `[]` and the concatenation `x::l` of an extra element `x` on the left of an existing list `l`.
Note that the `[1;2;3;4]` syntax is just a shortcut for `1::(2::(3::(4::[])))`.

For analyzing a list, there exist predefined functions `List.hd` et `List.tl` (for *head* and *tail*), but favor instead a `match...with` syntax dealing both with the `[]` case and the `x::l` case.

The lists (just as pairs earlier, and trees below) are *immutables* structures : when a list has been built, its elements cannot been modified anymore (unlike for instance *arrays* in OCaml or other programming languages). But it is possible to form new lists by reusing older ones (in whole or in proper subparts).

Note that while the head of a list (its leftmost element) can be accessed in constant time, just as a `x::l` operation, the bottom of a list (its rightmost element) can only be accessed after visiting the whole list, hence in a linear time (ie. proportional to the length of the list).

#### Exercise 11: retrieve some usual functions on lists

 - Recode a length function on lists (already provided by OCaml as `List.length`).

 - Recode the concatenation of two lists (already provided by OCaml as `List.append` or as infix operator `@`). For instance `append [1;2] [3;4] = [1;2;3;4]`.
 
 - Using the previous append function, recode a reverse function `rev` on lists (a.k.a. mirror), cf. `List.rev`. 

 - Recode the filter function `List.filter`. From a boolean test function `f` (ie of type `'a -> bool`) and a list `l`, `List.filter f l` keeps only the elements `x` of `l` such that `f x`.

 - Recode `List.map`, which apply a function `f` on all elements of a list.

#### Exercise 12: Efficient reverse

 - Recode the concatenation of two lists that reverses the first one on the fly (cf `List.rev_append`). For instance `rev_append [1;2] [3;4] = [2;1;3;4]`.

 - Recode a reverse function on lists (a.k.a. mirror), providing a linear-time solution, `cleverev: 'a list -> 'a list`.

#### Optional exercise 12': Observing the difference between naive reverse and efficient reverse

To observe the difference of complexity of the two implementations of reverse, you must reverse long enough lists and do the reverse operation a large enough number of time. 

- Define a function `nlist : int -> int list` such that `nlist k` returns a list containing the integers from `k` down to `0`. (if `k` is negative, just return the empty list.)

- Using the function `iter` defined above, define two functions, `naive` and `clever` of type `int -> intlist` such that applied to `k`, they respectively iterate `rev` or `cleverev` `k` times over a list of length 1000.

- Observe experimentally the difference between the execution time of `naive` and `clever` 

- You can use the following `time` function to compare the efficiency of `naive` and `clever`:  

```ocaml
let time f x =
  let t = Sys.time() in
  let fx = f x in
  Printf.printf "execution time: %fs\n" (Sys.time() -. t);
  fx  
``` 
 

#### Optional exercise 12': recode more advances functions

Optionally, consult documentation for iterators `List.fold_left` and `List.fold_right` and try to recode them.



Beware, when using large lists (more than 30000 elements), some functions may fail with `Stack overflow` error. To avoid that, consider using the *tail rercusive* style (see for instance `rev_append` above) ... or switch to some more clever structure (trees below, see also arrays).

## Trees 

No predefined trees in OCaml (too many variants would be interesting, depending on your intentions). Luckily, OCaml allows to easily define our own custom *algebraic types*.

For instance, here is a tree datatype with elements of type `'a` decoring each binary node:

```ocaml
type 'a tree =
 | Node of 'a * 'a tree * 'a tree
 | Leaf

let mytree = Node (1, Node (2,Leaf,Leaf), Leaf)
```

For analyzing a tree, once again a `match...with` syntax is very convenient
(or the `function` keyword, which amounts to a `fun` followed by a `match`).

#### Exercise 13: some usual functions on trees

 - Write the `size` and `depth` functions on trees
 - Write a `tree2list` function flattening a tree into the corresponding list, also known as *infix tree traversal*.

When elements are sorted from left to right during an infix tree traversal, such trees are called Binary Search Trees (BST or "arbre binaire de recherche" (ABR) in French). Using BST may greatly improve the complexity of usual operations. 

#### Optional exercise 13': binary search trees

 - Write a `search` function checking whether some particular element is in a BST. Proceed by dichotomy.
 - Write a `is_bst` function checking whether a tree is indeed a BST. Which complexity could we attain ? 

If the tree is shallow (i.e. of low depth, i.e. complete or almost complete), the `search` function is logarithmic w.r.t. the tree size.
All the challenge is to keep BST trees as shallow as possible, even when adding extra elements in them. See for instance Red-Black trees or AVL trees. 

OCaml provides by default a library of efficient set structures and operations, cf. the `Set` module.

## OCaml Algebraic Types : design your own types

Besides trees, we can continue defining algebraic types fulfilling specific needs. For instance, here is a micro-language for symbolic computation:

```ocaml
type unop = Sin | Cos
type binop = Plus | Mult

type expr =
 | Cst of int
 | Var of string (* variable name *)
 | Unop of unop * expr
 | Binop of binop * expr * expr

(* encoding 3x+sin(y) : *)
let example = Binop(Plus,Binop(Mult,Cst 3,Var "x"),Unop(Sin,Var "y"))
```

Note that transforming a character string such as `"3x+sin(y)"` into the treeish form above (of type `expr`) could be done automatically, but that's far from obvious. That's a lexical and grammatical analysis, for which dedicated tools exists and are quite handy (cf `ocamllex` et `ocamlyacc`).

We could then perform various operations on expressions, such as simbolic simplification or derivation.

#### Exercise 14: formal derivation

  - Write a `deriv : string->expr->expr` function computing a formal derivation with respect to a given variable name.

#### Optional exercise 14': simplification

  - Write a `simpl : expr->expr` function performing as much as possible basic algebraic rules such as `0+x = x`, at any place in the expression.


## OCaml is far more than that ...

OCaml is a full-fledge general-purpose language, so we've only seen a narrow part of it.
It also provides:

  - Exceptions (not only for errors, could be used as a programming style)
  - Modules for programming in the large
  - Primitives for imperative programming (`for`, `while`, references, mutable arrays, ...)
  - Primitives for interacting with the rest of the world (I/O, files, system, networks, ...)
  - Object-oriented programming (the "O" of OCaml...)
  - Lazy programming
  - ...
