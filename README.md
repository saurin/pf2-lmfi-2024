# PF2 -- LMFI 2024-2025
## Programmation fonctionnelle et preuves formelles en Coq
## Functional Programming and Formal Proofs using Coq


You will find here course material for the course entitled "Functional Programming and Formal Proofs using Coq" of LMFI second-year master, academic year 2024-2025.

