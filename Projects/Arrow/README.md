
# Project: Arrow's impossibility theorem #

The project consists in formalizing Arrow's theorem.

This project does not contain a Coq file to be filled: you have to start from scratch!
You may use Coq's standard library to do that. 

Your file should be detailed enough for me to understand your formalization and the proof behind it (see below).

An alternative, more ambitious, project consists in formalizing Gibbard-Satterthwaitr theorem which is another important result of social choice theory showing that under some conditions, strategic voting cannot be avoided.
See:
  - https://fr.wikipedia.org/wiki/Th%C3%A9or%C3%A8me_de_Gibbard-Satterthwaite
  - https://www.sciencedirect.com/science/article/pii/S0165176500003128
  - 



# Background on Arrow's impossibility theorem #


In social choice theory, Arrow’s theorem (1950) says that one cannot design a fair rule that, given the preferences of a group of voters, will provide a global preference, the social choice, respecting some conditions. 
More precisely, the theorem says that the only rule that satisfies some reasonable restrictions that will be explained in the following is to have one voter being a dictator, that is the social choice always follows the dictator's preference, which clearly is not a fair rule to make a decision in a group.

In order to state mathematically the theorem will consider having:
- a finite number N of voters that have to rank 
- a finite set of candidates C. 

Each voter shall have a preference modeled as a linear order on the set of candidates, from the least prefered to the most acceptable one. 


The *social choice* is therefore a function that, given a choice of a prefernce for each voter, returns a preference. 

Here are the three requirements that we would require the rule to satisfy:
– Unanimity: If for two candidates a,b ∈ C all the voters prefer b to a, then the social choice should also prefer b to a. 
– Independence of irrelevant alternatives: For two candidates a, b ∈ A, it should not be possible for a voter to change the social choice on the order of a and b by manipulating his or her preference for a third candidate c. 
– Absence of dictator: for any voter v, if v ranks any candidates a and b differently from all other voters, then the social choice differs from v's preference.

Arrow’s impossibility theorem says that if there are at least three candidates, the three conditions cannot be all fulfilled together.

An alternative way of phrasing the above is that if there are at least three candidates, any social choice rule sc satisfying both unanimity and independence of irrelevent alternatives is a dictartorship: there is a voter d, called the dictator, such that sc is simply the projection on the preference of d...


There are lots of *proofs* in the literature with various assumptions (finite numbers of voters or not, order of preference or pre-order, ...). Below we outline one proof (in the form of an exercize) that you can formalize and give a pointer to several other ones.


#Proof method by decisive subsets#

Let us set some notations first. 

given a finite set of voters V and of candidates C to be ordered, an election is 
a function from V to linear orders on C: E = (lt_i){i_in V}. (we consider that the orders order from the least preference to the greatest preference even though it does not matter.)

A social choice is a function from elections to linear orders on C that satisfies unanimity and intependence of irrelevant alternatives. 

One shall prove that any social choice is a dictatorship. 

Given a social choice rule sc, we say that a subset W of V decides the pair (c,c') when the following holds for any election E:
if for any i in W, c lt_i c', then c sc(E) c'.

- First, prove that if W decides (c,c'), then for any c'' in C \ {c,c'}, W also decides (c,c'') and (c'',c'). Conclude that in this case, W decides any pair (c,c'). Such a set is called decisive.

- Second, prove that there exists a decisive set, namely V itself. 

- Third, assume you are given a minimal decisive set W. Prove that W is a singleton and conclude.


#Alternative proof methods#

You can alternatively try to formalize one of the proof methods given in John Geanakoplos’ paper:
Three Brief Proofs of Arrow’s Impossibility Theorem, Geanakoplos, Economic Theory
Vol. 26, No. 1 (Jul., 2005).
that can be found online on the author's webpage (http://dido.econ.yale.edu/~gean/art/p1116.pdf)


#Possible extensions# 


Once you have formalized the theorem, you may try formalizing the alternative proofs given above, or look at another theorem of social choice theorey, such as Gibbard-Satterthwaite theorem, which deals with *strategic voting*:
https://en.wikipedia.org/wiki/Gibbard–Satterthwaite_theorem
https://fr.wikipedia.org/wiki/Théorème_de_Gibbard-Satterthwaite






