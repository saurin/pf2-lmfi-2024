Formalizing System T and its normalization proof

The project consists in formalizing Gödel's System T together with its proof of strong normalization.

It is a non-guided project for which you can rely on Thierry Joly's lecture notes or on lectures notes I will make available.

