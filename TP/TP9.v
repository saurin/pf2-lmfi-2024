
(** * Inductive proofs on Coq lists *)


(** * Formatting

<<
coqdoc --html TP7.v  -o TP7.html --no-index --no-lib-name  --lib-subtitles --utf8 --parse-comments
>>
*)


(** ** Revision : the Coq definition of lists.

In Coq, the following commands load the standard definition of 
lists and their notations and some usual functions and 
their properties:
*)

Require Import List.
Import ListNotations.

(** In the Coq standard library, these *polymorphic lists* are defined 
via this inductive definition (do not add it in your file):

<<
Inductive list (A : Type) : Type :=
| nil : list A
| cons : A -> list A -> list A.
>>

This definition adds in the current environment a type constructor 
[list : Type->Type]. For each type [A:Type], it returns the associated 
type [(list A) : Type] of lists whose elements are in [A].
This definition also provides two constructors:

<<
nil   :  forall A : Type, list A
cons  :  forall A : Type, A -> list A -> list A
>>

and a induction principle [list_ind] allowing to reason inductively on lists 
(via the [induction] tactic). Try to guess the type of this induction 
principle (and for the adventurous its implementation), and check your 
answer via [Check list_ind] (resp. [Print list_ind]).

*Nota*: in Coq, the universal quantification [forall x : T, U(x)] provides 
a *functional type* which generalizes the arrow type [T -> U], the 
[dependent product].
*)

(** ** Revision : implicit arguments 

Normally, the types of the form [forall A : Type,...] for 
[nil] and [cons] implies that these constructors expect a first 
argument of type [A]. For instance, we would normally have to write 
[(cons nat 3 (nil nat)) : list nat] for a list of numbers.

Fortunately, it is possible to make some arguments be _implicit_, 
and only write here [cons 3 nil], while letting Coq _infer_ internally 
these unwritten arguments. The Coq standard library provides [nil] and
[cons] which are already in implicit mode, and similarly for the standard 
functions on lists.

For you own definitions, you could activate this implicit mode via the
following command (to be put in the file header):

<<
Set Implicit Arguments.
>>

Otherwise, you could also use the syntax [{A:Type}] instead of [(A:Type)] 
for the function arguments that we wish to turn implicit (see for instance 
the [app] function below).
*)


(** ** Revision : Notations 

In addition to implicits, Coq also provides a notation system for 
lightening the syntax. For lists, some notations are available that 
mimic the ones from OCaml (cf. the command [Import ListNotations]):

 - [[]] for [nil]
 - [x :: l] for [cons x l]
 - [[x;y;z]] for [x :: y :: z :: nil], which is itself
    [cons x (cons y (cons z nil))]
*)

(** *** Exercise 14: List concatenation 

The (polymorphic) concatenation operation is defined in Coq via:

<<
Fixpoint app {A:Type} (l1 l2 : list A) : list A :=
 match l1 with
 | [] => l2
 | x :: tl => x :: (app tl l2)
 end.
>>

Once again, do not redefine this definition, but rather use the 
predefined version, and its notation [l1 ++ l2] for [app l1 l2].

 - Show that for all lists [l], we have [nil ++ l = l] and
     [l ++ nil = l].
     Which of these two propositions corresponds to 
     a definitional equality?
 - Show that the concatenation is associative.

*)




(** *** Exercise 15: Length 

 - Define a function [length : forall {A:Type}, list A -> nat]
     such that [length l] returns the length of the list [l].
     Check that your definition is indeed the same than 
     the standard definition of Coq.
 - Show that [length (l1 ++ l2) = length l1 + length l2]
     for all lists [l1] and [l2].

*)



(** *** Exercise 16: Reversing a list

 - Define a function [rev : forall {A}, list A -> list A] reversing 
    the list it receives. For that, you can introduce an auxiliary 
    function [rev_append : forall {A}, list A -> list A -> list A]
    which reverse the first list and catenate it to the second one.
 - Show that [length (rev l) = length l].
 - Show that [rev (l1 ++ l2) = (rev l2) ++ (rev l1)].

*Note*: you may need some intermediate statements!
*)


(** *** Exercise 17: Splitting lists 

Here is two ways to dispatch elements of a list in two sub-lists 
of approximatively the same length. 
For instance [split [1;2;3;4] = ([1;3],[2;4])].
*)

Fixpoint split {A} (l:list A) :=
 match l with
 | [] => ([],[])
 | a::l => let (l1,l2) := split l in (a::l2,l1)
 end.

Fixpoint splitbis {A} (l:list A) :=
 match l with
 | [] => ([],[])
 | [a] => ([a],[])
 | a::b::l => let (l1,l2) := splitbis l in (a::l1,b::l2)
 end.

(**
Show that [split] and [splitbis] always produce the same result. 
The following intermediate result may help:
*)

Lemma split_equiv_aux {A} (l:list A) :
  split l = splitbis l /\ forall x, split (x::l) = splitbis (x::l).
Proof. 
(* Complete here *)
Admitted. 

(** Show that [split] indeed dispatches the elements in the 
two final lists. For instance:
*)

Lemma split_contains {A} (l:list A) :
 let (l1,l2) := split l in
 forall x, In x l <-> In x l1 \/ In x l2.
Proof. 
(* Complete here *)
Admitted. 

(** Show that the two lists produced by [split] have indeed 
(almost) the same length. For instance: *)


Lemma split_length {A} (l:list A) :
 let (l1,l2) := split l in
 length l1 = length l2 \/ length l1 = S (length l2).
Proof. 
(* Complete here *)
Admitted. 
 

(** Show that the sum of the lengths of the two lists 
produced by [split] is indeed the length of the original 
list. For instance: *)



Lemma split_length_add {A} (l:list A) :
 let (l1,l2) := split l in
 length l = length l1 + length l2.
Proof.
(* Complete here *)
Admitted. 
 

Lemma split_div2 {A} (l:list A) :
 let (l1,l2) := split l in
 length l1 = (S (length l))/2 /\ length l2 = (length l) / 2.
Proof.
(* Complete here *)
Admitted. 


(** *** Exercise 18: Richer List Module

Define a richer module signature for lists extending the 
module for lists considered during the course with [app], [rev], 
[split] as well as their specifications and define an 
implementation of this module. 
*)