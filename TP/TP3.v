

(** * TP3: Practice inductive types and fixpoint definitions **)



(** * Practice: TP 3


(** **** Exercise 5 : Usual functions on natural 
numbers.

Define the following functions of type 
[nat -> nat -> nat] (without using the ones of 
Coq standard library of course!):

- [addition]
- [multiplication]
- [subtraction]
- [power]
- [gcd]

We recall that the Ackermann-P\u00e9ter function, AP, 
is defined as: 
- AP(0,n) = n+1
- AP(m+1, 0) = AP(m, 1)
- AP(m+1, n+1) = AP(m, AP(m+1, n)).

Try defining [AP], of type [nat -> nat -> nat], 
in the most natural way, based on its definition. 
What problem do you encounter?

What possible workaround can you imagine?

*)

(* *)

(** ** Recursive definitions

**** Exercise 6 : Fibonacci

- Define a function [fib] such that [fib 0 = 0], 
 [fib 1 = 1] then [fib (n+2) = fib (n+1) + fib n].  
 (you may use a [as] keyword to name some subpart 
 of the [match] pattern ("motif" en fran\u00e7ais)).
- Define an optimized version of [fib] that 
  computes faster that the previous one by using 
  Coq pairs.
- Same question with just natural numbers, no pairs. 
  Hint: use a special recursive style called 
  "tail recursion".
- Load the library of binary numbers via 
  [Require Import NArith].
  Adapt you previous functions for them now to 
  have type [nat -> N]. What impact does it have 
  on efficiency ? Is it possible to simply obtain 
  functions of type [N -> N] ?
*)

(* *)

(** **** Exercise 7 : Fibonacci though matrices

- Define a type of 2x2 matrices of numbers 
  (for instance via a quadruple).
- Define the multiplication and the power of 
  these matrices.
  Hint: the power may use an argument of type 
  [positive].
- Define a fibonacci function through power of 
  the following matrix:

<<
1 1
1 0
>>
*)

(* *)

(** **** Exercise 8 : Fibonacci decomposition 
of numbers

We aim here at programming the Zeckendorf theorem 
in practice : every number can be decomposed in a 
sum of Fibonacci numbers, and moreover this 
decomposition is unique as soon as these Fibonacci 
numbers are distinct and non-successive and with 
index at least 2.

Load the list library: *)

Require Import List.
Import ListNotations.

(**
- Write a function [fib_inv : nat -> nat] such 
  that if [fib_inv n = k] then [fib k <= n < fib 
  (k+1)].
- Write a function [fib_sum : list nat -> nat] 
  such that [fib_sum [k_1;...;k_p] = fib k_1 + 
  ... + fib k_p].
- Write a function [decomp : nat -> list nat] 
  such that [fib_sum (decomp n) = n] and 
  [decomp n] does not contain 0 nor 1 nor any 
  redundancy nor any successive numbers.
- (Optional) Write a function [normalise : 
  list nat -> list nat] which receives a 
  decomposition without 0 nor 1 nor redundancy, 
  but may contains successive numbers, and builds 
  a decomposition without 0 nor 1 nor redundancy 
  nor successive numbers. You might assume here 
  that the input list of this function is sorted 
  in the way you prefer. 

**)

(* *)


(** **** Exercise 9: Classical exercises on lists

Program the following functions, without using the 
corresponding functions from the Coq standard library :

 - [length]
 - concatenate ([app] in Coq, infix notation [++])
 - [rev] (for reverse, a.k.a mirror)
 - [map : forall {A B}, (A->B)-> list A -> list B]
 - [filter : forall {A}, (A->bool) -> list A -> list A]
 - at least one [fold] function, either [fold_right] or [fold_left]
 - [seq : nat -> nat -> list nat], such that 
  [seq a n = [a; a+1; ... a+n-1]]

Why is it hard to program function like [head], [last] 
or [nth] ? How can we do ?
*)

(* *)
(** **** Exercise 10:  Some executable predicates on lists

 - [forallb : forall {A}, (A->bool) -> list A -> bool].
 - [increasing] which tests whether a list of numbers is 
  strictly increasing.
 - [delta] which tests whether two successive numbers of 
  the list are always apart by at least [k].
 
*)

(* *)

(** **** Exercise 11: Powerset

Write a [powerset] function which takes a list [l] and returns 
the list of all subsets of [l].
For instance [powerset [1;2] = [[];[1];[2];[1;2]]]. 
The order of subsets in the produced list is not relevant.

*)

(** **** Exercise 12: Mergesort

- Write a [split] function which dispatch the elements of 
 a list into two lists of half the size (or almost). It is 
 not important whether an element ends in the first or second 
 list. In particular, concatenating the two final lists will 
 not necessary produce back the initial one, just a permutation 
 of it.
- Write a [merge] function which merges two sorted lists into a 
 new sorted list containing all elements of the initial ones. 
 This can be done with structural recursion thanks to a inner 
 [fix] (see [ack] in the session 3 of the course). 
 - Write a [mergesort] function based on the former functions.
*)



(* *)

(** ** Some inductive types

**** Exercise 13: Binary trees with distinct 
internal and external nodes.

By taking inspiration from the definition of 
lists above, define an inductive type [iotree] 
depending on two types [I] and [O]such that 
every internal node is labelled with an element 
of type I and every leaf is labelled with an 
element of type O. 
*)

(* *)

(** **** Exercise 14: Lists alternating elements 
of two types.

By taking inspiration from the definition of 
lists above, define an inductive type [ablists] 
depending on two types [A] and [B] which is 
constituted of lists of elements of types 
alternating between [A] and [B].
*)



(* *)
(** **  Lists with Fast Random Access


We consider here data structures that are purely 
functional (also said _persistent_ or _immutable_) 
and allow us to encode _lists_. We have already 
seen the Coq standard implementation of lists, but 
more generally a list is here a finite sequence of 
elements  where the order of elements in the list 
is meaningful and where the operations "on the left" 
are efficient, both a [cons] extension operation and 
a [head] and a [tail] access functions.

Actually, instead of two separated [head] and [tail] 
functions, we will consider here a unique function 
[uncons] doing both. More precisely, [uncons l = Some 
(h,t)] whenever the list has a head [h] and a tail [t] 
and [uncons l = None] whenever [l] is empty.

We will also focus on a [nth] function for our lists, 
allowing us to perform "random" access anywhere in a 
list, not just on the left.

The goal of this work is to implement lists in various 
manners, ending with [nth] functions of logarithmic 
complexity without compromising too much the cost of 
functions [cons] and [uncons] (and ideally keeping 
this cost constant). Here, the complexity we consider 
is the number of access to any part of any innner 
substructure, in the worst case, expressed in function 
of the number of elements present in the list. In a 
first time, we neglect the cost of any arithmetical 
operations we may perform on integers.
*)

(* *)
(**  **** Exercise 15 : Implementation via regular Coq lists

Start a _module_ to isolate the code of this exercise from 
the next ones (Modules will be studied in future lectures): *)

Module RegularList.

(** 
Implement the following operations on the Coq usual [list] 
datatype, and give their complexity.
*)

cons : forall {A}, A -> list A -> list A.
uncons : forall {A}, list A -> option (A * list A).
nth : forall {A}, list A -> nat -> option A.

(** Finish the previous module : *)

End RegularList.

(* *)
(** **** Exercise 16 : Implementation via b-lists (a.k.a 
binary lists)

We will now devise a data-structure where both [cons], 
[uncons] and [nth] operations will all be logarithmic 
(at worst).

We call _b-list_ a (regular) list of perfect binary trees 
with the following properties : the datas are stored at 
the leaves of the trees, and the sizes of the trees are 
strictly increasing when going through the external list 
from left to right. The elements of a b-list are the 
elements of the leftmost tree (from left to right) then 
the elements of the next tree, until the elements of the 
rightmost tree.

- Start again a module dedicated to this exercise : 
  [Module BList.]
- Define a Coq type [blist : Type -> Type] corresponding 
  to b-list, i.e. list of binary trees with data at the 
  leaves. No need to enforce in [blist] the other 
  constraints (trees that are all perfect and are of 
  strictly increasing sizes). But you should always 
  maintain these invariants when programming with [blist]. 
  And if you wish you may write later boolean tests 
  checking whether a particular [blist] fulfills these 
  invariants. 
- Write some examples of small b-lists. In particular 
  how is encoded the empty b-list ? Could we have two b-lists 
  of different shape while containing the same elements ?
- (Optional) Adjust your [blist] definition in such a way that 
  retrieving the sizes (or depth) of a tree inside a [blist] 
  could be done without revisiting the whole tree.
- On this [blist] type, implement operations [cons], 
  [uncons] and [nth]. Check that all these operations are 
  logarithmic (if you did the last suggested step).
- Finish the current module : [End BList.]

This b-list structure shows that a fast random access in a 
list-like structure is indeed possible. But this comes here 
at an extra cost : the operations "on the left" ([cons] and 
[uncons]) have a complexity that is not constant anymore. 
We will see now how to get the best of the two worlds. But 
first, some arithmetical interlude : in the same way the 
b-lists were closely related with the binary decomposition 
of number, here comes an alternative decomposition.
*)

(* *)
(** **** Exercise 17 : Skew binary number system

We call skew binary decomposition (or sb-decomposition) of a 
number its decomposition as sum of numbers of the form 2^k -1 
with k>0. Moreover all the terms in this sum must be differents, 
except possibly the two smallest ones.

- Write a [decomp] function computing a sb-decomposition for 
any natural number. Could we have different ordered 
sb-decompositions of the same number ?

- Write two functions [next] and [pred] that both take the 
sb-decomposition of a number, and compute the sb-decomposition 
of its successor (resp. precedessor), without trying to convert 
back the number in a more standard representation, and moreover 
without using any recursivity (no [Fixpoint]) apart from a 
possible use of [Nat.eqb].

For the last section, we admit that the sb-decomposition of a 
number [n] is a sum whose number of terms is logarithmic in [n].
*)

(* *)
(** **** Exercise 18 : Implementation via sb-lists (skew binary 
lists)

- Based on all previous questions, propose a data-structure of 
*skew binary lists* for which [cons] and [uncons] have constant 
complexity while [nth] is logarithmic.

- Compare these sb-lists with the usual Coq lists : what reason 
may explain that sb-lists are not used universally instead of 
usual lists ?

**** Possible extensions

- For b-lists and sb-lists, code a function [drop] of logarithmic 
complexity such that [drop k l] returns the list [l] except its 
first [k] elements.

- For b-lists and sb-lists, code a function [update_nth] such 
that [update_nth l n a] is either [Some l'] when [l'] is [l] 
except for [a] at position [n], or [None] when [n] is not a 
legal position in [l].

- Use a binary representation of numbers instead of [nat], 
and compute the complexity of all operations b-list and 
sb-list operations when taking in account the cost of the 
arithmetical sub-operations.
*)

(** **** Reference

- #<a 
href="https://en.wikipedia.org/wiki/Skew_binary_number_system">https://en.wikipedia.org/wiki/Skew_binary_number_system</a>#
- Okasaki, Purely Functional Data Structures, 1998, Cambridge University Press
*)

