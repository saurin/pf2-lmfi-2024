(** * TP4: Inductive types and dependent types **)


(** **** Exercise 3bis : Church numerals

Encode in Coq the Church numerals, for which the number 
<<n>> is represented by <<λf.λx.(f (f (... (f x))))>> where 
<<f>> is applied <<n>> times.

More precisely, define (without using [nat] nor any other inductive type):

 - a type [church : Type]
 - two constants [zero] and [one] of type [church]
 - a function [church_succ] of type [church->church]
 - two functions [church_plus] and [church_mult] of type [church->church->church]
 - a function [church_power]
 - a test [church_iszero]
 - two functions [nat2church : nat -> church] and [church2nat : church -> nat] 
   converting between Coq inductive definition of nats and the Church encoding 
   of natural numbers. 

*)

(** **** Exercise 3ter: Predecessor and subtraction

In addition, we define:
- [church_pred] of type [church -> church], which associates zero to zero and 
to any other church numeral, associates its predecessor 
*)

(** Using [church_pred], try and define a function [church_minus] of type 
[church -> church -> church] which computes the difference between to nats: 
it should return [zero] if the first argument is less or equal to the 
second one and the difference otherwise.

Analyze carefully why universe polymorphism is needed in this case! 
(try defining this first without setting universe polymorphism...)
*)


(** **** Exercise 12: Mergesort

- Write a [split] function which dispatch the elements of 
 a list into two lists of half the size (or almost). It is 
 not important whether an element ends in the first or second 
 list. In particular, concatenating the two final lists will 
 not necessary produce back the initial one, just a permutation 
 of it.
- Write a [merge] function which merges two sorted lists into a 
 new sorted list containing all elements of the initial ones. 
 This can be done with structural recursion thanks to a inner 
 [fix] (see [ack] in the session 3 of the course). 
 - Write a [mergesort] function based on the former functions.
*)



(* *)

(** ** Some inductive types

**** Exercise 13: Binary trees with distinct 
internal and external nodes.

By taking inspiration from the definition of 
lists above, define an inductive type [iotree] 
depending on two types [I] and [O]such that 
every internal node is labelled with an element 
of type I and every leaf is labelled with an 
element of type O. 
*)

(* *)

(** **** Exercise 14: Lists alternating elements 
of two types.

By taking inspiration from the definition of 
lists above, define an inductive type [ablists] 
depending on two types [A] and [B] which is 
constituted of lists of elements of types 
alternating between [A] and [B].
*)



(* *)
(** **  Lists with Fast Random Access


We consider here data structures that are purely 
functional (also said _persistent_ or _immutable_) 
and allow us to encode _lists_. We have already 
seen the Coq standard implementation of lists, but 
more generally a list is here a finite sequence of 
elements  where the order of elements in the list 
is meaningful and where the operations "on the left" 
are efficient, both a [cons] extension operation and 
a [head] and a [tail] access functions.

Actually, instead of two separated [head] and [tail] 
functions, we will consider here a unique function 
[uncons] doing both. More precisely, [uncons l = Some 
(h,t)] whenever the list has a head [h] and a tail [t] 
and [uncons l = None] whenever [l] is empty.

We will also focus on a [nth] function for our lists, 
allowing us to perform "random" access anywhere in a 
list, not just on the left.

The goal of this work is to implement lists in various 
manners, ending with [nth] functions of logarithmic 
complexity without compromising too much the cost of 
functions [cons] and [uncons] (and ideally keeping 
this cost constant). Here, the complexity we consider 
is the number of access to any part of any innner 
substructure, in the worst case, expressed in function 
of the number of elements present in the list. In a 
first time, we neglect the cost of any arithmetical 
operations we may perform on integers.
*)

(* *)
(**  **** Exercise 15 : Implementation via regular Coq lists

Start a _module_ to isolate the code of this exercise from 
the next ones (Modules will be studied in future lectures): *)

Module RegularList.

(** 
Implement the following operations on the Coq usual [list] 
datatype, and give their complexity.


cons : forall {A}, A -> list A -> list A.
uncons : forall {A}, list A -> option (A * list A).
nth : forall {A}, list A -> nat -> option A.
*)

(** Finish the previous module : *)

End RegularList.

(* *)
(** **** Exercise 16 : Implementation via b-lists (a.k.a 
binary lists)

We will now devise a data-structure where both [cons], 
[uncons] and [nth] operations will all be logarithmic 
(at worst).

We call _b-list_ a (regular) list of perfect binary trees 
with the following properties : the datas are stored at 
the leaves of the trees, and the sizes of the trees are 
strictly increasing when going through the external list 
from left to right. The elements of a b-list are the 
elements of the leftmost tree (from left to right) then 
the elements of the next tree, until the elements of the 
rightmost tree.

- Start again a module dedicated to this exercise : 
  [Module BList.]
- Define a Coq type [blist : Type -> Type] corresponding 
  to b-list, i.e. list of binary trees with data at the 
  leaves. No need to enforce in [blist] the other 
  constraints (trees that are all perfect and are of 
  strictly increasing sizes). But you should always 
  maintain these invariants when programming with [blist]. 
  And if you wish you may write later boolean tests 
  checking whether a particular [blist] fulfills these 
  invariants. 
- Write some examples of small b-lists. In particular 
  how is encoded the empty b-list ? Could we have two b-lists 
  of different shape while containing the same elements ?
- (Optional) Adjust your [blist] definition in such a way that 
  retrieving the sizes (or depth) of a tree inside a [blist] 
  could be done without revisiting the whole tree.
- On this [blist] type, implement operations [cons], 
  [uncons] and [nth]. Check that all these operations are 
  logarithmic (if you did the last suggested step).
- Finish the current module : [End BList.]

This b-list structure shows that a fast random access in a 
list-like structure is indeed possible. But this comes here 
at an extra cost : the operations "on the left" ([cons] and 
[uncons]) have a complexity that is not constant anymore. 
We will see now how to get the best of the two worlds. But 
first, some arithmetical interlude : in the same way the 
b-lists were closely related with the binary decomposition 
of number, here comes an alternative decomposition.
*)

(* *)
(** **** Exercise 17 : Skew binary number system

We call skew binary decomposition (or sb-decomposition) of a 
number its decomposition as sum of numbers of the form 2^k -1 
with k>0. Moreover all the terms in this sum must be differents, 
except possibly the two smallest ones.

- Write a [decomp] function computing a sb-decomposition for 
any natural number. Could we have different ordered 
sb-decompositions of the same number ?

- Write two functions [next] and [pred] that both take the 
sb-decomposition of a number, and compute the sb-decomposition 
of its successor (resp. precedessor), without trying to convert 
back the number in a more standard representation, and moreover 
without using any recursivity (no [Fixpoint]) apart from a 
possible use of [Nat.eqb].

For the last section, we admit that the sb-decomposition of a 
number [n] is a sum whose number of terms is logarithmic in [n].
*)

(* *)
(** **** Exercise 18 : Implementation via sb-lists (skew binary 
lists)

- Based on all previous questions, propose a data-structure of 
*skew binary lists* for which [cons] and [uncons] have constant 
complexity while [nth] is logarithmic.

- Compare these sb-lists with the usual Coq lists : what reason 
may explain that sb-lists are not used universally instead of 
usual lists ?

*)

(** **** Exercise 19: Possible extensions

- For b-lists and sb-lists, code a function [drop] of logarithmic 
complexity such that [drop k l] returns the list [l] except its 
first [k] elements.

- For b-lists and sb-lists, code a function [update_nth] such 
that [update_nth l n a] is either [Some l'] when [l'] is [l] 
except for [a] at position [n], or [None] when [n] is not a 
legal position in [l].

- Use a binary representation of numbers instead of [nat], 
and compute the complexity of all operations b-list and 
sb-list operations when taking in account the cost of the 
arithmetical sub-operations.
*)



(** **** Exercice 20 : programming with vectors
*)


Inductive vect (A:Type) : nat -> Type :=
 | Vnil : vect A 0
 | Vcons n : A -> vect A n -> vect A (S n).

Arguments Vnil {A}.
Arguments Vcons {A n}.

Definition testvec := Vcons 1 (Vcons 2 (Vcons 3 Vnil)).

Require Import List.
Import ListNotations.

Fixpoint v2l {A} {n} (v : vect A n) : list A :=
  match v with
  | Vnil => []
  | Vcons x v => x::(v2l v)
  end.

Fixpoint l2v {A} (l: list A) : vect A (length l) :=
  match l with
  | [] => Vnil
  | x :: l' => Vcons x (l2v l')
  end.

Definition length' {A} {n} (v: vect A n) : nat := n.


Definition Vhead {A} {n} (v:vect A (S n)) : A :=
 match v with
 | Vcons x _ => x
 end.

(**
- Define a tail function on vectors:
 [Vtail
     : forall (A : Type) (n : nat), vect A (S n) -> vect A n]. 
- Define an inverse to the cons function on vectors which takes 
a non-empty vectors and returns the pair of its head and tail:
[Vuncons
     : forall (A : Type) (n : nat), vect A (S n) -> A * vect A n]
- Define a map function on vectors: 
 [Vmap: forall (A B : Type) (n : nat), 
 vect A n -> (A -> B) -> vect B n]
- Define a _zip_ function on vectors, that takes two vectors 
 of the same length and builds the vectors of the pairs:
 [Vzip : forall (A B : Type) (n : nat),
       vect A n -> vect B n -> vect (A * B) n]
- Define a split function that splits a vectors in a pair of two vectors, 
 the first one containing a prefix of a specified size and the second 
containing the rest of the input vector:
[Vsplitat
     : forall (A : Type) (l r : nat),
       vect A (l + r) -> vect A l * vect A r]
*)


(** **** Exercise 21 : Finite sets 

We recall the definition of the inductive family [Fin]. *)


Inductive Fin : nat -> Type :=
 | Zero n : Fin (S n)
 | Succ n : Fin n -> Fin (S n).

Arguments Zero {n}.
Arguments Succ {n}.

(** Define: 
- a function [Fin2nat] which associates for any [n], to an
element [p: Fin n], the corresponding natural number. 
- a function [FinInj] which injects [Fin n] into [Fin (n+m)] 
for any [n,m : nat].
- defining a function [FinInj'] which injects [Fin n] 
into [Fin (m+n)] for any [n,m : nat] is not so simple 
(and not doable at this point of the course). 
Can you explain why? *)


(** **** Exercise 22 : Safe access to vectors

The type [Fin] of "bounded" integers provides a neat 
way to specify and implement a safe access function to 
the n-th element of a vector (type [vect]).

Indeed, for a vector v in type [vect A n], we 
can access any position [p] as long as [p] is in [Fin n]. 
One shall thus return an element in A, without bothering 
passing a default value or returning in option A as we had 
to do for lists. 

Write a function [Vnth] of type: 

[Vnth {A} {n} (p:Fin n) : vect A n -> A.]

Two important indications to follow: 
- the structurally decreasing argument shall be the 
argument in [Fin n]
- Vnth returns a function: build the function 
([fun v => ...]) in each return clause of the 
pattern matching, not globally to the definition of Vnth.

Then, test your function on vector [testvec].

Try to write the function in other ways (for instance passing 
the vector as a parameter) should fail: try some attempts to 
experiment that.

*)


(** **** Exercise 23 (extra) : perfect binary trees, dependent case

Redo Exercise 16, but this time use a dependent type for trees 
in `blist`, to ensure that all trees are necessarily perfect.

If not done, I suggest that you do in parallel exercise 16 
as well as this one, completing in parallel a module [BList]
and a module [BList_DepType] first! 

In order to do this exercise, you fill soon see that managing 
dependent types, one needs "cast" functions in order to 
enforce equality of types: 

*)


Require Import Bool Arith List.
Import ListNotations.
Set Implicit Arguments.

Module BList_DepType.

Inductive fulltree (A:Type) : nat -> Type :=
| FLeaf : A -> fulltree A 0
| FNode n : fulltree A n -> fulltree A n -> fulltree A (S n).

Arguments FLeaf {A}.
Arguments FNode {A} {n}.


Definition treecast {A} {n} {m} (v: fulltree A n)(h : n = m) : fulltree A m :=
  match h with
  | eq_refl => v
  end.

(** In order to use treecast, one need a proof of equality
between the nat expression used in the domain and that used 
in the codomain. 

For that, one shall refine the usual boolean equality 
test by using the following, that returns not only the 
boolean value, in the form of the left/rigth constructor, 
but also (dis)equality proofs: *)

Check Nat.eq_dec.

(* For instance you will use it with:
   match Nat.eq_dec n m with
        | left p => (* here (p : n=m) *) ...
        | right _ => (* here n <> m *) ...
        end
*)

End BList_DepType.


