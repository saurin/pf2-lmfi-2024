(* In this file, you will find several modules.

The first one makes you use the Excluded Middle and the Axiom of
Choice. This is good practice for using axioms or lemmas of an
imported library, admitted or proved outside your local proof.


The second module is helps you discover the library of natural numbers
in MathComp. You do not have to use the ssreflect language and can
still do proofs in the tactics you are used too.

The third module is about topology and linear maps between normed
spaces.

I stronlgy advise you to go through the first module quickly, and then
to choose between arithmetic or topology according to your
preference*)

Module Axioms.

Axiom EM : forall P : Prop, P \/ ~ P.

Section Classical.


  (* The following is an example on how to use an axiom or a lemma
  proved in the library or is the file. Here, we make us of the EM
  axiom, apply it to a chosen variable, and destruct it to use its
  components *)
  
Theorem NNPP : forall P : Prop, ~ ~ P -> P.
Proof.
  intros P. unfold not.
  intros H.
  destruct (EM P) as [HP | HnP].
  -   exact HP.
  -   exfalso.
  unfold not in HnP.
  apply H.
  exact HnP.
Qed.

(*The only de Morgan Law for which you need EM*)
Theorem de_morgan_not_and : forall P Q : Prop, ~ (P /\ Q) -> (~ P) \/ (~ Q).
Proof.
Admitted.

(*You can use the tactic case on EM applied to a proposition *)
Theorem Peirce_law : forall P Q : Prop, ((P -> Q) -> P) -> P.
Proof.
Admitted.

(* Without using EM as an axiom, you can prove *)
Theorem Pierce_EM : (forall P Q : Prop, ((P -> Q) -> P)->P) -> (forall P, (P \/ ~ P)).
Proof.
Admitted.


(* Do you want to try and go through double negation to prove that
Pierce implies the Excluded Middle ? *)
End Classical.


Section Fonctions.
Definition surjective {X Y} (f : X -> Y) := forall (y : Y), exists (x : X), (f x = y).

Definition injective {X Y} (f : X -> Y) := forall x1, forall  x2, (f x1 = f x2 -> x1=x2).

Definition bijective {X Y} (f : X -> Y) := (injective f) /\ (surjective f).

Definition inverse_gauche {X Y} (g : Y -> X) (f : X -> Y) := forall (x : X), g (f x) = x.

Definition inverse_droite {X Y} (g : Y -> X) (f : X -> Y) := forall x, f (g x) = x.


(* from Coq.Logic.ClassicalChoice *)
Axiom choice : forall (A B : Type) (R : A->B->Prop),
    (forall x : A, exists y : B, R x y) ->
  exists f : A->B, (forall x : A, R x (f x)).

Definition graphe {X Y} (f : X -> Y) y x := f x = y.

(* to get started *)
Lemma fun_graphe {X Y} f : surjective f -> (forall y : Y, exists x : X, graphe f y x).
Proof.
Admitted.

(*now you need to use the axiom of choice *)
Lemma surj_inverse_d {X Y} (f : X -> Y) :
  surjective f -> exists g, inverse_droite g f.
Proof.
Admitted.

Lemma bij_inversible {X Y} (f : X -> Y) :
  bijective f -> exists g, inverse_droite g f /\ inverse_gauche g f.
Proof.
Admitted.


Lemma inversible_bijective {X Y} (f : X -> Y) :
  (exists g, inverse_droite g f /\ inverse_gauche g f) -> bijective f.
Proof.
Admitted.

End Fonctions.
End Axioms.




Module MathComp.

  (*The ssreflect language is conservative over coq tactics. You can
  always use the tactics you know, but most of them have an equivalent
  is the ssreflect language that you can also use. *)


(* Remember our basic sets of tactics: 
- "move => H" and "move: H" to put an hypothesis from the goal to context, and
  vice-versa. To break H, use move => [A B]
- The signs // /= can be use to refer to an hypothesis in the context and to
  apply basic computations.  When in doubt, keep calm and try " move => //="...
- exists: x  allows to prove an existential goal
- rewrite (with all its patterns) does rewriting.
- appply: H. applies the hypothesis or lemma H. 
- A "view" is a lemma of the form T : H -> H' or T: H <-> H', which transforms
    an hypothesis on the top of the stack when we do  move=> /H.. 
- have lem : .... allows you to introduce an intermediate goal. Instead of 
  "lem", you can also write "->" or "<-" to rewrite, or "[]"to break the lemma
  in its sub logical parts, and you might also want to apply a view direclty 
  have /view : ..
  *)

(* Basic and advanced cheat sheets  *)
(* http://www-sop.inria.fr/marelle/math-comp-tut-16/MathCompWS/basic-cheatsheet.pdf *)
(*  http://www-sop.inria.fr/marelle/math-comp-tut-16/MathCompWS/cheatsheet.pdf *)
  (* We are going to do a little bit of arithmetic*)

  (* The keyword to look for lemmas about divisibility and
  multiplication are "div" "mul", for proofs by modularity it's
  "modn"*)

Section Basic_arith.

  
From mathcomp Require Import all_ssreflect.
Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

(* These are borrowed from
https://team.inria.fr/marelle/en/coq-winter-school-2018-2019-ssreflect-mathcomp/
*)

Implicit Type p q r : bool.
Implicit Type m n a b c : nat.

Lemma ex1 x y : 8 * y <> 6 * x + 1.
Proof.
Check congr1.
Check (congr1 (modn^~ 2)).
(*move => H.
move : (congr1 (modn^~ 2) H). *)
move=> /(congr1 (modn^~ 2)).
(* Check these : modnMml  mul0n modnDml modnMm *)
Admitted.

(* Most statement in Mathcomp are boolean valued, so mathcomp spends
its time switching from booleans to propositions*)

Print Bool.reflect.

(* The key lemmas to go from one boolean connector to the other are
the "_P" statements, such as andP iffP orP eqP negP ltnP... Check these*)

About iffP.
About idP.

(* Example: Equalities between natural numbers*)
Lemma eqnP {n m : nat} :
  reflect (n = m) (eqn n m).
Proof. 
apply: (iffP idP).
  elim: n m => [|n IH] m; case: m => // m Hm.
  by rewrite (IH m Hm).
move=> def_n; rewrite {}def_n.
Undo.
move=> ->. (* move + rewrite + clear idiom *)
by elim: m.
Qed.


(*This is what views are for:*)
 
About andP.

Lemma example n m k : k <= n ->
  (n <= m) && (m <= k) -> n = k.
Proof.
(*move=> lekn /andP H; case: H => lenm lemk. *)
(* The previous line works, the following is juste the same thing in short*)  
move=> lekn /andP[lenm lemk]. (* view + case idiom *)
Search "leq".
Check anti_leq.
Print antisymmetric.
Abort.


(*Now a long section about  *)
Definition sol n a b := [&& a > 0, b > 0 & 2 ^ n == a ^ 2 + b ^ 2].
(* What does it mean ?*)

Lemma sol0 a b : ~~ sol 0 a b.
Proof.
(* Check ltn_eqP expn_gt0 leq_add*)
Admitted.

Lemma sol1 a b : sol 1 a b = (a == 1) && (b == 1).
Proof.
Admitted.

Lemma mod4Dsqr_even a b : (a ^ 2 + b ^ 2) %% 4 = 0 -> (~~ odd a) && (~~ odd b).
Proof.
have sqr_x2Dy_mod4 x y : (x * 2 + y) ^ 2 = y ^ 2 %[mod 4].
admit.
About divn_eq.
Search _ modn odd.
Admitted.

Lemma sol_are_even n a b : n > 1 -> sol n a b -> (~~ odd a) && (~~ odd b).
Proof.
Check expnS.
Check  mulnA.
Check modnMr.
Admitted.

Lemma solSS n a b : sol n.+2 a b -> sol n a./2 b./2.
Proof.
Search _ odd double.
Search _ "eq" "mul". 
Admitted.

(* Now you can use the previous results to prove the following *)

Lemma sol_even a b n : ~~ sol (2 * n) a b.
Proof.
Admitted.

Lemma sol_odd a b n : sol (2 * n + 1) a b = (a == 2 ^ n) && (b == 2 ^ n).
Proof.
Admitted.


(* New independant exercise *)
Lemma square_and_cube_modulo7 (m n p : nat) : m = n ^ 2 -> m = p ^ 3 ->
  (m == 0 %[mod 7]) || (m == 1 %[mod 7]).
Proof.
(* Proof suggestion. *)
(* 1. First subsitute the first equality inside the rest and get rid of m *)
(*    see rewrite or intro patterns (after the move=>) *)
(* 2. Take the modulo of the equation n ^ 2 = p ^ 2. *)
(*    You can use have: to pose an intermediate statement. *)
(*    Or you can use a congr1 in a forward view. *)
(* 3. Then push the modulo "to the leaves" / inside *)
(*    Hint: *) Search modn expn.
(* 4. Generalize using the fact that a modulo 7 is smaller than 7 *)
(*    Hint: *) Search leq modn in div.
(* 5. Perform 7 case analysis for each modulo 7 *)
(*    Use repeated case or repeated [] inside move=> *)
Admitted.

End Basic_arith.  

Section Topology.
From mathcomp Require Import all_ssreflect ssralg ssrint ssrnum finmap matrix.
From mathcomp Require Import fingroup action.
From mathcomp Require Import rat interval zmodp vector fieldext falgebra galois.
From mathcomp Require Import mathcomp_extra boolp classical_sets functions.
From mathcomp Require Import cardinality set_interval (*Rstruct*).
From mathcomp Require Import ereal reals signed topology prodnormedzmodule normedtype.
Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.


(* This sheets feature one exercice on basic number theory, two exercises on
 basic topology, and one exercice on boundedness in normed spaces *)

Import Order.TTheory GRing.Theory Num.Theory.
Import numFieldTopology.Exports.


Local Open Scope classical_set_scope.
Local Open Scope ring_scope.

(* Information about notations and theorem can be found in the header of 
https://github.com/math-comp/analysis/blob/master/theories/topology.v 
 But should not be needed for the next two exercises *)  

(*Let us show that a Topological space is hausdorff if and only if its diagonal
is closed. You might want to unfold definitions to understand how they are
structured, but it is not necessary to unfold them in the final version of the
proof. Otherwise, no search nor any external lemmas are needed.  *)

(*Here's the idea for the proof: a space is hausdorff by definition if any two
different points x y can always be separated by a neighborhood V *)
Print hausdorff_space.
(* Another way to state that, which is what is defined above,
 is that if x is is every neighborhood of y, then x=y *)
 (* Now supposed the diagonal is closed. Then if y is in every neighborhood that
 contains x, then (y,x) is in the closure of the diagonal, and thus y=x*)
 
 (* More formally, the diagonal is  [set (x, x) | x in [set: T]]. *)

 (*[set x : T | P] == set of points x : T such that P holds.
 T can be made implicit : [set x | P] *)

 (* Neigborhoods in the products are exactly product of neighborhoods.*)
Print closed.
Print closure.

(* A set C is closed if it contains its closure, that is if its contains all the
points p such that all neighborhood of p intersects C .*)

(*Due to the too recent port of mathcomp-analysis to Hierarchy Builder, some
types might not appear as they should. If your object appears with an ugly type
in your context (after doing "move=>  A"), please do "move=> /= A" instead*)

Lemma closed_diag_hausdorff (T : topologicalType) :
  closed [set (x, x) | x in [set: T]] <-> hausdorff_space T. 
Proof. 
split.
Admitted.

(* Continuity uses the limits --> notation, wich is just about filter inclusion.
*)
About continuous.
(*You will witness the notation F --> x where F is a filter. This is a notation
for (nbhs x) `<=` F, the canonical filter of neighborhoods of x is included in F
*)

(* The notation f @^-1` A is used to denote the reverse image of A (included in
F) by f : E -> F *)

Lemma closed_graph_function (T U : topologicalType) (f  : T -> U): 
  hausdorff_space U -> continuous f -> closed [set xy | f(xy.1) = xy.2].
Proof.
Admitted. 

(* The next exercise concerns normed spaces.*) 
(* The notation "\near" is used in mathcomp-analysis to represent filter
inclusion: \forall x \near F, P x <-> F (fun x => P x). A whole set of tactics
and lemmas are available to reason with near.

In normed spaces, these tactics allow to avoid giving the explicit distance
between two points, and reasoning with explicit epsilon.

For now, you can get back to filter reasoning with nearE, and explicit handling
of epsilon thanks to a whole set of rewriting lemmas*)

(* Notations :
 - _ *: _ : scalar multiplication, search for "scale" in lemmas' names.
 - _ * _ , _ + _, as usual, called "mult" and "add" *)

(* Searching lemmas : 
-  By name : Search "scale".
-  By pattern : Search _ "1*:_". The first _ is the space to be used of a
   pattern that needs to be in the conclusion of the lemma.
-  Both: Search "scale" (_+_) (_*:_).
-  Somme lemmas use predicate instead of notations and are harder to find. For example :    *)
Check scale1r.
(* uses "left_id" to denote "1*:r=r". *)


(*The following is another exercise to use continuity. 
These are the lemmas to be used:*)
About ex_bound.
About linear0.
About nbhs_le.

Lemma continuous_linear_bounded_at0 (R : numFieldType) (V W : normedModType R)
    (f : {linear V -> W}) :
  {for 0, continuous f} -> bounded_near f (nbhs 0).
Proof.
Admitted.

(* The following is the generalized version at any point x. If you want, you can
try to do it without near *)
(* Then we still suggest to use a structure that allow automation on positive
numbers: https://github.com/math-comp/analysis/blob/master/theories/signed.v *)
Lemma with_near (R : numFieldType) (V W : normedModType R)
    (x : V) (f : {linear V -> W}) :
  {for 0, continuous f} -> bounded_near f (nbhs x).
Proof.
rewrite /prop_for /continuous_at linear0 /bounded_near => f0.
near=> M; apply/nbhs0P.
 near do rewrite /= linearD (le_trans (ler_normD _ _))// -lerBrDl.
apply: cvgr0_norm_le; rewrite // subr_gt0.
by []. (* This is where it happens*)
Unshelve. all: by end_near. Qed.

End Topology.
End MathComp.
