
(** TP11: Typeclasses *)

Require Import Coq.Bool.Bool.
Require Import Setoid.
Require Import Coq.Classes.Equivalence.
Require Import List.
Require Import ListSet.
Import ListNotations.

Section GroupAxioms.

  Definition is_associative {A: Set} (f: A -> A -> A) := 
    forall a b c, f (f a b) c = f a (f b c).

  Definition is_commutative {A: Set} (f: A -> A -> A) := 
    forall a b, f a b = f b a.

  Definition is_inverse {A: Set} (f: A -> A -> A) (inv: A -> A) (u: A) :=
    forall a,  f a (inv a) = u /\ f (inv a) a = u.

  Definition is_unit {A: Set} (f: A -> A -> A) (u: A) := 
    forall a, f a u = a /\ f u a = a.

  Definition is_monoid {A: Set} (op: A -> A -> A) (unit: A) := 
    (is_associative op) /\ (is_unit op unit).

  Definition is_group {A: Set} (op: A -> A -> A) (inv: A -> A) (unit: A) :=
    is_monoid op unit /\ is_inverse op inv unit.

  Definition is_abelian {A: Set} (op: A -> A -> A) (inv: A -> A) (unit: A) :=
    is_group op inv unit /\ is_commutative op.



(** 
Exercise:
Define classes Magma, Monoid, Group and AbGroup

*)

Generalizable Variables dot one inv.


Section GroupAxioms.


End GroupAxioms. 

(** 
Exercise: 
Define an instance of the above class for Z viewed 
as an multiplicative magma and monoid, using both 
the definition mode and the interactive mode.
*)

Require Import ZArith.
Open Scope Z_scope.

Section ZMonoid.

#[export] Instance ZMultMagma : Magma Zmult 1:=
{
(* Fill in here *)
}.


#[export] Instance ZMultMonoid : Monoid Zmult 1.
Admitted.

End ZMonoid.

(* 
In case you have issues later, you may have to come 
back to this point and uncomment the following: 
Remove Hints ZMultMagma ZMultMonoid : typeclass_instances.
*)

(** 
Exercise
Let us consider the basic properties of the above algebraic structures:
*)

Section BasicProperties.


Lemma mag_dot_l: forall `{M: Magma}, 
forall (a b c : A), b = c -> dot a b = dot a c.
Proof.
Admitted.

Lemma mon_dot_l: forall `{M: Monoid}, 
forall (a b c : B), b = c -> dot a b = dot a c.
Proof.
Admitted.

Check @Monoid_Magma.

Lemma mag_to_mon: forall {P: forall (A: Type), (A -> A -> A) -> A -> Prop}, 
(forall `{M: Magma}, P(A) dot one) -> forall `{M': Monoid}, P(B) dot one.
Proof. 
Admitted.


Lemma mon_dot_l': forall `{M: Monoid}, 
forall (a b c : B), b = c -> dot a b = dot a c.
Proof.
Admitted.


Lemma group_dot_l: forall `{G: Group}, 
forall (a b c : A), b = c -> dot a b = dot a c.
Proof.
Admitted.

Lemma abgroup_dot_l: forall `{G: AbGroup}, 
forall (a b c : A), b = c -> dot a b = dot a c.
Proof.
Admitted.

End BasicProperties.



(** 
Exercise
You shall now establish some more systematic group properties:
*)


Section GroupProperties.
Variable A: Type.
Variable dot: A -> A -> A.
Variable one: A. 
Variable inv: A -> A.
Variable (G: Group dot one inv).
Generalizable Variables A dot one inv. 

Notation "x <*> y" := (dot x y) (at level 50, left associativity).


Lemma group_dot_r: 
forall (a b c : A), b = c -> dot b a = dot c a.
Proof.
Admitted. 

Lemma group_one_r: forall (a : A), dot a one = a.
Proof. 
Admitted. 

Lemma group_one_l: forall (a : A), dot one a = a.
Proof. 
Admitted. 

Lemma group_assoc: 
forall (a b c : A), (a <*> b) <*> c = a <*> (b <*> c).
Proof. 
Admitted. 

 Lemma group_inverse1 : forall a: A, dot a (inv a) = one.
Proof.
Admitted. 

Lemma group_inverse2 : forall (a: A), (inv a) <*> a = one.
Proof.
Admitted. 

Lemma group_inverse3 : forall (a b : A), inv a <*> (a <*> b) = b.
Proof.
Admitted. 

Lemma group_inverse4 : forall (a b : A), a <*> (inv a <*> b) = b.
Proof.
Admitted. 

Hint Rewrite group_inverse1.
Hint Rewrite group_inverse2.
Hint Rewrite group_inverse3.
Hint Rewrite group_inverse4.

Lemma group_inverse_commutes : forall (a: A), 
a <*> (inv a) = (inv a) <*> a.
Proof.
Admitted. 


Lemma group_cancel_l: forall (a b c : A), 
a <*> b = a <*> c -> b = c.
Proof.
Admitted. 

Lemma group_cancel_r: forall (a b c :A), 
b <*> a = c <*> a -> b = c.
Proof.
Admitted. 

Theorem id_is_unique: forall a : A, 
(forall b : A, a <*> b = b) -> a = one.
Proof.
Admitted. 

Theorem dot_one_commutes: forall (a b : A), 
a <*> b = one <-> b <*> a = one.
Proof.  
Admitted. 


Theorem inverse_unique: forall (a b: A), 
a <*> b = one -> b = inv a.
Proof. 
Admitted. 

Theorem inverse_cancel: forall (a: A), inv (inv a) = a.
Proof.
Admitted. 

Hint Rewrite inverse_cancel.

Lemma inverse_cancel2: forall (a b: A), inv a = inv b -> a = b.
Proof.
Admitted. 

  Hint Rewrite group_inverse1.
  Hint Rewrite group_inverse2.
  Hint Rewrite group_inverse3.
  Hint Rewrite group_inverse4.

  Hint Rewrite group_one_r.
  Hint Rewrite group_one_l.
  Hint Rewrite group_assoc.

  Hint Rewrite inverse_cancel.

Lemma inverse_apply: forall (a b : A), inv (a <*> b) = inv b <*> inv a.
Proof.
Admitted. 

Lemma inverse_swap: forall (a b c : A), a = (inv b) <*> c <-> b <*> a = c.
Proof.
Admitted. 

Lemma inverse_one: inv one = one.
Proof.
Admitted. 

Hint Rewrite inverse_one.

End GroupProperties.


(**
Exercise
Now, one shall define two instances of the class Group, 
Z as an additive group and integers modulo as an additive group
*)

Section GroupExamples.
Require Import Coq.ZArith.BinInt.
Require Import ZArithRing.
Require Import Znumtheory.
Require Import Zdiv.
Local Open Scope Z_scope.


Check Group.
Print Group.
Print Magma.
Print Monoid.

(* One first defines the data needed to build the Group instance for Z. *)

Definition Zdot := fun x y => x + y.
Definition Zone := 0.
Definition Zinv := (fun n => -n).

Lemma Z_assoc : forall a b c : Z, a + (b + c) = a + b + c.
Proof.
Admitted.

Lemma Z_one_left : forall a : Z, Z.zero + a = a.
Proof.
Admitted.

Lemma Z_one_right : forall a : Z, a + Z.zero = a.
Proof.
Admitted.

Lemma Z_inv_left : forall a : Z, (fun n => -n) a + a = Z.zero.
Proof. 
Admitted.

Lemma Z_inv_right : forall a : Z, a + (fun n => -n) a = Z.zero.
Proof. 
Admitted.


#[export] Instance integer_magma : Magma Zdot Zone :=
{
(* *)
}.


#[export] Instance integer_monoid : Monoid Zdot Zone :=
{
(* *)
}.

#[export] Instance integer_group : Group Zdot Zone Zinv :=
{
(* *)
}.

Check dot_one_commutes.

Arguments dot_one_commutes {A} {dot} {one} {inv}.

Check (dot_one_commutes integer_group).

(* In order to consider integer modulo, we shall 
define an inductive type, sone operations and 
speculate an axiom. *)

Inductive lt_n (n : Z): Set :=
| lt_n_intro (m : Z) : lt_n n.


Definition lt_n_add n (a b: lt_n n) : (lt_n n).
destruct a as [r]; induction b as [s].
apply (lt_n_intro n ((r + s) mod n)); auto.
Defined.

Notation "m # n" := (lt_n_intro n m) (at level 50, left associativity).

Check (4 # 3).

Axiom lt_n_intro_equality:
forall n a b, a mod n = b mod n -> a # n = b # n.

Require Import Lia.


(* The following definition is written with the interactive mode: *)

Definition lt_n_inv n (a: lt_n n) : (lt_n n).
      destruct a as [r].
      apply (lt_n_intro n (-r)); auto.
    Defined.

Definition lt_n_zero n := lt_n_intro n 0.

Lemma lt_n_add_rewrite n: forall r s,
        lt_n_add n (r # n) (s # n) =
        ((r + s) mod n) # n.
Proof.
Admitted.

Lemma lt_n_add_comm n:
      forall (a b : lt_n n), lt_n_add n a b = lt_n_add n b a.
Proof.
Admitted.

Lemma lt_n_add_assoc n:
      forall (a b c: lt_n n),
        lt_n_add n a (lt_n_add n b c) = lt_n_add n (lt_n_add n a b) c.
Proof.
Admitted.


Lemma lt_n_add_inverse_right n: forall a,
        lt_n_add n a (lt_n_inv n a) =
        lt_n_zero n.
Proof.
Admitted.

    Lemma lt_n_add_inverse_left n: forall a,
        lt_n_add n (lt_n_inv n a) a =
        lt_n_zero n.
Proof.
Admitted.


    Lemma lt_n_add_zero_right n: forall a,
        lt_n_add n a (lt_n_zero n) = a.
Proof.
Admitted.

    Lemma lt_n_add_zero_left n: forall a,
        lt_n_add n (lt_n_zero n) a = a.
Proof.
Admitted.

o(* We then declare a variable n of type Z to represent 
the parameter of the Zn *)

Variable n : Z.

#[export] Instance Zn_add_magma : Magma (lt_n_add n) (lt_n_zero n) :=
{
(* *)
}.


#[export] Instance Zn_add_monoid : Monoid (lt_n_add n) (lt_n_zero n) :=
{
(* *)
}.

#[export] Instance Zn_add_group : Group (lt_n_add n) (lt_n_zero n) (lt_n_inv n) :=
{
(* *)
}.
 

Check Zn_add_group.

End GroupExamples.

Check Zn_add_group.


(** 
Exercise
We shall now consider how one can consider subgroups.
*)

Section subgroups.

(* A subset of A is defined as a boolean function over A *)

Definition set (A : Set) := A -> bool.

(* is_mem turns the above into a predicate *) 
Definition is_mem (A: Set) (H: set A) (a : A) := H a = true.

Arguments is_mem {A} _ _.

(* The following shows that membership is decidable which will be 
useful below in order to reason by case on membership in the form
"destruct (is_mem_dec A B a)" where B is a subset of A and a is 
of type A. 
*)
    Theorem is_mem_dec (A : Set) (H : set A) :
      forall a, { is_mem H a } +  { ~(is_mem H a) }.
Proof.
      unfold is_mem. intros a.
      apply (bool_dec (H a)).
    Qed.

    Theorem is_mem_contradict (A : Set) (H : set A) :
      forall a, is_mem H a -> ~is_mem H a -> False.
Proof.
Admitted.

    Theorem is_mem_not (A : Set) (H : set A):
      forall a, ~is_mem H a <-> (H a) = false.
Proof.
Admitted.

Variable A : Set.
Generalizable Variables A dot zero inv G.

(** Define a subgroup class parameterized by a group G: 
it should be equiped with 
- subgroup_mem, a membership field (in the form of a field of type 
  set A where A is the carrier of G)
- subgroup_z, a proof that the subgroup contains the unit of G
- subgroup_closed, a proof that the subgroup is closed by the 
  group binary law
- subgroup_inverse, a proof that the subgroup is closed by inverse
*) 

Class subgroup `(G : Group A dot zero inv) : Type := 
{
(* *)
}.

    Lemma subgroup_mem_bool_rewrite `(H: subgroup):
      forall a b, (is_mem subgroup_mem a) <-> (is_mem subgroup_mem b) -> 
             subgroup_mem a = subgroup_mem b.
Proof.
Admitted.

    Lemma subgroup_closed1: forall `(H: subgroup) (a b : A),
        is_mem subgroup_mem a -> is_mem subgroup_mem b -> 
        is_mem subgroup_mem  (dot a b).
Proof.
Admitted.

    Lemma subgroup_closed2: forall `(H : subgroup) (a b : A),
        is_mem subgroup_mem a -> is_mem subgroup_mem  b -> 
        is_mem subgroup_mem  (dot b a).
Proof.
Admitted.

    Lemma subgroup_inverse1: forall `(H: subgroup ) (a : A),
        is_mem subgroup_mem a -> is_mem subgroup_mem  (inv a).
Proof.
Admitted.

    Lemma subgroup_inverse2: forall `(H: subgroup) (a : A),
        is_mem subgroup_mem  (inv a) -> is_mem subgroup_mem  a.
Proof.
Admitted.

    Lemma subgroup_inverse3: forall `(H: subgroup) (a : A),
        is_mem subgroup_mem (inv a) <-> is_mem subgroup_mem a.
Proof.
Admitted.

    Lemma subgroup_op_non_member_right: forall `(H: subgroup) (a b : A),
        is_mem subgroup_mem a -> ~is_mem subgroup_mem b -> 
        ~is_mem subgroup_mem (dot a b).
Proof.
Admitted.

    Lemma subgroup_op_non_member_left: forall `(H : subgroup) (a b : A),
        is_mem subgroup_mem b -> ~is_mem subgroup_mem a -> 
        ~is_mem subgroup_mem (dot a b).
Proof.
Admitted.

    Lemma subgroup_mem_l:
      forall `(H : subgroup) a b,
        is_mem subgroup_mem a -> is_mem subgroup_mem (dot a b) <-> is_mem subgroup_mem b.
Proof.
Admitted.

    Lemma subgroup_mem_r: forall `(H : subgroup) a b,
        is_mem subgroup_mem b -> is_mem subgroup_mem (dot a b) <-> is_mem subgroup_mem a.
Proof.
Admitted.

    Lemma subgroup_inverse_non_member1: forall `(H: subgroup) a,
        ~is_mem subgroup_mem a -> ~is_mem subgroup_mem (inv a).
Proof.
Admitted.

    Lemma subgroup_inverse_non_member2: forall `(H: subgroup ) a,
        ~is_mem  subgroup_mem  (inv a) -> ~is_mem  subgroup_mem  a.
Proof.
Admitted.

  End subgroups.

